#ifndef LIBANGEVESSEL_BAYSLICE_H
#define LIBANGEVESSEL_BAYSLICE_H

#include "angevessel_export.h"
#include "bayrowtier.h"
#include "decklevel.h"

#include <ange/units/units.h>

#include <QList>
#include <QObject>

namespace ange {
namespace vessel {

class BaySlicePrivate;
class CargoHold;
class Compartment;
class Stack;
class Vessel;

/*
 * A slice of the vessel, it consists of all the virtual stacks in the same cross section.
 */
class ANGEVESSEL_EXPORT BaySlice : public QObject {

    Q_OBJECT

public:

    /**
     * Construct BaySlice
     * @param stacks
     *   stacks in this BaySlice, this constructor takes ownership of all the stacks
     */
    BaySlice(const QList<Stack*>& stacks);

    virtual ~BaySlice();

    /**
     * @return all stacks in slice
     */
    const QList<Stack*> stacks() const ;

    /**
     * @return all stacks above deck in slice
     */
    const QList<Stack*> stacksAbove() const ;

    /**
     * @return all stacks below deck in slice
     */
    const QList<Stack*> stacksBelow() const ;

    /**
     * Gets the stack containing position brt, will return 0 if there is no such stack.
     * As Stack is the full virtual stack there is no issue with what to do when asked for a 40' position.
     * @param brt
     *   bay/row/tier position
     */
    Stack* stackContaining(BayRowTier brt) const;

    /**
     * @return the stack containing @param row
     * As Stack is the full virtual stack there is no issue with what to do when asked for a 40' position.
     */
    Stack* stackContaining(int row, ange::vessel::DeckLevel level) const;

    /**
     * @returns the names of the bays this bay_slice covers, in order of fore to aft
     */
    QList<int> bays() const ;

    /**
     * @returns the bay name that represents the entire slice (e.g. for 13,14,15 -> 14 or 43->43)
     */
    int joinedBay() const;

    /**
     * This contains bay numbers from:
     *  - bays()
     *  - joinedBay()
     *  - stacks()
     * @returns the names of the _all_ bays this bay_slice covers, in order of fore to aft
     */
    QList<int> allBays() const;

    /**
     * @return the compartments in this bay
     */
    QList<Compartment*> compartments() const;

    /**
     * @return compartment containing row
     */
    Compartment* compartmentContainingRow(int row, ange::vessel::DeckLevel level) const;

    /**
     * @return true if brt is aftwards in bay_slice
     */
    bool isAftward(ange::vessel::BayRowTier brt) const;

    /**
     * @return true if brt is forwards in bay_slice
     */
    bool isForward(ange::vessel::BayRowTier brt) const;

    /**
     * @return true if bay is aftwards in bay_slice
     * Note: a pure 40' bay is both aftward and forward
     * and so is a single 20' bay.
     * A 40' bay between 2 20' bays is neither nor.
     */
    bool isAftward(int bay) const;

    /**
     * @return true if bay is forwards  in bay_slice
     * Note: a pure 40' bay is both aftward and forward
     * and so is a single 20' bay
     * A 40' bay between 2 20' bay is neither nor.
     */
    bool isForward(int bay) const;

    /**
     * Sets the hold of the slice (and add myself to the hold)
     **/
    void setCargoHold(ange::vessel::CargoHold* cargoHold);

    /**
     * @return hold of this slice, or null if no hold
     */
    CargoHold* cargoHold() const;

    /**
     * sets the slots of the bayslice to be clear of living quarters
     */
    void setClearOfLivingQuarters(bool isClearAbove, bool isClearBelow);

    /**
     * @return the median LCG of the stacks in the bay.
     */
    ange::units::Length lcg() const;

private:
    void setCompartments(QList< ange::vessel::Compartment* > compartments);

private:
    QScopedPointer<BaySlicePrivate> d;

private:
    friend class Vessel;
};

ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const BaySlice* bay_slice);
ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const BaySlice& bay_slice);

}} // namespace ange::vessel

#endif // VESSEL_BAY_SLICE_H
