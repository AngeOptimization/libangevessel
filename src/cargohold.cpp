#include "cargohold.h"

#include "bayslice.h"
#include "decklevel.h"
#include "vessel.h"

namespace ange {
namespace vessel {

class CargoHold::CargoHoldPrivate : public QSharedData {
public:
    QList<BaySlice*> m_baySlices;
    QHash<DeckLevel, QSet<QString> > m_forbiddenImoClasses;
    QHash<DeckLevel, QSet<QString> > m_acceptedImoClasses;
};

CargoHold::CargoHold(QObject* parent)
  : QObject(parent),
    d(new CargoHoldPrivate())
{
    // Empty
}

CargoHold::~CargoHold() {
    Q_FOREACH(BaySlice* slice, d->m_baySlices) {
        slice->setCargoHold(0L);
    }
}

void CargoHold::addBayslice(BaySlice* bay_slice) {
    if (!d->m_baySlices.contains(bay_slice)) {
        d->m_baySlices << bay_slice;
        bay_slice->setCargoHold(this);
    }
}

void CargoHold::removeBayslice(BaySlice* bay_slice) {
    if (d->m_baySlices.contains(bay_slice)) {
        d->m_baySlices.removeOne(bay_slice);
        bay_slice->setCargoHold(0L);
    }
}

QList<BaySlice*> CargoHold::baySlices() const {
    return d->m_baySlices;
}

// Decode the DG string from the JSON format (should this be moved to parser?)
//   Transform 1.1-1.6 to the six codes: 1.1, 1.2, ..., 1.6
//   Transform e.g. 3(A-D) to 3 and 6.1(A-D) to 6.1
static QSet<QString> decodeDgString(const QString& dgString) {
    QSet<QString> classes;
    if (dgString == "1.1-1.6") {
        classes << "1.1" << "1.2" << "1.3" << "1.4" << "1.5" << "1.6";
    } else {
        QRegExp classExtractor("(\\d(?:\\.\\d)?)(\\([A-D]\\))?");
        if (classExtractor.indexIn(dgString) != -1) {
            const QString classString = classExtractor.cap(1);
            classes << classString;
        } else {
            qWarning() << "Could not decode the DG string " << dgString;
            // This should really be a warning in the vessel parsing
        }
    }
    return classes;
}

void CargoHold::addForbiddenImoClass(const QString& imoClass, DeckLevel level) {
    d->m_forbiddenImoClasses[level] += decodeDgString(imoClass);
}

QSet<QString> CargoHold::forbiddenImoClasses(DeckLevel level) const {
    return d->m_forbiddenImoClasses.value(level);
}

void CargoHold::addAcceptedImoClass(const QString& imoClass, DeckLevel level) {
    d->m_acceptedImoClasses[level] += decodeDgString(imoClass);
}

QSet<QString> CargoHold::acceptedImoClasses(DeckLevel level) const {
    return d->m_acceptedImoClasses.value(level);
}

QDebug operator<<(QDebug dbg, const CargoHold& hold) {
    dbg.nospace() << "[cargo hold:" << hold.baySlices() << "]";
    return dbg.space();
}

QDebug operator<<(QDebug dbg, const CargoHold* hold) {
    if (hold) {
        return dbg << *hold;
    }
    return dbg << "[cargo hold: null]";
}

}} // namespaces
