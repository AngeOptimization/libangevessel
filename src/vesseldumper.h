#ifndef VESSEL_DUMPER_H
#define VESSEL_DUMPER_H

#include "angevessel_export.h"

#include <QScopedPointer>

namespace ange {
namespace vessel {

class Vessel;

class ANGEVESSEL_EXPORT VesselDumper {

public:
    VesselDumper(const Vessel* vessel);
    virtual ~VesselDumper();
    const Vessel* vessel() const;
private:
    class VesselDumperPrivate;
    QScopedPointer<VesselDumperPrivate> d;
};

ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, VesselDumper* dumper);

}
}

#endif // VESSEL_DUMPER_H
