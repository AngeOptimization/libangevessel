#ifndef ANGEVESSEL_BLOCKWEIGHT_H
#define ANGEVESSEL_BLOCKWEIGHT_H

#include "angevessel_export.h"

#include <ange/units/units.h>

#include <QSharedPointer>

namespace ange {
namespace vessel {

/**
 * A class representing a "weight block" on the vessel, i.e.
 * characterized by a LCG, VCG and a TCG, a weight together with a
 * longitudinal extent and longitudinal linear weight distribution
 *
 * The weight distribution is allowed to be positive, negative or both,
 * i.e. there are no constraints on the densities.
 *
 * Point masses (longitudinal direction) are approximated with a 1 cm extent.
 */
class ANGEVESSEL_EXPORT BlockWeight {

public:

    /**
     * Construct a block weight.
     * @param lcg the lcg of the block
     * @param aft_limit the coordinate set describing the aftmost point of the block.
     * @param fore_limit the coordinate set describing the foremost point of the block
     * @param vcg the vcg of the block
     * @param tcg the tcg of the block
     * @param weight total weight of block
     * Note: we do not accept point masses, i.e. the block_weight needs to have longitudinal extent.
     * The longitudinal extent defaults to 1 cm if fore_limit == aft_limit
     */
    BlockWeight(ange::units::Length lcg, ange::units::Length aft_limit, ange::units::Length fore_limit,
                ange::units::Length vcg, ange::units::Length tcg, ange::units::Mass weight);

    /**
     * Construct a block weight.
     * @param aft_limit the coordinate set describing the aftmost point of the block.
     * @param aft_density linear density in aft point
     * @param fore_limit the coordinate set describing the foremost point of the block
     * @param fore_density linear density in fore point
     * @param vcg the vcg of the block
     * @param tcg the tcg of the block
     * Note: we do not accept point masses, i.e. the block_weight needs to have longitudinal extent.
     * The longitudinal extent defaults to 1 cm if fore_limit == aft_limit
     */
    BlockWeight(ange::units::Length aft_limit, ange::units::LinearDensity aft_density,
                ange::units::Length fore_limit, ange::units::LinearDensity fore_density,
                ange::units::Length vcg = 0.0 * ange::units::meter, ange::units::Length tcg = 0.0 * ange::units::meter);

    /**
     * Copy constructor
     */
    BlockWeight(const BlockWeight& other);

    /**
     * Construct the unpositioned block, it has no longitudinal placement and the other parameters are zero
     */
    BlockWeight();

    /**
     * Destructor
     */
    ~BlockWeight();

    /**
     * Assignment operator
     */
    BlockWeight& operator=(const BlockWeight& other);

    /**
     * @return the lcg of the block
     */
    ange::units::Length lcg() const;

    /**
     * @return the aft coordinate (longitudinal) of the block
     */
    ange::units::Length aftLimit() const;

    /**
     * @return the fore coordinate (longitudinal) of the block
     */
    ange::units::Length foreLimit() const;

    /**
     * @return the weight of the block
     */
    ange::units::Mass weight() const;

    /**
     * @return the aft density of the block
     */
    ange::units::LinearDensity aftDensity() const;

    /**
     * @return the fore density of the block
     */
    ange::units::LinearDensity foreDensity() const;

    /**
     * @return the tcg of the block in meters
     */
    ange::units::Length tcg() const;

    /**
     * @return the vcg of the block in meters
     */
    ange::units::Length vcg() const;

    /**
     * @return the description of this block
     */
    QString description() const;

    /**
     * sets the aft limit of the block_weight
     */
    void setAftLimit(ange::units::Length longitudinalPosition);

    /**
     * sets the aft density
     */
    void setAftDensity(ange::units::LinearDensity density);

    /**
     * sets the fore limit
     */
    void setForeLimit(ange::units::Length longitudinalPosition);

    /**
     * sets the fore density
     */
    void setForeDensity(ange::units::LinearDensity density);

    /**
     * sets the vcg
     */
    void setVcg(ange::units::Length vcg);

    /**
     * sets the tcg
     */
    void setTcg(ange::units::Length tcg);

    /**
     * sets the weight
     * Note: after the weight has been set, the
     * densities will be of equal sign.
     */
    void setWeight(ange::units::Mass weight);

    /**
     * Sets the destription of this block
     * @param description
     */
    void setDescription(const QString& description);

    /**
     * extends the block_weight with another block_weight i.e.
     * adds the weights and combines the CGs linearly depending
     * on the weights.
     *
     * The new longitudinal extent covers both blocks (and what might
     * be in between. Once the extent has been increased in this way
     * it cannot be reduced back again.
     */
    void extendWith(const BlockWeight& other);

    /**
     * @return a block with the opposite weight
     */
    BlockWeight oppositeWeight() const;

    /**
     * The mass of the block aft of the given reference plane
     */
    ange::units::Mass weightAftOf(ange::units::Length longitudinalPosition) const;

    /**
     * The mass of the block fore of the given reference plane
     */
    ange::units::Mass weightForeOf(ange::units::Length longitudinalPosition) const;

    /**
     * The moment of the block aft of the given reference plane, relative to the same reference plane
     */
    ange::units::MassMoment longitudinalMomentAftOf(ange::units::Length longitudinalPosition) const;

    /**
     * The moment of the block fore of the given reference plane, relative to the same reference plane
     */
    ange::units::MassMoment longitudinalMomentForeOf(ange::units::Length longitudinalPosition) const;

    /**
     * The moment in the longitudinal direction, relative to the given reference plane
     */
    ange::units::MassMoment longitudinalMoment(ange::units::Length longitudinalPosition = 0.0 * ange::units::meter) const;

    /**
     * The moment in the vertical direction, relative to the given reference plane
     */
    ange::units::MassMoment verticalMoment(ange::units::Length verticalPosition = 0.0 * ange::units::meter) const;

    /**
     * The moment in the transverse direction, relative to the given reference plane
     */
    ange::units::MassMoment transverseMoment(ange::units::Length transversePosition = 0.0 * ange::units::meter) const;

    /**
     * mirrors the block weight longitudinally wrt. 0
     */
    void mirrorLongitudinally();

    /**
     * @return a string description of the block weight
     * for printing/debugging purposes
     */
    QString toString() const;

    /**
     * @return the cumulated block weight of the block weights contained in @param block_weights
     */
    static BlockWeight totalBlockWeight(const QList<BlockWeight>& blockWeights);

private:
    /**
     * @returns the torque of the block weight obtained by slicing this
     * block_weight at @param start and @param end, the torque being calculated
     * with reference to @param torque_reference
     */
    double torqueIntegral(ange::units::Length start, ange::units::Length end, ange::units::Length torqueReference) const;

private:
    class BlockWeightPrivate;
    QSharedDataPointer<BlockWeightPrivate> d;

};

ANGEVESSEL_EXPORT QDebug operator<<(QDebug stream, const BlockWeight& block);
ANGEVESSEL_EXPORT QDebug operator<<(QDebug stream, const BlockWeight* block);

}} // namespace ange::vessel

#endif // ANGEVESSEL_BLOCKWEIGHT_H
