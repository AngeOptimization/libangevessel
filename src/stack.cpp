#include "stack.h"

#include "slot.h"
#include "bayslice.h"
#include "stacksupport.h"

#include <QMap>
#include <QMetaEnum>

using namespace ange::units;

namespace ange {
namespace vessel {

class StackPrivate {
  public:
    StackPrivate(StackSupport* stack_support_20_fore,
                    StackSupport* stack_support_20_aft,
                    StackSupport* stack_support_40,
                    const ange::vessel::Point3D& bottom,
                    DeckLevel level);
    QList<StackSupport*> stack_supports() const {
      QList<StackSupport*> rv;
      rv << m_stack_support_20_fore << m_stack_support_40 << m_stack_support_20_aft;
      return rv;
    }
    QList<Slot*> m_slots;
    Point3D m_bottom;
    DeckLevel m_level;
    QList<int> m_bays;
    int m_row;
    QList<int> m_tiers;
    QList<HatchCover*> m_lidsAbove;
    QList<HatchCover*> m_lidsBelow;
    StackSupport* m_stack_support_20_fore;
    StackSupport* m_stack_support_20_aft;
    StackSupport* m_stack_support_40;
    Compartment* m_compartment;
    bool m_is_twinlift_supported;
    bool m_has_generated_fake_40_slots;
    QSet<QString> m_allowed_imo_classes;
    QSet<QString> m_forbiddenImoClasses;
    bool m_imoForbidden;
    bool m_isClearOfLivingQuarters;
    ~StackPrivate() {
        delete m_stack_support_20_aft;
        delete m_stack_support_20_fore;
        delete m_stack_support_40;
        qDeleteAll(m_slots);
    }
};

Stack::Stack(const ange::vessel::Point3D& bottom,
                 DeckLevel level,
                 StackSupport* stack_support_20_fore,
                 StackSupport* stack_support_20_aft,
                 StackSupport* stack_support_40) :
    QObject(0),
    d(new StackPrivate(stack_support_20_fore, stack_support_20_aft, stack_support_40, bottom, level))
{
  //the reason for this being here and not
  //actually in the private class is that we
  //rely on friend declarations in stack_support
  //as we really don't want outsiders to be able
  //to call the reset_slots method. It is a internal
  //detail
  if(d->m_has_generated_fake_40_slots) {
    stack_support_40->resetSlots(d->m_slots);
  }
  Q_FOREACH(Slot* stack_slot, stackSlots()) {
    stack_slot->setStack(this, bottom.l());
  }
  if (stack_support_20_aft) {
    stack_support_20_aft->setStack(this);
  }
  if (stack_support_20_fore) {
    stack_support_20_fore->setStack(this);
  }
  if (stack_support_40) {
    stack_support_40->setStack(this);
  }
}

Stack::~Stack() {
   // empty
}

BaySlice* Stack::baySlice() const {
  return static_cast<BaySlice*>(parent());
}

QList< Slot* > Stack::stackSlots() const {
  return d->m_slots;
}

QList< vessel::StackSupport* > Stack::stackSupports() const {
    QList<vessel::StackSupport*> list;
    list << d->m_stack_support_20_fore << d->m_stack_support_40 << d->m_stack_support_20_aft;
    return list;
}

Slot* Stack::slotAt(BayRowTier brt) const {
  Q_FOREACH (Slot* slot, d->m_slots) {
    if (slot->brt() == brt) {
      return slot;
    }
  }
  // 20/40 offset hack:
  if (brt.bay() % 2 == 0) {
    // Done in two loops as we should get the fore 20' if at all possible
    Q_FOREACH (Slot* slot, d->m_slots) {
      if (BayRowTier(slot->brt().bay() + 1, slot->brt().row(), slot->brt().tier()) == brt) {
        return slot;
      }
    }
    Q_FOREACH (Slot* slot, d->m_slots) {
      if (BayRowTier(slot->brt().bay() - 1, slot->brt().row(), slot->brt().tier()) == brt) {
        return slot;
      }
    }
  }
  return 0;
}

QString Stack::toString() const {
  int bay = *bays().begin();
  Q_FOREACH(int b, bays()) {
    if (b%2 == 0) {
      bay = b;
    }
  }
  return QString("%1-%2%3").arg(bay).arg(row()).arg(toChar(level()));
}

struct sort_by_tier_t {
    bool operator()(const Slot* lhs, const Slot* rhs) const {
        return (lhs->brt().tier() < rhs->brt().tier());
    }
};

StackPrivate::StackPrivate(StackSupport* stack_support_20_fore,
                                 StackSupport* stack_support_20_aft,
                                 StackSupport* stack_support_40,
                                 const ange::vessel::Point3D& bottom,
                                 DeckLevel level) :
        m_slots(),
        m_bottom(bottom),
        m_level(level),
        m_bays(),
        m_row(-1),
        m_tiers(),
        m_stack_support_20_fore(stack_support_20_fore),
        m_stack_support_20_aft(stack_support_20_aft),
        m_stack_support_40(stack_support_40),
        m_compartment(0L),
        m_is_twinlift_supported(true),
        m_has_generated_fake_40_slots(false),
        m_allowed_imo_classes(QSet<QString>()),
        m_forbiddenImoClasses(QSet<QString>()),
        m_imoForbidden(true),
        m_isClearOfLivingQuarters(true)
{
  if (m_stack_support_20_aft) {
    m_slots.append(m_stack_support_20_aft->stackSlots());
  }
  if (m_stack_support_20_fore) {
    m_slots.append(m_stack_support_20_fore->stackSlots());
  }
  if (m_stack_support_40) {
    // Create pseudo-20' slots from 40' slot
    Q_FOREACH(const Slot* slot40, m_stack_support_40->stackSlots()) {
      bool has_seen_as_20 = false;  // check if tier has been seen in 20, alternatively this is a 40 only slot
      Q_FOREACH(const Slot* slot20, m_slots){
         if (slot40->brt().tier() == slot20->brt().tier()) {
             has_seen_as_20 = true;
             break;
         }
      }
      if (has_seen_as_20) continue;

      // this is a 40' only slot which is split into two virtual 20' slots
      Point3D pos40 = slot40->pos();
      BayRowTier brt40 = slot40->brt();
      Slot::Capabilities caps;
      QMetaEnum capenum;
      Q_ASSERT(!capenum.isValid());
      for(int i = 0 ; i < slot40->metaObject()->enumeratorCount(); i++) {
        if(qstrcmp(slot40->metaObject()->enumerator(i).name(),"Capability")==0) {
          capenum = slot40->metaObject()->enumerator(i);
          break;
        }
      }
      Q_ASSERT(capenum.isValid());
      Q_ASSERT_X(qstrcmp(capenum.name(),"Capability")==0,"wrong capability enum found, expected Capability, got",capenum.name());
      for(int i = 0 ; i < capenum.keyCount() ; i ++) {
        Slot::Capability cap = static_cast<Slot::Capability>(capenum.value(i));
        if(slot40->hasCapability(cap)) {
          caps |= cap;
        }
      }
      Slot* slotfore = new Slot(Point3D(pos40.l()+3.048*meter, pos40.v(), pos40.t()), BayRowTier(brt40.bay()-1, brt40.row(), brt40.tier())); // 3.048=10 feet
      slotfore->addCapabilities(caps);
      Slot* slotaft = new Slot(Point3D(pos40.l()-3.048*meter, pos40.v(), pos40.t()), BayRowTier(brt40.bay()+1, brt40.row(), brt40.tier())); // 3.048=10 feet
      slotaft->addCapabilities(caps);
#ifndef NDEBUG
      for(int i = 0 ; i < capenum.keyCount() ; i ++) {
        Slot::Capability cap = static_cast<Slot::Capability>(capenum.value(i));
        Q_UNUSED(cap);
        Q_ASSERT_X(slotfore->hasCapability(cap)==slot40->hasCapability(cap),
                   QString::fromLatin1("creating fake 20'ies").toLocal8Bit(),
                   QString::fromLatin1("At capability %1, 20: %2, 40: %3").arg(capenum.valueToKey(cap)).arg(slotfore->hasCapability(cap)).arg(slot40->hasCapability(cap)).toLocal8Bit()
                  );
        Q_ASSERT_X(slotaft->hasCapability(cap)==slot40->hasCapability(cap),
                   QString::fromLatin1("creating fake 20'ies").toLocal8Bit(),
                   QString::fromLatin1("At capability %1, 20: %2, 40: %3").arg(capenum.valueToKey(cap)).arg(slotaft->hasCapability(cap)).arg(slot40->hasCapability(cap)).toLocal8Bit()
                  );
      }
#endif // NDEBUG
      delete slot40;
      //Note: this fore-aft ordering is no longer valid after mirroring the stack longitudinally...
      m_slots << slotfore << slotaft;
    }
    m_has_generated_fake_40_slots=true;
  }
  qSort(m_slots.begin(), m_slots.end(), sort_by_tier_t());

  QMap<int, Slot*> tierMap;
  Q_FOREACH (Slot* stack_slot, m_slots) {
    BayRowTier brt = stack_slot->brt();
    m_row = brt.row();
    if (!m_tiers.contains(brt.tier())) {
      m_tiers << brt.tier();
    }
    Slot*& sister = tierMap[brt.tier()];
    if (not sister) {
      sister =  stack_slot;
    } else {
      // join the stern-most as slave to the fore-most.
      Q_ASSERT(std::abs(sister->brt().bay() - stack_slot->brt().bay()) == 2);
      if (sister->brt().bay() > stack_slot->brt().bay()) {
        stack_slot->joinSisterAsSlave(sister);
      } else {
        sister->joinSisterAsSlave(stack_slot);
      }
    }
  }
  qSort(m_tiers);
  if (m_stack_support_20_fore) {
    m_bays << m_stack_support_20_fore->bay();
  }
  if (m_stack_support_40) {
    m_bays << m_stack_support_40->bay();
  }
  if (m_stack_support_20_aft) {
    m_bays << m_stack_support_20_aft->bay();
  }
  Q_ASSERT(m_bays.size() <= 3);
  Q_ASSERT(m_bays.size() >= 0);
}

const ange::vessel::Point3D& Stack::bottom() const {
    return d->m_bottom;
}
QList<int> Stack::bays() const {
    return d->m_bays;
}
const QList< int >& Stack::tiers() const {
    return d->m_tiers;
}
int Stack::row() const {
    return d->m_row;
}
DeckLevel Stack::level() const {
    return d->m_level;
}

Stack* Stack::portNeighbour() const {
  QList<Stack*> stacks = level() == Above? baySlice()->stacksAbove() : baySlice()->stacksBelow();
  // I cannot imagine indexOf ever dereferencing the argument, hence the const_cast
  const int index = stacks.indexOf(const_cast<Stack*>(this));
  if (index>0) {
    return stacks.at(index-1);
  }
  return 0L;
}

Stack* Stack::starboardNeighbour() const {
  QList<Stack*> stacks = level() == Above? baySlice()->stacksAbove() : baySlice()->stacksBelow();
  const int index = stacks.indexOf(const_cast<Stack*>(this));
  if (index+1<stacks.size()) {
    return stacks.at(index+1);
  }
  return 0L;
}

void Stack::addLidBelow(HatchCover* lid) {
  d->m_lidsBelow << lid;
}

void Stack::addLidAbove(HatchCover* lid) {
  d->m_lidsAbove << lid;
}

QDebug operator<<(QDebug dbg, const ange::vessel::Stack& stack) {
  return dbg << stack.toString();
}

QDebug operator<<(QDebug dbg, const ange::vessel::Stack* stack) {
  if (stack) {
    return dbg << *stack;
  }
  return dbg << "<null>";
}

StackSupport* Stack::stackSupport20Aft() const {
  return d->m_stack_support_20_aft;
}

StackSupport* Stack::stackSupport20Fore() const {
    return d->m_stack_support_20_fore;
}

StackSupport* Stack::stackSupport40() const {
  return d->m_stack_support_40;
}

Mass Stack::maximumTotalWeight() const {
  const bool has_20_aft  = stackSupport20Aft() && !std::isinf(stackSupport20Aft()->maxWeight()/kilogram);
  const bool has_20_fore = stackSupport20Fore() && !std::isinf(stackSupport20Fore()->maxWeight()/kilogram);
  const bool has_40 = stackSupport40() && !std::isinf(stackSupport40()->maxWeight()/kilogram);
  const Mass fourty_max_weight = has_40 ? stackSupport40()->maxWeight() : 0.0*ton;
  if (has_20_fore || has_20_aft) {
    const Mass twenty_max_weight = (has_20_aft ? stackSupport20Aft()->maxWeight() : 0.0*ton) +
      (has_20_fore ? stackSupport20Fore()->maxWeight() : 0.0*ton);
    if (has_40) {
      if (0.5*twenty_max_weight > fourty_max_weight) {
        // Best solution is to fill in 20 feet until 40' stack weight limit is reached.
        return 2*fourty_max_weight;
      } else {
        // Best solution is to fill up 20' stack weights, then add 40' until 40' stack weight limit is reached
        // = twenty_max_weight + (fourty_max_weight - 0.5*twenty_max_weight) simplified as
        return 0.5*twenty_max_weight + fourty_max_weight;
      }
    } else {
      // No 40' stack weight, best soluton is 20' stack weight
      return twenty_max_weight;
    }
  } else {
    // No 20' stack weight, best solution is 40' stack weight.
    return fourty_max_weight;
  }
}

Compartment* Stack::compartment() const {
  return d->m_compartment;
}

void Stack::setCompartment(Compartment* compartment) {
  d->m_compartment = compartment;
}

void Stack::setTwinlifting(bool twinlifting)
{
  d->m_is_twinlift_supported = twinlifting;
}

bool Stack::isTwinliftSupported() const
{
  return d->m_is_twinlift_supported;
}

bool Stack::canRussian() const {
    if (level() == Below) {
        return true;
    }
    if(!stackSupport20Aft() || !stackSupport20Fore() || !stackSupport40()) {
        return false;
    }
    Length spread = abs(stackSupport20Fore()->bottomPos().l() - stackSupport20Aft()->bottomPos().l()) - 20*feet;
    return spread < 0.1*meter;
}

QList< HatchCover* > Stack::lidsBelow() const {
  return d->m_lidsBelow;
}

QList< HatchCover* > Stack::lidsAbove() const {
  return d->m_lidsAbove;
}

void Stack::mirrorLongitudinally(const Length& medianStackLcg) {
    d->m_bottom = Point3D(-bottom().l(), bottom().v(), bottom().t());
    if(stackSupport40()){
        stackSupport40()->mirrorLongitudinally();
    }
    if(stackSupport20Fore()){
        stackSupport20Fore()->mirrorLongitudinally();
    }
    if(stackSupport20Aft()){
        stackSupport20Aft()->mirrorLongitudinally();
    }
    std::swap(d->m_stack_support_20_aft, d->m_stack_support_20_fore);
    Q_FOREACH(Slot* slot, stackSlots()) {
        slot->mirrorLongitudinally(medianStackLcg);
    }
}

void Stack::setClearOfLivingQuarters(bool isClear) {
    d->m_isClearOfLivingQuarters = isClear;
}

bool Stack::clearOfLivingQuarters() const {
    return d->m_isClearOfLivingQuarters;
}

bool Stack::imoForbidden() const {
    if (stackSupport20Aft() && stackSupport20Aft()->imoForbidden()) {
        return true;
    }
    if (stackSupport20Fore() && stackSupport20Fore()->imoForbidden()) {
        return true;
    }
    if (stackSupport40() && stackSupport40()->imoForbidden()) {
        return true;
    }
    return false;
}

void Stack::setForbiddenImoClasses(const QSet< QString >& forbiddenImoClasses) {
    d->m_forbiddenImoClasses = forbiddenImoClasses;
}

void Stack::setAllowedImoClasses(const QSet< QString >& allowedImoClasses) {
    d->m_allowed_imo_classes = allowedImoClasses;
}

bool Stack::imoClassesAllowed(const QSet< QString >& imoClasses) const {
    Q_FOREACH (const QString& imoClass, d->m_forbiddenImoClasses) {
        if (imoClasses.contains(imoClass)) {
            return false;
        }
    }
    return true;
}

const StackSupport* Stack::stackSupportContaining(int bayNumber) const {
    Q_FOREACH (const StackSupport* stackSupport, stackSupports()){
        if(stackSupport && stackSupport->bay() == bayNumber) {
            return stackSupport;
        }
    }
    return 0;
}

}} // namespace ange::vessel
