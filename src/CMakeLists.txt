add_library(Vessel SHARED
    bayrowlevel.cpp
    bayrowtier.cpp
    bayslice.cpp
    bilinearinterpolator.cpp
    blockweight.cpp
    cargohold.cpp
    compartment.cpp
    direction.cpp
    hatchcover.cpp
    lashingpatterndata.cpp
    lashingrod.cpp
    point3d.cpp
    slot.cpp
    slottablesummary.cpp
    stack.cpp
    stacksupport.cpp
    vessel.cpp
    vesseldumper.cpp
    vesseltank.cpp
)
target_link_libraries(Vessel
    Ange::Utils
    Qt5::Core
)

generate_export_header(Vessel BASE_NAME AngeVessel)

set_property(TARGET Vessel PROPERTY VERSION "${SO_VERSION}.0.0")
set_property(TARGET Vessel PROPERTY SOVERSION "${SO_VERSION}")
set_property(TARGET Vessel PROPERTY OUTPUT_NAME AngeVessel)

install(TARGETS Vessel EXPORT AngeVesselTargets
    RUNTIME DESTINATION "bin"
    LIBRARY DESTINATION "lib"
    ARCHIVE DESTINATION "lib"
    INCLUDES DESTINATION "include/AngeVessel"
)

install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/angevessel_export.h
    bayrowlevel.h
    bayrowtier.h
    bayslice.h
    bilinearinterpolator.h
    blockweight.h
    cargohold.h
    compartment.h
    decklevel.h
    direction.h
    hatchcover.h
    lashingpatterndata.h
    lashingrod.h
    point3d.h
    slot.h
    slottablesummary.h
    stack.h
    stacksupport.h
    vessel.h
    vesseldumper.h
    vesseltank.h
    DESTINATION "include/AngeVessel/ange/vessel"
)

write_basic_package_version_file(
    ${CMAKE_CURRENT_BINARY_DIR}/AngeVesselConfigVersion.cmake
    COMPATIBILITY SameMajorVersion
)

configure_file(
    AngeVesselConfig.cmake.in
    AngeVesselConfig.cmake
    @ONLY
)

set(ConfigPackageLocation lib/cmake/AngeVessel)
install(EXPORT AngeVesselTargets NAMESPACE Ange::
    FILE AngeVesselTargets.cmake
    DESTINATION ${ConfigPackageLocation}
)
install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/AngeVesselConfigVersion.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/AngeVesselConfig.cmake
    DESTINATION ${ConfigPackageLocation}
)

add_subdirectory(test)
