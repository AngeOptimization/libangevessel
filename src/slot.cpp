#include "slot.h"

#include "stack.h"
#include "bayslice.h"
#include "vessel.h"

using namespace ange::units;

namespace ange {
namespace vessel {

class SlotPrivate {
  public:
      enum Position {
          Unknown,
          Fore,
          Aft
      };
    SlotPrivate(Slot* slot,const Point3D& pos, const BayRowTier& brt, Slot::Capabilities capabilities, Slot* sister, bool master) :
    q(slot),
    m_pos(pos),
    m_brt(brt),
    m_sister(sister),
    m_fourty(0),
    m_support_20(0L),
    m_support_40(0L),
    m_master(master),
    m_capabilities(capabilities),
    m_positionCache(Unknown) {}

    const Point3D& pos() const {
      return m_pos;
    }

    const BayRowTier& brt() const {
      return m_brt;
    }

    const Slot* sister() const {
      return m_sister;
    }

    Slot* sister() {
      return m_sister;
    }

    bool is_master() const {
      return m_master;
    }

    void join_sister_as_slave(Slot* master, Slot* slave) {
      m_sister = slave;
      slave->d->set_master(master);
    }

    void set_master(Slot* master) {
      m_sister = master;
      m_master = false;
    }

    Slot* q;
    Point3D m_pos;
    BayRowTier m_brt;
    Slot* m_sister;
    Slot* m_fourty;
    StackSupport* m_support_20;
    StackSupport* m_support_40;
    bool m_master;
    Slot::Capabilities m_capabilities;
    Position m_positionCache;

    void updateAft(const Length& stacksMedianLongitudinalPos) {
        bool isAft;
        isAft  = stacksMedianLongitudinalPos > pos().l();
        if (isAft) {
            m_positionCache = SlotPrivate::Aft;
        } else {
            m_positionCache = SlotPrivate::Fore;
        }
    }
};

Slot::Slot(const ange::vessel::Point3D& pos, const ange::vessel::BayRowTier& brt)
    : d(new SlotPrivate(this,pos, brt, 0, 0L, true)) {
}

Slot::~Slot() {
    // Empty
}

Stack* Slot::stack() const {
  return static_cast<Stack*>(parent());
}

void Slot::joinSisterAsSlave(Slot* sister) {
  Q_ASSERT(sister);
  d->join_sister_as_slave(this, sister);
  Q_ASSERT(sister->stack() == stack());
  Q_ASSERT(sister->brt().tier() == brt().tier());
}

Slot* Slot::sister() const {
  return d->sister();
}

const ange::vessel::BayRowTier& Slot::brt() const {
  return d->brt();
}

const ange::vessel::Point3D& Slot::pos() const {
  return d->pos();
}

QDebug operator<<(QDebug dbg, const Slot& slot) {
  return dbg << QString("[%1]").arg(slot.brt().toString());
}

bool Slot::hasCapability(Slot::Capability capability) const {
  return d->m_capabilities & capability;
}

void Slot::addCapability(Slot::Capability capability) {
    d->m_capabilities |= capability;
}

void Slot::addCapabilities(ange::vessel::Slot::Capabilities capabilities) {
    d->m_capabilities |= capabilities;
}

void Slot::removeCapability(Slot::Capability capability) {
    d->m_capabilities &= ~capability;
}

QDebug operator<<(QDebug dbg, const Slot* slot) {
  if (slot) {
    return dbg << *slot;
  }
  return dbg << "<null>";
}

static Point3D pos_unknown(0*meter,0*meter,0*meter);
static const BayRowTier brt_unknown;
const Slot* const UnknownPosition = new Slot(pos_unknown, brt_unknown);

StackSupport* Slot::support20() const {
  return d->m_support_20;
}

StackSupport* Slot::support40() const {
  return d->m_support_40;
}

bool Slot::isAft() const {
    // Here we would like to check the physical layout, but that is impossible to do in pure 40' stacks
    Q_ASSERT(d->m_positionCache != SlotPrivate::Unknown);
#ifndef QT_NO_DEBUG
    if (support20()) {
        // TODO Make this a warning on the Vessel that gets shown as a problem
        // This can not be changed from qWarning to Q_ASSERT as the problem is in user provided data
        if (d->m_positionCache == SlotPrivate::Aft && support20() == stack()->stackSupport20Fore()) {
            qWarning() << QString("Slot %1 is aft but linked to stackSupport20Fore").arg(brt().toString()).toLocal8Bit();
        }
        if (d->m_positionCache == SlotPrivate::Fore && support20() == stack()->stackSupport20Aft()) {
            qWarning() << QString("Slot %1 is fore but linked to stackSupport20Aft").arg(brt().toString()).toLocal8Bit();
        }
    }
#endif
    return d->m_positionCache == SlotPrivate::Aft;
}

bool Slot::isFore() const {
    return !isAft();
}

bool Slot::isOddSlot() const {
    return !sister();
}

void Slot::setSupport40(StackSupport* support) {
  d->m_support_40 = support;
}

void Slot::setSupport20(StackSupport* support) {
  d->m_support_20 = support;
}

void Slot::mirrorLongitudinally(const Length& medianStackLcg) {
    d->m_pos = Point3D(-d->m_pos.l(), d->m_pos.v(), d->m_pos.t());
    d->updateAft(medianStackLcg);
}

void Slot::setStack(Stack* stack, const Length& bayStacksLcg) {
    setParent(stack);
    d->updateAft(bayStacksLcg);
}

}} // namespace ange::vessel
