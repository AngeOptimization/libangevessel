#include "lashingpatterndata.h"

#include "lashingrod.h"

using namespace ange::units;
using namespace ange::vessel::Direction;

namespace ange {
namespace vessel {


// LashingPatternData:

LashingPatternData::~LashingPatternData() {
    qDeleteAll(m_lashingStackDatas);
    qDeleteAll(m_stackLashingPatterns);
}

void LashingPatternData::addStackPattern(StackLashingPattern* stackLashingPattern) {
    Q_ASSERT(!stackLashingPattern->patternName.isEmpty());
    m_stackLashingPatterns.insert(stackLashingPattern->patternName, stackLashingPattern);
}

void LashingPatternData::addStackData(BayRowLevel bayRowLevel, QString aftPatternName, QString forePatternName, int cellGuidesToTier) {
    Q_ASSERT(!m_lashingStackDatas.contains(bayRowLevel));
    LashingStackData* lashingStackData = new LashingStackData();
    lashingStackData->stackPatterns[Direction::Aft] = m_stackLashingPatterns.values(aftPatternName);
    lashingStackData->stackPatterns[Direction::Fore] = m_stackLashingPatterns.values(forePatternName);
    lashingStackData->cellGuidesToTier = cellGuidesToTier;
    m_lashingStackDatas[bayRowLevel] = lashingStackData;
}

QMultiHash<QString, const StackLashingPattern*> LashingPatternData::stackLashingPatterns() const {
    return m_stackLashingPatterns;
}

QHash<BayRowLevel, const LashingStackData*> LashingPatternData::lashingStackDatas() const {
    return m_lashingStackDatas;
}

const LashingStackData* LashingPatternData::get(BayRowLevel bayRowLevel) const {
    Q_ASSERT(m_lashingStackDatas.contains(bayRowLevel));
    static const LashingStackData emptyLashingStackData;
    return m_lashingStackDatas.value(bayRowLevel, &emptyLashingStackData);
}


// LashingStackData:

LashingStackData::LashingStackData()
  : cellGuidesToTier(0)
{
    // Empty
}

const LashingRod* LashingStackData::generateRod(AftFore end, StarboardPort side, TopBottom topBottom, int containerNumber,
                                                int numberOfContainersInStack, units::Length heightUnderLashing,
                                                const Point3D& containerCorner) const {
    const StackLashingPattern* stackPattern = 0;
    Q_FOREACH (const StackLashingPattern* testStackPattern, stackPatterns.value(end)) {
        if (testStackPattern->minimumNumberOfContainersInStack <= numberOfContainersInStack
            && testStackPattern->minimumHeightUnderLashing() <= heightUnderLashing
            && testStackPattern->attachedToContainerNumber == containerNumber
            && testStackPattern->position == topBottom
            && testStackPattern->side.contains(side)) {
            if (!stackPattern || testStackPattern->minimumHeightUnderLashing() > stackPattern->minimumHeightUnderLashing()) {
                stackPattern = testStackPattern;
            }
        }
    }
    if (stackPattern == 0) {
        return 0;
    }
    LashingRod* rod = new LashingRod();
    rod->setEnd(end);
    rod->setSide(side);
    rod->setTopBottom(topBottom);

    rod->setEModule(stackPattern->eModule);
    rod->setDiameter(stackPattern->diameter);
    rod->setMaxLoad(stackPattern->maxLashingForce);
    Q_ASSERT(!qIsNaN(stackPattern->lashingBridgeDeformation / meter));
    rod->setLashingBridgeDeformation(stackPattern->lashingBridgeDeformation);

    if (qIsNaN(stackPattern->lashingPlatePostionVertical / meter)) {
        Q_ASSERT(!qIsNaN(stackPattern->rodLength / meter));
        Q_ASSERT(!qIsNaN(stackPattern->angle));
        Q_ASSERT(qIsNaN(stackPattern->lashingDistanceTransverse / meter));
        rod->setLength(stackPattern->rodLength);
        rod->setTransverseAngle(stackPattern->angle);
    } else {
        Q_ASSERT(qIsNaN(stackPattern->rodLength / meter));
        Q_ASSERT(qIsNaN(stackPattern->angle));
        Q_ASSERT(!qIsNaN(stackPattern->lashingDistanceTransverse / meter));
        Point3D vesselEnd;
        vesselEnd.setL(containerCorner.l());
        vesselEnd.setV(stackPattern->lashingPlatePostionVertical);
        vesselEnd.setT(containerCorner.t() - stackPattern->lashingDistanceTransverse * (side == Starboard ? +1 : -1));
        rod->setLashingPoints(vesselEnd, containerCorner);
    }
    return rod;
}


// StackLashingPattern:

StackLashingPattern::StackLashingPattern()
  : minimumNumberOfContainersInStack(-1), minimumHcCountUnderLashing(-1), attachedToContainerNumber(-1),
    position(Direction::Top), eModule(qQNaN()), diameter(qQNaN()), maxLashingForce(qQNaN()), lashingBridgeDeformation(0),
    angle(qQNaN()), rodLength(qQNaN()), lashingPlatePostionVertical(qQNaN()), lashingDistanceTransverse(qQNaN())
{
    // Empty
}

units::Length StackLashingPattern::minimumHeightUnderLashing() const {
    if (minimumHcCountUnderLashing == 0) {
        return 0 * feet;
    }
    int containersUnderLashing = attachedToContainerNumber;
    if (position == Bottom) {
        containersUnderLashing -= 1; // When lashing to the bottom the lashed container will be above the lashing
    }
    Q_ASSERT(containersUnderLashing >= minimumHcCountUnderLashing);
    return containersUnderLashing * 8.5 * feet + minimumHcCountUnderLashing * feet - 0.5 * feet;
}


}} // namespace ange::vessel
