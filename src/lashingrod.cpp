#include "lashingrod.h"

#include <cmath>

using namespace ange::units;
using namespace ange::vessel::Direction;

namespace ange {
namespace vessel {

LashingRod::LashingRod()
  : m_eModule(qQNaN()), m_diameter(qQNaN()), m_length(qQNaN()), m_transverseAngle(qQNaN()), m_maxLoad(qQNaN()),
    m_lashingBridgeDeformation(0), m_end(Aft), m_side(Starboard), m_topBottom(Top)
{
    // Empty
}

Unit<1, -1, -2> LashingRod::eModule() const {
    return m_eModule;
}

void LashingRod::setEModule(Unit<1, -1, -2> eModule) {
    m_eModule = eModule;
}

Length LashingRod::diameter() const {
    return m_diameter;
}

void LashingRod::setDiameter(Length diameter) {
    m_diameter = diameter;
}

Length LashingRod::length() const {
    return m_length;
}

void LashingRod::setLength(Length length) {
    m_length = length;
}

double LashingRod::transverseAngle() const {
    return m_transverseAngle;
}

void LashingRod::setTransverseAngle(double transverseAngle) {
    m_transverseAngle = transverseAngle;
}

Force LashingRod::maxLoad() const {
    return m_maxLoad;
}

void LashingRod::setMaxLoad(Force maxLoad) {
    m_maxLoad = maxLoad;
}

Length LashingRod::lashingBridgeDeformation() const {
    return m_lashingBridgeDeformation;
}

void LashingRod::setLashingBridgeDeformation(Length lashingBridgeDeformation) {
    Q_ASSERT(!qIsNaN(lashingBridgeDeformation / meter));
    m_lashingBridgeDeformation = lashingBridgeDeformation;
}

AftFore LashingRod::end() const {
    return m_end;
}

void LashingRod::setEnd(AftFore end) {
    m_end = end;
}

StarboardPort LashingRod::side() const {
    return m_side;
}

void LashingRod::setSide(StarboardPort side) {
    m_side = side;
}

TopBottom LashingRod::topBottom() const {
    return m_topBottom;
}

void LashingRod::setTopBottom(TopBottom topBottom) {
    m_topBottom = topBottom;
}

void LashingRod::setLashingPoints(const Point3D& vessel, const Point3D& container) {
    Point3D rodVector = container - vessel;
    m_length = rodVector.length();
    m_transverseAngle = atan2(rodVector.t() / meter * (m_side == Starboard ? +1 : -1), rodVector.v() / meter) ;
}

Unit<1, 0, -2> LashingRod::transverseSpringConstant() const {
    static const double pi = 3.14159265358979323846;
    Unit<0, 2, 0> area = pi * square(m_diameter / 2);
    double sinTransverseAngle = sin(m_transverseAngle);
    return area * m_eModule * sinTransverseAngle * sinTransverseAngle / m_length;
}

}} // namespace ange::vessel
