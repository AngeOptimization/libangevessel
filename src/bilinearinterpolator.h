/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2013
 Copyright: See COPYING file that comes with this distribution
*/
#ifndef BILINEARINTERPOLATOR_H
#define BILINEARINTERPOLATOR_H
#include "angevessel_export.h"
#include <QSharedDataPointer>

namespace ange {
namespace vessel {
/**
 * Class implementing a grid-based (2D) bilinear interpolation
 * The orientation is as follows: x increases with columns, y with rows.
 * Extrapolation is done according to the nearest neighbour
 */
class ANGEVESSEL_EXPORT BilinearInterpolator
{
public:
    /**
     * Constructor taking
     * @param xCoordinates: the x coordinates (columns)
     * @param yCoordinates: the y coordinates (rows)
     * @param sampleData: the sample values such that f(x_j,y_i) = data(i*nbColumns + j)
     */
    BilinearInterpolator();
    BilinearInterpolator(const QList <qreal>& xCoordinates, const QList <qreal>& yCoordinates,
                         const QList< qreal>& sampleData);
    BilinearInterpolator(const BilinearInterpolator& other);
    virtual ~BilinearInterpolator();                     
    const BilinearInterpolator& operator=(const BilinearInterpolator& rhs);
    /**
     * @return a list of the underlying x coordinates (e.g. for saving)
     */
    const QList< qreal > xCoordinates() const;
    /**
     * @return a list of the underlying x coordinates (e.g. for saving)
     */
    const QList< qreal > yCoordinates() const;
    /**
     * @return the interpolated value at coordinates (@param x, @param y)
     */
    qreal valueAt(qreal x, qreal y) const;
    /**
     * mirror (negate) the x axis
     */
    void mirrorXaxis();
    /**
     *  mirror (negate) the y axis
     */
    void mirrorYaxis();
    /**
     * mirror (negate) the function values
     */
    void mirrorValues();
    /**
     * @return whether or not the function is defined, i.e. contains any values
     */
    bool isValid() const;
private:
    class Private;
    QSharedDataPointer<Private> d;
};


}
}
#endif // BILINEARINTERPOLATOR_H
