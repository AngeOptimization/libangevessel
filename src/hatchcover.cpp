#include "hatchcover.h"

#include "vessel.h"
#include "stack.h"

namespace ange {
namespace vessel {

template<typename T>
QList<const T*> constify(QList<T*> list) {
    QList<const T*> constList;
    Q_FOREACH (const T* t, list) {
        constList << t;
    }
    return constList;
}

class HatchCover::HatchCoverPrivate {
public:
    QList<const Stack*> m_above;
    QList<const Stack*> m_below;
    HatchCoverPrivate(const QList<Stack*>& above, const QList<Stack*>& below) : m_above(constify(above)), m_below(constify(below)) {};
};

HatchCover::HatchCover(QList<Stack*> above, QList<Stack*> below) : d(new HatchCoverPrivate(above, below))
{
    Q_FOREACH (Stack* stackAbove, above) {
        stackAbove->addLidBelow(this);
    }
    Q_FOREACH (Stack* stackBelow, below) {
        stackBelow->addLidAbove(this);
    }
}

HatchCover::~HatchCover() {
    // Just to ensure that QScopedPointer knows about HatchCoverPrivate's destructor
}

QList<const Stack*> HatchCover::stacksAbove() const {
    return d->m_above;
}

QList<const Stack*> HatchCover::stacksBelow() const {
    return d->m_below;
}

QDebug operator<<(QDebug dbg, const HatchCover& lid) {
    dbg.nospace() << "[";
    if (lid.stacksAbove().empty()) {
        dbg << "-";
    } else {
        dbg << lid.stacksAbove().front()->row() << "-" << lid.stacksAbove().back()->row();
    }
    dbg << "/";
    if (lid.stacksBelow().empty()) {
        dbg << "-";
    } else {
        dbg << lid.stacksBelow().front()->row() << "-" << lid.stacksBelow().back()->row();
    }
    dbg << "]";
    return dbg.maybeSpace();
}

QDebug operator<<(QDebug dbg, const HatchCover* lid) {
    if (lid) {
        return dbg << *lid;
    }
    dbg.nospace() << "[null lid]";
    return dbg.maybeSpace();
}

} // namespace vessel
} // namespace ange
