#ifndef LIBANGEVESSEL_STACK_H
#define LIBANGEVESSEL_STACK_H

#include "angevessel_export.h"

#include "point3d.h"
#include "bayrowtier.h"
#include "decklevel.h"
#include <ange/units/units.h>

#include <QObject>
#include <QList>
#include <QSet>

namespace ange {
namespace vessel {
class Slot;
class BaySlice;
class StackPrivate;
class HatchCover;
class StackSupport;
class Compartment;
}
}

namespace ange {
namespace vessel {

/**
 * The "virtual" stack that can contain several actual 20' stacks
 */
class ANGEVESSEL_EXPORT Stack : public QObject {

    Q_OBJECT

public:
    /**
     * Construct Stack
     * @param bottom
     *   position of the bottom of the stack
     * @param stackSupport*
     *   Stack supports each stack support can be eighter 20' or 40' stack holding information about weight, and height.
     */
    Stack(const Point3D& bottom, DeckLevel level,
          StackSupport* stackSupport20Fore, StackSupport* stackSupport20Aft, StackSupport* stackSupport40);

    /**
     * dtor
     */
    ~Stack();

    /**
     * @return the parent BaySlice
     */
    BaySlice* baySlice() const;

    /**
     * @return a list from bottom to top, fore to aft of stack describing the slots
     */
    QList<Slot*> stackSlots() const;

    /**
     * Return bottom of lowest slot
     */
    const Point3D& bottom() const ;

    /**
     * @return a list of the stack support connected to this stack, The list include two 20' stacks and one 40' stack.
     * Will contain nullptrs if the supports are missing!
     */
    QList<StackSupport*> stackSupports() const;

    /**
     * @return fore 20' stack support (null if not applicable)
     */
    StackSupport* stackSupport20Fore() const;

    /**
     * @return aft 20' stack support (null if not applicable)
     */
    StackSupport* stackSupport20Aft() const;

    /**
     * @return fore 20' stack support (null if not applicable)
     */
    StackSupport* stackSupport40() const;

    /**
     * Gets the slot at position brt, will return 0 if there is no such slot.
     * When getting a slot at a 40'-position it will return the fore most 20' slot.
     * @param brt
     *   bay/row/tier position
     */
    Slot* slotAt(BayRowTier brt) const;

    /**
     * The names of the bays this stack covers
     */
    QList<int> bays() const ;

    /**
     * The name of the row this stack covers
     */
    int row() const ;

    /**
     * The names of the tiers this stack covers
     */
    const QList<int>& tiers() const ;

    /**
     * @return under or over deck
     */
    DeckLevel level() const ;

    /**
     * @return textual representation of this (mostly for debugging purposes)
     */
    QString toString() const;

    /**
     * @return port neighbour, or null if there is not neighbours port
     */
    Stack* portNeighbour() const;

    /**
     * @return starboard neighbour, or null if there is not neighbours port
     */
    Stack* starboardNeighbour() const;

    /**
     * @return the maximum total weight that can be fit into this stack.
     */
    ange::units::Mass maximumTotalWeight() const;

    /**
     * @return compartment for stack, or null if stack isn't part of any compartment
     **/
    Compartment* compartment() const;

    /**
     * @return true if twinlifting is supported in this stack
     */
    bool isTwinliftSupported() const;

    /**
     * @return true if the stack supports russion stowage.
     */
    bool canRussian() const;

    /**
     * Sets the twinlift capabilities above deck in this bayslice.
     */
    void setTwinlifting(bool twinlifting);

    /**
     * @return lids on on top of stack (if any)
     */
    QList<HatchCover*> lidsAbove() const;

    /**
     * @return lids underneath stack (if any)
     */
    QList<HatchCover*> lidsBelow() const;

    /**
     * negates the stack's longitudinal position
     * used when inverting the vessel's longitudinal axis
     */
    void mirrorLongitudinally(const ange::units::Length& medianStackLcg);

    /**
     * sets the stack's slots as being clear of living quarters (or not)
     * (used when placing DG)
     */
    void setClearOfLivingQuarters(bool isClear);

    /**
     * @return whether the stack is clear of living quarters
     */
    bool clearOfLivingQuarters() const;

    /**
     * @return if the DG is forbidden in this stack (by quering the StackSupports)
     * In the vessel profile file format, whether a stack does not allow DG (e.g.
     * outer rows) is defined for the Stack Supports
     */
    bool imoForbidden() const;

    /**
     * Sets the forbidden IMO classes for this stack
     */
    void setForbiddenImoClasses(const QSet<QString>& forbiddenImoClasses);

    /**
     * Sets the allowed IMO classes for this stack
     * This data is never used
     */
    void setAllowedImoClasses(const QSet<QString>& allowedImoClasses);

    /**
     * @returns whether or not all the IMO classes in @param imoClasses are allowed in this stack.
     * Only checks forbiddenImoClasses
     */
    bool imoClassesAllowed(const QSet<QString>& imoClasses) const;

    /**
     * @return the stack_support containing @param bayNumber and null if not found
     */
    const ange::vessel::StackSupport* stackSupportContaining(int bayNumber) const;

private:

    /**
     * Sets the compartment the stack is currently a member of
     */
    void setCompartment(Compartment* compartment);

    void addLidAbove(HatchCover* lid);
    void addLidBelow(HatchCover* lid);

private:
    QScopedPointer<StackPrivate> d;

private:
    using QObject::setParent; // Make changes to parent less likely
    friend class BaySlice; // Call setParent
    friend class HatchCover; // To be able to update lids information
    friend class Compartment; // To be able to register compartment for stacks

};

ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const Stack& stack);
ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const Stack* stack);

}
}

#endif // VESSEL_STACK_H
