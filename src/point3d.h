#ifndef VESSEL_POINT3D_H
#define VESSEL_POINT3D_H

#include "angevessel_export.h"
#include <ange/units/units.h>

#include <QDebug>

namespace ange {
namespace vessel {

/**
 * Point in 3D space
 * the origin of the coordinate system is defined in the vessel profile
 */
class ANGEVESSEL_EXPORT Point3D {

public:

    /**
     * Create a point the 3D space.
     * l: longitudinal position
     * v: vertical position
     * t: transverse position
     */
    Point3D(ange::units::Length l, ange::units::Length v, ange::units::Length t);

    /**
     * Default constructor, creates point in (0, 0, 0)
     */
    Point3D();

    /**
     * Increases normally from aft to fore.
     * But notice that the direction can be opposite in vessel profiles, see Vessel::isOrientedAftFwd()
     * @return longitudinal component
     */
    ange::units::Length l() const {
        return m_l;
    }

    void setL(ange::units::Length l) {
        m_l = l;
    }

    /**
     * Increases from bottom to top
     * @return vertical component
     */
    ange::units::Length v() const {
        return m_v;
    }

    void setV(ange::units::Length v) {
        m_v = v;
    }

    /**
     * Point3D has positive TCG to starboard (but JSON format has positive TCG to port, see #1345)
     * @return transverse component
     */
    ange::units::Length t() const {
        return m_t;
    }

    void setT(ange::units::Length t) {
        m_t = t;
    }

    /**
     * The distance from origo
     */
    ange::units::Length length() const;

    /**
     * Debug string
     */
    QString toString() const;

private:
    ange::units::Length m_l;
    ange::units::Length m_v;
    ange::units::Length m_t;
};

inline bool operator==(const Point3D& lhs, const Point3D& rhs) {
    return lhs.l() == rhs.l() && lhs.v() == rhs.v() && lhs.t() == rhs.t();
}

inline Point3D operator+(const Point3D& lhs,const Point3D& rhs) {
    return Point3D(lhs.l() + rhs.l(), lhs.v() + rhs.v(), lhs.t() + rhs.t());
}

inline Point3D operator-(const Point3D& lhs,const Point3D& rhs) {
    return Point3D(lhs.l() - rhs.l(), lhs.v() - rhs.v(), lhs.t() - rhs.t());
}

ANGEVESSEL_EXPORT QDebug operator<<(QDebug str, const Point3D& point3d);

}} // end of namespaces

#endif // VESSEL_POINT3D_H
