#include "vesseltank.h"

#include "bilinearinterpolator.h"
#include "vessel.h"

using namespace ange::units;
namespace ange {
namespace vessel {

class TankPrivate : public QSharedData {
  public:
    TankPrivate(ange::vessel::Vessel* vessel, const QString& description, const QString& tankgroup, qreal massCapacity,
                qreal volCapacity, qreal density, qreal foreEnd, qreal aftEnd, bool is_ballast)
      : QSharedData(),
        m_vessel(vessel),
        description(description),
        tank_group(tankgroup),
        massCapacity(massCapacity),
        volCapacity(volCapacity),
        density(density),
        foreEnd(foreEnd),
        aftEnd(aftEnd),
        m_is_ballast(is_ballast) {
    }
    Vessel* m_vessel;
    QString description;
    QString tank_group;
    qreal massCapacity;
    qreal volCapacity;
    qreal density;
    qreal foreEnd;
    qreal aftEnd;
    bool m_is_ballast; //whether this tank can be used for ballasting
    BilinearInterpolator m_lcg;
    BilinearInterpolator m_vcg;
    BilinearInterpolator m_tcg;
    BilinearInterpolator m_fsm;
    void initialize(const BilinearInterpolator& lcg, const BilinearInterpolator& vcg,
                    const BilinearInterpolator& tcg, const BilinearInterpolator& fsm) {
        m_lcg = lcg;
        m_vcg = vcg;
        m_tcg = tcg;
        m_fsm = fsm;
    }
};

VesselTank::VesselTank(QString description, QString tank_group, qreal massCapacity, qreal volCapacity,
               qreal density, qreal foreEnd, qreal aftEnd,
               const BilinearInterpolator& lcg, const BilinearInterpolator& vcg,
               const BilinearInterpolator& tcg, const BilinearInterpolator& fsm,
               bool is_ballast) :
    Identifiable< ange::vessel::VesselTank >(-1),
    d(new TankPrivate(0L, description, tank_group, massCapacity, volCapacity, density, foreEnd, aftEnd, is_ballast)) {
    d->initialize(lcg, vcg, tcg, fsm);
}

VesselTank::~VesselTank() {
    // Empty
}

Vessel* VesselTank::vessel() const {
    return d->m_vessel;
}

QString VesselTank::description() const {
    return d->description;
}

qreal VesselTank::capacity() const {
    return std::min(d->massCapacity, d->volCapacity*d->density);
}

qreal VesselTank::massCapacity() const {
    return d->massCapacity;
}

qreal VesselTank::volCapacity() const {
    return d->volCapacity;
}

qreal VesselTank::density() const {
    return d->density;
}

qreal VesselTank::lcg(ange::units::Mass weight) const {
    return d->m_lcg.valueAt(weight/kilogram / d->density, 0.0);
}

qreal VesselTank::tcg(ange::units::Mass weight) const {
    return d->m_tcg.valueAt(weight/kilogram / d->density, 0.0);
}

qreal VesselTank::vcg(ange::units::Mass weight) const {
    return d->m_vcg.valueAt(weight/kilogram / d->density, 0.0);
}

qreal VesselTank::fsm(ange::units::Mass weight) const {
    return d->m_fsm.valueAt(weight/kilogram / d->density, 0.0);
}

qreal VesselTank::aftEnd() const {
    return d->aftEnd;
}

qreal VesselTank::foreEnd() const {
  return d->foreEnd;
}

bool VesselTank::extentSet() {
  if (foreEnd() == aftEnd()) {
    return false;
  }
  return true;
}

bool VesselTank::isBallast() const {
  return d->m_is_ballast;
}


const ange::vessel::BilinearInterpolator& VesselTank::fsmData() const {
  return d->m_fsm;
}

const ange::vessel::BilinearInterpolator& VesselTank::lcgData() const {
  return d->m_lcg;
}

const ange::vessel::BilinearInterpolator& VesselTank::tcgData() const {
  return d->m_tcg;
}

const ange::vessel::BilinearInterpolator& VesselTank::vcgData() const {
  return d->m_vcg;
}

QString VesselTank::tankGroup() const {
  return d->tank_group;
}

bool VesselTank::operator==(const ange::vessel::VesselTank& rhs) const {
  if (id() == rhs.id()) {
    return true;
  }
  if (density() != rhs.density()) {
    return false;
  }
  if (capacity() != rhs.capacity()) {
    return false;
  }
  if (description() != rhs.description()) {
    return false;
  }
  if (tankGroup() != rhs.tankGroup()) {
    return false;
  }
  if (!qFuzzyCompare(tcg(0*kilogram), rhs.tcg(0*kilogram))) {
    return false;
  }
  if (!qFuzzyCompare(lcg(0*kilogram), rhs.lcg(0*kilogram))) {
    return false;
  }
  if (!qFuzzyCompare(vcg(0*kilogram), rhs.vcg(0*kilogram))) {
    return false;
  }
  return true;
}

void VesselTank::mirrorLongitudinally() {
    d->m_lcg.mirrorValues();
    d->aftEnd *= -1;
    d->foreEnd *= -1;
}

void VesselTank::setVessel(Vessel* vessel) {
    d->m_vessel = vessel;
}

QDebug operator<<(QDebug dbg, const ange::vessel::VesselTank* tank) {
  if (tank) {
    return dbg << *tank;
  } else {
    return dbg << "<null VesselTank>";
  }
}

QDebug operator<<(QDebug dbg, const ange::vessel::VesselTank& tank) {
  return dbg << tank.description() << ":(" << (tank.capacity()) << "m^3/" << tank.density() / 1000.0 << "ton pr m^3)";
}

} // vessel
} // ange
