#ifndef LIBANGEVESSEL_SLOT_H
#define LIBANGEVESSEL_SLOT_H

#include "angevessel_export.h"
#include "bayrowtier.h"
#include "point3d.h"

#include <QObject>
#include <QScopedPointer>

namespace ange {
namespace vessel {
class StackSupport;
class Stack;
class Vessel;
class SlotPrivate;
}
}

namespace ange {
namespace vessel {

/**
 * Basic description of a slot as a physical (20') volume on the vessel
 */
class ANGEVESSEL_EXPORT Slot : public QObject {

    Q_OBJECT

public:

    /**
     * What content the slot allows. Default (0) is that it doesn't allow
     */
    enum Capability {
        ReeferCapable     = 0x01,
        TwentyCapable     = 0x02,
        FourtyCapable     = 0x04,
        FourtyFiveCapable = 0x08,
        FiftyThreeCapable = 0x10,
    };
    Q_ENUMS(Capability);
    Q_DECLARE_FLAGS(Capabilities, Capability);
    Q_FLAGS(Capabilities);

    /**
     * Constructor
     * @param pos
     *   Position of the bottom middle of the slot
     * @param brt
     *   Name of the slot
     *   By default slots are clear of living quarters and allow DG
     */
    Slot(const Point3D& pos, const BayRowTier& brt);

    ~Slot();

    /*
     * @return the parent stack
     */
    Stack* stack() const;

    /**
     * @return position of bottom, middle of slot.
     */
    const Point3D& pos() const;

    /**
     * @return name of the slot.
     */
    const BayRowTier& brt() const;

    /**
     * @return the sister slot, or 0 if this is an odd-slot.
     */
    Slot* sister() const;

    /**
     * join sister slots. Sister will became slave to this.
     */
    void joinSisterAsSlave(Slot* sister);

    /**
     * @returns true if this slot has all the required @param capability
     */
    bool hasCapability(ange::vessel::Slot::Capability capability) const;

    /**
     * adds the given @param capability to the slot
     */
    void addCapability(ange::vessel::Slot::Capability capability);

    /**
     * adds the given @param capability to the slot
     */
    void addCapabilities(ange::vessel::Slot::Capabilities capabilities);

    /**
     * removes the given @param capability to the slot
     */
    void removeCapability(ange::vessel::Slot::Capability capability);

    /**
     * @return the 20' support for slot, or null if no such support
     */
    StackSupport* support20() const;

    /**
     * @return the 4x' support for slot, or null if no such support (e.g. cell guides)
     */
    StackSupport* support40() const;

    /**
     * @return true if the slot is aft in the bay and false if it is fwd.
     */
    bool isAft() const;

    /**
     * @return true if the slot is fore in the bay and false if it is aft.
     */
    bool isFore() const;

    /**
     * @return true if the slot is and odd slot
     */
    bool isOddSlot() const;

    /**
     * negates the longitudinal position of the slot and uses @param medianStackLcg to determine fore/aftness
     */
    void mirrorLongitudinally(const ange::units::Length& medianStackLcg);

    /**
     * set the @param stack of the slot and use @param bayStacksLcg to determine
     * fore/aftness of the slot
     */
    void setStack(Stack* stack, const ange::units::Length& bayStacksLcg);

Q_SIGNALS:
    /**
     * Emitted when something has changed in this slot
     */
    void changed();

private:
    void setSupport20(StackSupport* support);
    void setSupport40(ange::vessel::StackSupport* support);

private:
    QScopedPointer<SlotPrivate> d;

private:
    using QObject::setParent; // Make changes to parent less likely
    friend class SlotPrivate;
    friend class StackSupport;

};

/**
 * Slot representing the unknown position onboard the vessel
 * XXX This should be part of the vessel, should be moved to a method on Vessel
 */
ANGEVESSEL_EXPORT extern const Slot* const UnknownPosition;

ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const Slot& slot);
ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const Slot* slot);

Q_DECLARE_OPERATORS_FOR_FLAGS(Slot::Capabilities);

}} // namespace ange::vessel

#endif // VESSEL_SLOT_H
