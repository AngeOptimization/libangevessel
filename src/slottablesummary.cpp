#include "slottablesummary.h"

#include "bayslice.h"
#include "slot.h"
#include "stack.h"
#include "vessel.h"

namespace ange {
namespace vessel {

class SlotTableSummary::SlotTableSummaryPrivate {
public:
    int m_totalTEU;
    int m_nbReeferPlugs;
};

SlotTableSummary::SlotTableSummary(const QList<BaySlice*>& baySlices) : d(new SlotTableSummaryPrivate()) {
    d->m_totalTEU = 0;
    d->m_nbReeferPlugs = 0;
    Q_FOREACH(const BaySlice* baySlice, baySlices) {
        Q_FOREACH(const Stack* stack, baySlice->stacks()) {
            Q_FOREACH(const Slot* slot, stack->stackSlots()) {
                d->m_totalTEU++;
                if(slot->hasCapability(Slot::ReeferCapable)) {
                    d->m_nbReeferPlugs++;
                }
            }
        }
    }
}

int SlotTableSummary::totalTEU() const {
    return d->m_totalTEU;
}

int SlotTableSummary::reeferPlugs() const {
    return d->m_nbReeferPlugs;
}

SlotTableSummary::SlotTableSummary(const SlotTableSummary& other) : d(other.d.data()) {}

SlotTableSummary::~SlotTableSummary(){}

}// namespace vessel
}// namespace ange
