#include "direction.h"

namespace ange {
namespace vessel {

namespace Direction {


// Aft / Fore:

QList<AftFore> aftForeValues() {
    return QList<AftFore>() << Aft << Fore;
}

QString toString(AftFore end) {
    switch (end) {
        case Aft:
            return "Aft";
        case Fore:
            return "Fore";
    }
    Q_ASSERT(false);
    return "INVALID";
}

AftFore toAftFore(const QString& string, bool* ok) {
    if (string == "Aft") {
        *ok = true;
        return Aft;
    } else if (string == "Fore") {
        *ok = true;
        return Fore;
    } else {
        *ok = false;
        return static_cast<AftFore>(-1);
    }
}


// Starboard / Port:

QList<StarboardPort> starboardPortValues() {
    return QList<StarboardPort>() << Starboard << Port;
}

QString toString(StarboardPort side) {
    switch (side) {
        case Starboard:
            return "Starboard";
        case Port:
            return "Port";
    }
    Q_ASSERT(false);
    return "INVALID";
}

StarboardPort toStarboardPort(const QString& string, bool* ok) {
    if (string == "Starboard") {
        *ok = true;
        return Starboard;
    } else if (string == "Port") {
        *ok = true;
        return Port;
    } else {
        *ok = false;
        return static_cast<StarboardPort>(-1);
    }
}


// Top / Bottom:

QList<TopBottom> topBottomValues() {
    return QList<TopBottom>() << Top << Bottom;
}

QString toString(TopBottom topBottom) {
    switch (topBottom) {
        case Top:
            return "Top";
        case Bottom:
            return "Bottom";
    }
    Q_ASSERT(false);
    return "INVALID";
}

TopBottom toTopBottom(const QString& string, bool* ok) {
    if (string == "Top") {
        *ok = true;
        return Top;
    } else if (string == "Bottom") {
        *ok = true;
        return Bottom;
    } else {
        *ok = false;
        return static_cast<TopBottom>(-1);
    }
}


} // namespace Direction

}} // namespace ange::angelstow
