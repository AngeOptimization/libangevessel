#ifndef CARGOHOLD_H
#define CARGOHOLD_H

#include "angevessel_export.h"
#include "decklevel.h"

#include <QObject>
#include <QSharedPointer>

namespace ange {
namespace vessel {

class Vessel;
class BaySlice;

/**
 * A class representing a cargo hold (generally 2 or 3 40' bays)
 */
class ANGEVESSEL_EXPORT  CargoHold : public QObject {
    Q_OBJECT

public:
    /**
     * Create empty bulkhead
     **/
    explicit CargoHold(QObject* parent = 0);

    virtual ~CargoHold();

    /**
     * Add bayslice to bulk_head. This will also set the bulkhead of the bay_slice to this
     **/
    void addBayslice(BaySlice* bay_slice);

    /**
     * Remove bayslice from bulkhead. This will also set the bulkhead of the bay_slice to null
     **/
    void removeBayslice(BaySlice* bay_slice);

    /**
     * @return list of bayslices in bulkhead
     */
    QList<BaySlice*> baySlices() const;

    /**
     * Forbid an @param imoClass above or below deck (@param level)
     */
    void addForbiddenImoClass(const QString& imoClass, ange::vessel::DeckLevel level);

    /**
     * @returns the forbidden IMO classes in this hold above/ below deck (@param level)
     */
    QSet<QString> forbiddenImoClasses(ange::vessel::DeckLevel level) const;

    /**
     * Accept an @param imoClass above or below deck (@param level)
     */
    void addAcceptedImoClass(const QString& imoClass, ange::vessel::DeckLevel level);

    /**
     * @returns the accepted IMO classes in this hold above/below deck (@param level)
     */
    QSet<QString> acceptedImoClasses(ange::vessel::DeckLevel level) const;

private:
    class CargoHoldPrivate;
    QSharedPointer<CargoHoldPrivate> d;
};

ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const ange::vessel::CargoHold& hold);
ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const ange::vessel::CargoHold* hold);

}} // end namespaces

#endif // CARGOHOLD_H
