#include "point3d.h"

using namespace ange::units;

namespace ange {
namespace vessel {

Point3D::Point3D(Length l, Length v, Length t)
    : m_l(l), m_v(v), m_t(t)
{
    // Empty
}

Point3D::Point3D()
    : m_l(0 * meter), m_v(0 * meter), m_t(0 * meter)
{
    // Empty
}

Length Point3D::length() const {
    return sqrt((square(m_l) + square(m_v) + square(m_t)) / meter2) * meter;
}

QString Point3D::toString() const {
    return QString("(%1,%2,%3)").arg(m_l / meter).arg(m_v / meter).arg(m_t / meter);
}

QDebug operator<<(QDebug str, const Point3D& point3d) {
    return str << point3d.toString();
}

}} // namespaces
