#include "bayrowtier.h"

#include <QString>
#include <QTextStream>

#include <stdexcept>

namespace ange {
namespace vessel {

BayRowTier::BayRowTier(int bay, int row, int tier) :
    m_bay(bay), m_row(row), m_tier(tier)
{
    // Empty
}

BayRowTier::BayRowTier() :
    m_bay(0), m_row(0), m_tier(0)
{
    // Empty
}

namespace {
  int brt_entry_cast(const QString& string) {
    bool ok;
    int i = string.toInt(&ok);
    if (not ok) {
      throw std::runtime_error("uccast failed, string \""+string.toStdString()+"\" is bad, was probably caused by invalid JSON data");
    }
    if (i < 0) {
      throw std::runtime_error(QString("uccast failed, %1 less than 0, was probably caused by invalid JSON data").arg(i).toStdString());
    }
    if (i > 99) {
      throw std::runtime_error(QString("uccast failed, %1 more than 99, was probably caused by invalid JSON data").arg(i).toStdString());
    }
    return i;
  }
}

BayRowTier::BayRowTier(const QString& bay, const QString& row, const QString& tier)
  :  m_bay(brt_entry_cast(bay)),
     m_row(brt_entry_cast(row)),
     m_tier(brt_entry_cast(tier))
{
    // Empty
}

bool BayRowTier::placed() const {
  return m_bay != 0 || m_row != 0 || m_tier != 0;
}

QString BayRowTier::toString() const {
    const int size = 12;
    char buffer[size];
    int rv = snprintf(buffer, size, "%d,%d,%d", m_bay, m_row, m_tier);
    if (rv >= size) {
        throw std::runtime_error("truncate in toString()");
    }
    return QString::fromLatin1(buffer);
}

QDebug operator<<(QDebug dbg, const BayRowTier& brt) {
  dbg.nospace() << brt.bay() << "," << brt.row() << "," << brt.tier();
  return dbg.space();
}

}} // namespace ange::vessel

QTextStream& operator<<(QTextStream& stream, const ange::vessel::BayRowTier& brt) {
  return stream << brt.bay() << "," << brt.row() << "," << brt.tier();
}
