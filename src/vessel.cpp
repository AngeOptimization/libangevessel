#include "vessel.h"

#include "bayrowtier.h"
#include "bayslice.h"
#include "bilinearinterpolator.h"
#include "blockweight.h"
#include "cargohold.h"
#include "compartment.h"
#include "hatchcover.h"
#include "lashingpatterndata.h"
#include "slot.h"
#include "slottablesummary.h"
#include "stack.h"
#include "vesseltank.h"

using namespace ange::units;

namespace ange {
namespace vessel {

namespace {
  struct sort_by_bay_number {
    bool operator()(const BaySlice* lhs, const BaySlice* rhs) {
      return lhs->joinedBay() < rhs->joinedBay();
    }
  };
}

class VesselPrivate {
  public:
    VesselPrivate(const QList<BaySlice*>& bay_slices,
                     const QList<HatchCover*>& lids,
                     const QList<VesselTank*>& tanks,
                     const QString& name,
                     const QString& imo_number,
                     const QString& callsign,
                     const QString& vessel_code) :
        m_bay_slices(bay_slices),
        m_lids(lids),
        m_tanks(tanks),
        m_name(name),
        m_imo_number(imo_number),
        m_callsign(callsign),
        m_vessel_code(vessel_code),
        m_hull_weight(0.0),
        m_hull_lcg(0.0),
        m_hull_vcg(0.0),
        m_vessel_lpp(qQNaN()),
        m_observer_lcg(qQNaN()),
        m_observer_vcg(qQNaN()),
        m_breadth(0.0),
        m_slotSummary(bay_slices),
        m_aftOverAll(qInf()),
        m_foreOverAll(-qInf()),
        m_lashingPatternData(0)
{
  qSort(m_bay_slices.begin(), m_bay_slices.end(), sort_by_bay_number());
}

    ~VesselPrivate() {
        qDeleteAll(m_lids);
        qDeleteAll(m_tanks);
        delete m_lashingPatternData;
    }

  public:
    BaySlice* baySliceContaining(int bay);
    /**
     * List of bay slices - should be kept sorted according to its longitudinal positions
     */
    QList<BaySlice*> m_bay_slices;
    QList<HatchCover*> m_lids;
    QList<VesselTank*> m_tanks;
    QList<VesselTank*> m_ballast_tanks;
    QHash<QString,QList<VesselTank*> > m_tanks_by_group;
    QHash<int, VesselTank*> m_tanksById;
    QHash<QString, VesselTank*> m_tanksByDescription;
    QString m_name;
    QString m_imo_number;
    QString m_callsign;
    QString m_vessel_code;
    BilinearInterpolator m_trim;
    BilinearInterpolator m_draft;
    BilinearInterpolator m_bonjean;
    BilinearInterpolator m_metaCenterFunction;
    BilinearInterpolator m_maxBendingLimits;
    BilinearInterpolator m_minBendingLimits;
    BilinearInterpolator m_maxTorsionLimits;
    BilinearInterpolator m_minTorsionLimits;
    BilinearInterpolator m_maxShearForceLimits;
    BilinearInterpolator m_minShearForceLimits;
    ange::units::Mass m_hull_weight;
    ange::units::Length m_hull_lcg;
    ange::units::Length m_hull_vcg;
    ange::units::Length m_vessel_lpp;
    ange::units::Length m_observer_lcg;
    ange::units::Length m_observer_vcg;
    ange::units::Length m_breadth;
    QList<CargoHold*> m_cargoHolds;
    QList<BlockWeight> m_hull_weight_data;
    QList<BlockWeight> m_constantWeightData;
    QSet<QString> m_tank_groups;
    QString m_ballast_tank_group;
    const SlotTableSummary m_slotSummary;
    QHash<BayRowTier, Slot*> m_brt_slot_map;
    ange::units::Length m_aftOverAll;
    ange::units::Length m_foreOverAll;
    QString m_classificationSociety;
    const LashingPatternData* m_lashingPatternData;
};

const Density Vessel::oceanWaterDensity = 1025 * kilogram_per_meter3;

BaySlice* VesselPrivate::baySliceContaining(int bay) {
  Q_FOREACH (BaySlice* bay_slice, m_bay_slices) {
    if (bay_slice->allBays().contains(bay)) {
      return bay_slice;
    }
  }
  return 0;
}

Vessel::Vessel(QList< BaySlice* > bay_slices,
                   QList< HatchCover* > lids,
                   QList< VesselTank* > tanks,
                   const QString& name,
                   const QString& imo_number,
                   const QString& callsign,
                   const QString& vessel_code,
                   QObject* parent):
    QObject(parent),
    d(new VesselPrivate(bay_slices, lids, tanks, name, imo_number, callsign, vessel_code)) {
  setup();
}

Vessel::~Vessel() {
  // Do nothing, needed for use in QScopedPointer
}

void Vessel::setup() {
  Q_FOREACH(BaySlice* bay_slice, baySlices()) {
    bay_slice->setParent(this);
  }
  QHash<const Stack*, QList<HatchCover*> > lids_below_for_stack;
  QHash<const Stack*, QList<HatchCover*> > lids_above_for_stack;
  Q_FOREACH(HatchCover* lid, lids()) {
        Q_FOREACH (const Stack* stack, lid->stacksAbove()) {
            lids_below_for_stack[stack] << lid;
        }
        Q_FOREACH (const Stack* stack, lid->stacksBelow()) {
            lids_above_for_stack[stack] << lid;
        }
  }
  Q_FOREACH(BaySlice* bay_slice, baySlices()) {
    QList<Stack*> stacks = bay_slice->stacks();
    if (stacks.empty()) {
      continue;
    }
    QList<Compartment*> compartments;
    Stack* stack = stacks.front();
    QList<HatchCover*> lids_below_stack(lids_below_for_stack.value(stack));
    QList<HatchCover*> lids_above_stack(lids_above_for_stack.value(stack));
    QList<Stack*> compartment_stacks;
    compartment_stacks << stack;
    stacks.pop_front();
    DeckLevel level = stack->level();
    while (!stacks.empty()) {
      stack = stacks.front();
      stacks.pop_front();
      if (lids_below_stack != lids_below_for_stack[stack] ||
                    lids_above_stack != lids_above_for_stack[stack] ||
          level != stack->level()) {
        Q_ASSERT(!compartment_stacks.empty());
        compartments << new Compartment(bay_slice, compartment_stacks, lids_above_stack, lids_below_stack);
                lids_below_stack = lids_below_for_stack[stack];
                lids_above_stack = lids_above_for_stack[stack];
        level = stack->level();
        compartment_stacks.clear();
        compartment_stacks << stack;
      } else {
        compartment_stacks << stack;
      }
      Q_FOREACH(Slot* slot, stack->stackSlots()) {
          d->m_brt_slot_map[slot->brt()] = slot;
      }
      d->m_aftOverAll = qMin(d->m_aftOverAll,stack->bottom().l());
      d->m_foreOverAll = qMax(d->m_foreOverAll,stack->bottom().l());
    }
    if (!compartment_stacks.empty()) {
      compartments << new Compartment(bay_slice, compartment_stacks, lids_above_stack, lids_below_stack);
    }
    bay_slice->setCompartments(compartments);
  }
  // Install tanks into vessel
  d->m_tank_groups.clear();
  QHash<QString, qreal> tankGroupMass;
  Q_FOREACH(VesselTank* tank, tanks()) {
    tank->setVessel(this);
    d->m_tank_groups << tank->tankGroup();
    d->m_tanks_by_group[tank->tankGroup()] << tank;
    d->m_tanksByDescription[tank->description()] = tank;
    d->m_tanksById[tank->id()] = tank;
    tankGroupMass[tank->tankGroup()]+= tank->capacity();
    if(tank->isBallast()){
      d->m_ballast_tanks << tank;
    }
    d->m_aftOverAll = qMin(d->m_aftOverAll,tank->aftEnd()*meter);
    d->m_foreOverAll = qMax(d->m_foreOverAll,tank->foreEnd()*meter);
  }
    qreal maxCapacity = -1;
    Q_FOREACH(QString tankgroup, d->m_tank_groups) {
        qreal capacity = tankGroupMass.value(tankgroup);
        if(maxCapacity < capacity) {
            d->m_ballast_tank_group = tankgroup;
            maxCapacity = capacity;
        }
    }
}

const VesselTank* Vessel::tankByDescription(const QString& description) const {
    return d->m_tanksByDescription.value(description);
}

const VesselTank* Vessel::tankById(int id) const {
    return d->m_tanksById.value(id);
}

BaySlice* Vessel::baySliceContaining(int bay){
    return d->baySliceContaining(bay);
}

const BaySlice* Vessel::baySliceContaining(int bay) const {
    return d->baySliceContaining(bay);
}

const BaySlice* Vessel::baySliceContaining(const BayRowTier& brt) const {
  return baySliceContaining(brt.bay());
}

const Slot* Vessel::slotAt(const ange::vessel::BayRowTier& brt) const {
  const BaySlice* bay_slice = baySliceContaining(brt);
  if (bay_slice == 0) {
    return d->m_brt_slot_map.value(brt);
  }
  Stack* stack = bay_slice->stackContaining(brt);
  if (stack == 0) {
    return d->m_brt_slot_map.value(brt);
  }
  return stack->slotAt(brt);
}

const QList< BaySlice* > Vessel::baySlices() const {
    return d->m_bay_slices;
}

QString Vessel::name() const {
  return d->m_name;
}

QList< HatchCover* > Vessel::lids() const {
  return d->m_lids;
}

QString Vessel::callSign() const {
  return d->m_callsign;
}

QString Vessel::imoNumber() const {
  return d->m_imo_number;
}

QString Vessel::vesselCode() const {
  return d->m_vessel_code;
}

QList< VesselTank* > Vessel::tanks() const {
  return d->m_tanks;
}

QList< VesselTank* > Vessel::tankByTankGroup(const QString& tankgroup) const {
  return d->m_tanks_by_group.value(tankgroup);
}

void Vessel::setDraftData(const ange::vessel::BilinearInterpolator& draftTable) {
    d->m_draft = draftTable;
}

void Vessel::setTrimData(const ange::vessel::BilinearInterpolator& trimTable) {
    d->m_trim = trimTable;
}

void Vessel::setBonjeanData(const ange::vessel::BilinearInterpolator& bonjeanData) {
    d->m_bonjean = bonjeanData;
}

void Vessel::setMetacenterData(const ange::vessel::BilinearInterpolator& metacentreData) {
    d->m_metaCenterFunction = metacentreData;
}

void Vessel::setStressLimits(const ange::vessel::BilinearInterpolator& maxBendingLimits,
                               const ange::vessel::BilinearInterpolator& minBendingLimits,
                               const ange::vessel::BilinearInterpolator& maxShearForceLimits,
                               const ange::vessel::BilinearInterpolator& minShearForceLimits,
                               const ange::vessel::BilinearInterpolator& maxTorsionLimits,
                               const ange::vessel::BilinearInterpolator& minTorsionLimits) {
    d->m_maxBendingLimits = maxBendingLimits;
    d->m_minBendingLimits = minBendingLimits;
    d->m_maxShearForceLimits = maxShearForceLimits;
    d->m_minShearForceLimits = minShearForceLimits;
    d->m_maxTorsionLimits = maxTorsionLimits;
    d->m_minTorsionLimits = minTorsionLimits;
}

void Vessel::setLightshipWeights(const QList< BlockWeight >& blocks) {
  d->m_hull_weight_data=blocks;
}

void Vessel::setConstantWeights(const QList< BlockWeight >& blocks) {
  d->m_constantWeightData=blocks;
}

Length Vessel::draft(Mass weight, Length lcg, Density waterDensity) const {
    if(d->m_draft.isValid()) {
        return d->m_draft.valueAt((weight*Vessel::oceanWaterDensity)/(kilogram*waterDensity), lcg/meter)*meter;
    }
    return qreal(0.0)*meter;
}

Length Vessel::trim(Mass weight, Length lcg, Density waterDensity) const {
    if(d->m_trim.isValid()) {
        return d->m_trim.valueAt((weight*Vessel::oceanWaterDensity)/(kilogram*waterDensity), lcg/meter)*meter;
    }
    return qreal(0.0)*meter;
}

Unit< 0, 2, 0 > Vessel::bonjean(Length lcg, Length draft) const {
  if (d->m_bonjean.isValid()) {
    return d->m_bonjean.valueAt(lcg/meter, draft/meter)*(meter*meter);
  }
  return 0.0*meter*meter;
}

Length Vessel::metacenter(Length trim, Length draft) const {
  if (d->m_metaCenterFunction.isValid()) {
    return d->m_metaCenterFunction.valueAt(trim/meter, draft/meter)*meter;
  }
  return qreal(-1000.0)*meter;
}

Mass Vessel::lightshipWeight() const {
    return d->m_hull_weight;
}

qreal Vessel::maxBendingLimit(Length lcg) const {
  if (d->m_maxBendingLimits.isValid()) {
    return d->m_maxBendingLimits.valueAt(lcg/meter, 0.0);
  }
  return qreal(0.0);
}

qreal Vessel::minBendingLimit(Length lcg) const {
  if (d->m_minBendingLimits.isValid()) {
    return d->m_minBendingLimits.valueAt(lcg/meter, 0.0);
  }
  return qreal(0.0);
}

qreal Vessel::maxShearForceLimit(Length lcg) const {
  if (d->m_maxShearForceLimits.isValid()) {
    return d->m_maxShearForceLimits.valueAt(lcg/meter, 0.0);
  }
  return qreal(0.0);
}

qreal Vessel::minShearForceLimit(Length lcg) const {
  if (d->m_minShearForceLimits.isValid()) {
    return d->m_minShearForceLimits.valueAt(lcg/meter, 0.0);
  }
  return qreal(0.0);
}

qreal Vessel::maxTorsionLimit(Length lcg) const {
  if (d->m_maxTorsionLimits.isValid()) {
    return d->m_maxTorsionLimits.valueAt(lcg/meter, 0.0);
  }
  return qreal(0.0);
}

qreal Vessel::minTorsionLimit(Length lcg) const {
  if (d->m_minTorsionLimits.isValid()) {
    return d->m_minTorsionLimits.valueAt(lcg/meter, 0.0);
  }
  return qreal(0.0);
}

ange::units::Length Vessel::hullLcg() const {
  return d->m_hull_lcg;
}

ange::units::Length Vessel::hullVcg() const {
  return d->m_hull_vcg;
}

Length Vessel::lpp() const {
    return d->m_vessel_lpp;
}

Length Vessel::observerLcg() const {
  return d->m_observer_lcg;
}

Length Vessel::observerVcg() const {
  return d->m_observer_vcg;
}

void Vessel::setHullLcg(Length lcg) {
  d->m_hull_lcg=lcg;
}

void Vessel::setHullVcg(const Length vcg) {
    d->m_hull_vcg=vcg;
}

void Vessel::setClassificationSociety(const QString& society) {
    d->m_classificationSociety = society;
}

void Vessel::setLightshipWeight(Mass weight) {
  d->m_hull_weight=weight;
}

void Vessel::setLpp(Length lpp) {
    d->m_vessel_lpp = lpp;
}

void Vessel::setObserver(Length lcg, Length vcg) {
  d->m_observer_lcg = lcg;
  d->m_observer_vcg = vcg;
}

QSet< QString > Vessel::tankGroups() const{
  return d->m_tank_groups;
}

QList< VesselTank* > Vessel::ballastTanks() const {
  return d->m_ballast_tanks;
}

QString Vessel::ballastTankGroup() const {
  return d->m_ballast_tank_group;
}

const ange::vessel::BilinearInterpolator& Vessel::draftData() const {
  return d->m_draft;
}

const ange::vessel::BilinearInterpolator& Vessel::trimData() const {
  return d->m_trim;
}

const ange::vessel::BilinearInterpolator& Vessel::bonjeanCurves() const {
  return d->m_bonjean;
}

const ange::vessel::BilinearInterpolator& Vessel::metacenterFunction() const {
  return d->m_metaCenterFunction;
}

const QList<ange::vessel::BlockWeight>& Vessel::lightshipWeights() const {
  return d->m_hull_weight_data;
}

const QList<ange::vessel::BlockWeight>& Vessel::constantWeights() const {
  return d->m_constantWeightData;
}

const ange::vessel::BilinearInterpolator& Vessel::maxBendingLimitsData() const {
  return d->m_maxBendingLimits;
}

const ange::vessel::BilinearInterpolator& Vessel::minBendingLimitsData() const {
  return d->m_minBendingLimits;
}

const ange::vessel::BilinearInterpolator& Vessel::maxShearForceLimitsData() const {
  return d->m_maxShearForceLimits;
}

const ange::vessel::BilinearInterpolator& Vessel::minShearForceLimitsData() const {
  return d->m_minShearForceLimits;
}

const ange::vessel::BilinearInterpolator& Vessel::maxTorsionLimitsData() const {
  return d->m_maxTorsionLimits;
}

const ange::vessel::BilinearInterpolator& Vessel::minTorsionLimitsData() const {
  return d->m_minTorsionLimits;
}

const Compartment* Vessel::compartmentContaining(const ange::vessel::BayRowTier& brt) const {
  if (const BaySlice* slice = baySliceContaining(brt)) {
    if (const Stack* stack = slice->stackContaining(brt)) {
      return slice->compartmentContainingRow(stack->row(), stack->level());
    }
  }
  return 0L;
}

void Vessel::addCargoHold(ange::vessel::CargoHold* cargoHold) {
  Q_ASSERT(!cargoHold->baySlices().isEmpty());
  if (!d->m_cargoHolds.contains(cargoHold)) {
        cargoHold->setParent(this);
    int i=0;
    for (; i<d->m_cargoHolds.size(); ++i) {
      CargoHold* bh = d->m_cargoHolds.at(i);
      if (bh->baySlices().front()->joinedBay() < cargoHold->baySlices().front()->joinedBay()) {
        break;
      }
    }
    d->m_cargoHolds.insert(i, cargoHold);
  }
}

QList< CargoHold* > Vessel::cargoHolds() const {
  return d->m_cargoHolds;
}

Length Vessel::breadth() const {
  if (d->m_breadth == 0.0*meter) {
    double port = 0.0;
    double starboard = 0.0;
    // Calculat breadth from stack positions
    Q_FOREACH(BaySlice* bay_slice, d->m_bay_slices) {
      Q_FOREACH(Stack* stack, bay_slice->stacks()) {
        port = qMin(stack->bottom().t()/meter, port);
        starboard = qMax(stack->bottom().t()/meter, port);
      }
    }
    d->m_breadth = qMax(starboard - port + 4.0, 4.0)*meter; // Add 4 meters as a fudge factor;
  }
  return d->m_breadth;
}

bool Vessel::hasInfo(ange::vessel::Vessel::VesselFeature requested_data) const {
    switch(requested_data) {
        case Hydrostatics: return draftData().isValid() && trimData().isValid() && metacenterFunction().isValid();
        case HullShape: return bonjeanCurves().isValid()&& lightshipWeights().size() > 0
                            && maxBendingLimitsData().isValid() && minBendingLimitsData().isValid()
                            && maxShearForceLimitsData().isValid() && minShearForceLimitsData().isValid()
                            && maxTorsionLimitsData().isValid() && minTorsionLimitsData().isValid();
        case VisibilityLineData: return !std::isnan(d->m_observer_lcg/meter) && !std::isnan(d->m_observer_vcg/meter);
        default: return false;
    }
}

bool Vessel::isOrientedAftFwd() const {
    if(!d->m_bay_slices.isEmpty()){
        Length fwd_bay_longitudinal_position= d->m_bay_slices.front()->stacks().at(0)->bottom().l();
        Length aft_bay_longitudinal_position= d->m_bay_slices.back()->stacks().at(0)->bottom().l();
        return fwd_bay_longitudinal_position >= aft_bay_longitudinal_position;
    } else {
        return true;
    }
}

void Vessel::mirrorLongitudinally() {
    Q_FOREACH(BaySlice* bay_slice, baySlices()){
        const Length medianBaySliceLcg = bay_slice->lcg();
        Q_FOREACH(Stack* stack, bay_slice->stacks()) {
            stack->mirrorLongitudinally(-medianBaySliceLcg);
        }
    }
    if (hasInfo(Hydrostatics)) {
        d->m_hull_lcg =-d->m_hull_lcg;
        //d->m_draft.mirror_axis_1(); //nothing LCG-related in draft table
        d->m_trim.mirrorYaxis();
        d->m_trim.mirrorValues();
    }
    if (hasInfo(HullShape)) {
        for(int i = 0; i < d->m_hull_weight_data.size(); ++i){
            BlockWeight new_block_weight = d->m_hull_weight_data.at(i);
            new_block_weight.mirrorLongitudinally();
            d->m_hull_weight_data.replace(i, new_block_weight);
        }
        for(int i = 0; i < d->m_constantWeightData.size(); ++i){
            BlockWeight new_block_weight = d->m_constantWeightData.at(i);
            new_block_weight.mirrorLongitudinally();
            d->m_constantWeightData.replace(i, new_block_weight);
        }
        for(int i = 0; i < d->m_tanks.size(); ++i){
            d->m_tanks.value(i)->mirrorLongitudinally();
        }
        d->m_bonjean.mirrorXaxis();
        d->m_minBendingLimits.mirrorXaxis();
        d->m_maxBendingLimits.mirrorXaxis();
        d->m_minShearForceLimits.mirrorXaxis();
        d->m_maxShearForceLimits.mirrorXaxis();
        d->m_minTorsionLimits.mirrorXaxis();
        d->m_maxTorsionLimits.mirrorXaxis();
    }
    if (hasInfo(VisibilityLineData)) {
        d->m_observer_lcg = -d->m_observer_lcg;
    }
}

void Vessel::setAccomodation(Length longitudinalPosition) {
    if(baySlices().isEmpty()) {
        qWarning("Trying to set accomodation in a ship with no bay slices. doing nothing");
        Q_ASSERT(false);
        return;
    }
    //note: the slices are ordered from fwd to back, i.e. increasing long. position
    BaySlice* prev = baySlices().at(0);
    bool last_prev_bay_was_adjacent_to_lq = false;
    for(int i = 1; i< baySlices().size(); ++i) {
        if(baySlices().at(i)->stacks().front()->bottom().l() < longitudinalPosition
                      && prev->stacks().front()->bottom().l() > longitudinalPosition){
            prev->setClearOfLivingQuarters(false, true);
            last_prev_bay_was_adjacent_to_lq = true;
        } else{
            if(last_prev_bay_was_adjacent_to_lq) {
                prev->setClearOfLivingQuarters(false, true);
                last_prev_bay_was_adjacent_to_lq = false;
            } else {
                prev->setClearOfLivingQuarters(true, true);
            }
        }
        prev = baySlices().at(i);
    }
    if(prev->stacks().front()->bottom().l() > longitudinalPosition) {
        prev->setClearOfLivingQuarters(false, true);
    } else{
        prev->setClearOfLivingQuarters(true, true);
    }
}

const SlotTableSummary& Vessel::slotTableSummary() const {
    return d->m_slotSummary;
}

Length Vessel::aftOverAll() const {
    return d->m_aftOverAll;
}

Length Vessel::foreOverAll() const {
    return d->m_foreOverAll;
}

QString Vessel::classificationSociety() const {
    return d->m_classificationSociety;
}

void Vessel::setLashingPatternData(const LashingPatternData* lashingPatternData) {
    delete d->m_lashingPatternData;
    d->m_lashingPatternData = lashingPatternData;
}

const LashingPatternData* Vessel::lashingPatternData() const {
    return d->m_lashingPatternData;
}

}
}
