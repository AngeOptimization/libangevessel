#include "stacksupport.h"

#include "stack.h"
#include "slot.h"

#include <ange/units/units.h>

using namespace ange::units;

namespace ange {
namespace vessel {

class StackSupport::StackSupportPrivate {
  public:
  StackSupportPrivate(Length max_height,
                          Mass max_weight,
                          int bay,
                          int row,
                          DeckLevel support_level,
                          StackSupportType support_type,
                          const QList<Slot* >& stack_slots,
                          const Point3D& bottom_pos);
  Mass m_max_weight;
  Length m_max_height;
  int m_bay;
  int m_row;
  QList<Slot*> m_slots;
  StackSupportType m_StackSupportype;
  DeckLevel m_stack_support_level;
  Point3D m_bottom_pos;
  bool m_imo_allowed;
  Stack* m_stack;
};

StackSupport::StackSupportPrivate::StackSupportPrivate(Length max_height,
                                                                  Mass max_weight,
                                                                  int bay,
                                                                  int row,
                                                                  DeckLevel support_level,
                                                                  StackSupport::StackSupportType support_type,
                                                                  const QList< Slot* >& stack_slots,
                                                                  const Point3D& bottom_pos) :
  m_max_weight(max_weight),
  m_max_height(max_height),
  m_bay(bay),
  m_row(row),
  m_slots(stack_slots),
  m_StackSupportype(support_type),
  m_stack_support_level(support_level),
  m_bottom_pos(bottom_pos),
  m_imo_allowed(false)
{
    // Empty
}

StackSupport::StackSupport(Length max_height, Mass max_weight, QString bayname, QString rowname,
                           ange::vessel::DeckLevel support_level, ange::vessel::StackSupport::StackSupportType support_type,
                           const QList< ange::vessel::Slot* >& stack_slots, const ange::vessel::Point3D& bottom_pos)
  : QObject(0L),
  d(new StackSupportPrivate(max_height, max_weight, bayname.toInt(), rowname.toInt(), support_level, support_type, stack_slots, bottom_pos))
{
  setSupports(stack_slots);
}

StackSupport::~StackSupport() {
    // Empty
}

Mass StackSupport::maxWeight() const {
  return d->m_max_weight;
}

Length StackSupport::maxHeight() const {
  return d->m_max_height;
}

QString StackSupport::bayName() const {
  return QString::number(d->m_bay);
}

QString StackSupport::rowName() const {
  return QString::number(d->m_row);
}

QList< Slot* > StackSupport::stackSlots() const {
  return d->m_slots;
}

StackSupport::StackSupportType StackSupport::stackSupportType() const {
  return d->m_StackSupportype;
}

DeckLevel StackSupport::stackSupportLevel() const {
  return d->m_stack_support_level;
}

int StackSupport::bay() const {
  return d->m_bay;
}

int StackSupport::row() const {
  return d->m_row;
}

Point3D StackSupport::bottomPos() const {
  return d->m_bottom_pos;
}

void StackSupport::forbidImo(bool is_forbidden) {
    d->m_imo_allowed = !is_forbidden;
}

bool StackSupport::imoForbidden() const {
  return !d->m_imo_allowed;
}

void StackSupport::setStack(Stack* stack) {
  d->m_stack = stack;
}

const Stack* StackSupport::stack() const {
  return d->m_stack;
}

void StackSupport::mirrorLongitudinally() {
    d->m_bottom_pos = ange::vessel::Point3D(-d->m_bottom_pos.l(), d->m_bottom_pos.v(), d->m_bottom_pos.t());
}

void StackSupport::resetSlots(QList< Slot* > stack_slots) {
  setSupports(stack_slots);
  d->m_slots=stack_slots;
}

void StackSupport::setSupports(QList<Slot*> stack_slots) {
  switch (d->m_StackSupportype) {
    case Twenty: {
      Q_FOREACH(Slot* slot, stack_slots) {
        slot->setSupport20(this);
      }
      break;
    }
    case Forty: {
      Q_FOREACH(Slot* slot, stack_slots) {
        slot->setSupport40(this);
      }
      break;
    }
  }
}

}} // namespace ange::vessel
