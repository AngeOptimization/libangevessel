#ifndef LIBANGEVESSEL_BAYROWTIER_H
#define LIBANGEVESSEL_BAYROWTIER_H

#include "angevessel_export.h"

#include <QDebug>
#include <QMetaType>
#include <QHash>

class QTextStream;
class QString;

namespace ange {
namespace vessel {

/**
 * Bay/Row/Tier position
 */
class ANGEVESSEL_EXPORT BayRowTier {

public:

  /**
   * Construct off-vessel or unknown brt
   */
  BayRowTier();

  /**
   * Constructor
   */
  BayRowTier(int bay, int row, int tier);

  /**
   * Constructor using strings for input
   */
  BayRowTier(const QString& bay, const QString& row, const QString& tier);

  /**
   * Get bay
   */
  int bay() const {
    return m_bay;
  }

  /**
   * Get row
   */
  int row() const {
    return m_row;
  }

  /**
   * Get tier
   */
  int tier() const {
    return m_tier;
  }

  /**
   * Returns true if placed
   */
  bool placed() const;

  /**
   * @return string representation
   */
  QString toString() const;

  /**
    * @return hash of brt
    */
  friend uint qHash(const BayRowTier& brt) {
    return 10000 * brt.m_bay + 100 * brt.m_row + brt.m_tier;
  }

private:
  int m_bay;
  int m_row;
  int m_tier;
};

inline bool operator==(const BayRowTier& lhs, const BayRowTier& rhs) {
  return lhs.bay() == rhs.bay() && lhs.row() == rhs.row() && lhs.tier() == rhs.tier();
}

inline bool operator!=(const BayRowTier& lhs, const BayRowTier& rhs) {
  return !(lhs == rhs);
}

ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const BayRowTier& brt);

}} // namespace ange::vessel

ANGEVESSEL_EXPORT QTextStream& operator<<(QTextStream& stream, const ange::vessel::BayRowTier& brt);

Q_DECLARE_METATYPE(ange::vessel::BayRowTier);

#endif // LIBANGEVESSEL_BAYROWTIER_H
