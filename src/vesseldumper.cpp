#include "vesseldumper.h"

#include "slot.h"
#include "vessel.h"
#include "bayslice.h"
#include "stack.h"
#include "stacksupport.h"

#include <QScopedPointer>

using namespace ange::units;

namespace ange {
namespace vessel {

class VesselDumper::VesselDumperPrivate {

public:
    VesselDumperPrivate(const Vessel* vessel) : m_vessel(vessel) {
        // Empty
    }

public:
    const Vessel* m_vessel;
};

VesselDumper::VesselDumper(const Vessel* vessel) : d(new VesselDumperPrivate(vessel)) {
    // Empty
}

VesselDumper::~VesselDumper() {
    // Empty
}

const ange::vessel::Vessel* ange::vessel::VesselDumper::vessel() const {
  return d->m_vessel;
}

static inline void dump_slot(const ange::vessel::Slot* slot, QDebug& dbg) {
  dbg << slot->brt().tier();
  if(slot->isFore()) {
    dbg << "*";
  }
  if(slot->hasCapability(ange::vessel::Slot::ReeferCapable)) {
    dbg << "R";
  }
}

static inline void dump_slots(QList<ange::vessel::Slot*> stack_slots, QDebug& dbg) {
  bool first = true;
  Q_FOREACH(const ange::vessel::Slot* slot, stack_slots) {
    if(!first) dbg << ',';
    dump_slot(slot,dbg);
    first = false;
  }
  dbg << '\n';
}

static inline void dump_stack_support(const ange::vessel::StackSupport* stack_support,QDebug& dbg) {
  dbg << "  STACKSUPPORT  " << (stack_support->stackSupportType() == ange::vessel::StackSupport::Forty ? "FOURTY" : "TWENTY") << '\n';
  dbg << "  ";
  dump_slots(stack_support->stackSlots(),dbg);
}

static inline void dump_stack(const ange::vessel::Stack* stack, QDebug& dbg) {
  dbg << "STACK  " << stack->row() << "  " << (stack->level() ==ange::vessel::Above ? "ABOVE" : "BELOW") << '\n';
  dump_slots(stack->stackSlots(),dbg);
  Q_FOREACH(const ange::vessel::StackSupport* stack_support, stack->stackSupports()) {
    if(stack_support) {
      dump_stack_support(stack_support,dbg);
    }
  }
}

static inline void dump_bay(const ange::vessel::BaySlice* bay_slice, QDebug& dbg) {
  dbg << "BAY SLICE  " << bay_slice->bays() << '\n';
  Q_FOREACH(const ange::vessel::Stack* stack, bay_slice->stacks()) {
    dump_stack(stack, dbg);
  }
}

QDebug operator<<(QDebug dbg, VesselDumper* dumper) {
  dbg << "VESSEL: " << dumper->vessel()->name() << dumper->vessel()->callSign() << '\n';
  Q_FOREACH(const BaySlice* bay_slice, dumper->vessel()->baySlices()) {
    dump_bay(bay_slice,dbg);
  }
  return dbg;
}

}
}
