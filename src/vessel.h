#ifndef LIBANGEVESSEL_VESSEL_H
#define LIBANGEVESSEL_VESSEL_H

#include "angevessel_export.h"

#include "bayrowtier.h"
#include <ange/units/units.h>

#include <QObject>
#include <QSet>

namespace ange {
namespace vessel {
class BaySlice;
class BilinearInterpolator;
class BilinearInterpolator;
class BlockWeight;
class CargoHold;
class Compartment;
class HatchCover;
class LashingPatternData;
class Slot;
class SlotTableSummary;
class VesselPrivate;
class VesselTank;
}
}

namespace ange {
namespace vessel {

/**
 * A vessel, hull description and slot table
 */
class ANGEVESSEL_EXPORT Vessel : public QObject {

    Q_OBJECT

public:

    /**
     * Standard density of ocean water, is 1.025 kg/l
     */
    static const ange::units::Density oceanWaterDensity;

    /**
     * Construct Vessel
     * @param bay_slices
     * @param lids
     *   bay_slices in this Vessel, this constructor takes ownership of all the bay_slices
     */
    Vessel(QList<BaySlice*> bay_slices,
             QList<HatchCover*> lids,
             QList<VesselTank*> tanks,
             const QString& name = QString::fromLocal8Bit("unnamed"),
             const QString& imo_number = "",
             const QString& callsign = "",
             const QString& vessel_code = "",
             QObject* parent = 0L);

    virtual ~Vessel();

    /**
     * @return all bay_slices
     */
    const QList<BaySlice*> baySlices() const ;

    /**
     * Gets the slot at position brt, will return 0 if there is no such slot.
     * When getting a slot at a 40'-position it will return the fore most 20' slot.
     * @param brt
     *   bay/row/tier position
     */
    const Slot* slotAt(const BayRowTier& brt) const;

    /**
     * Gets the bay_slice containeng position brt, will return 0 if there is no such bay_slice.
     * As bay_slices contains the full virtual stacks there is no issue with what to do when asked for a 40' position.
     * @param brt
     *   bay/row/tier position
     */
    const BaySlice* baySliceContaining(const BayRowTier& brt) const;

    /**
     * @return bay slice containing bay no, or 0 if no such slice
     * @param bay
     */
    const BaySlice* baySliceContaining(int bay) const;

    /**
     * @return bay slice containing bay no, or 0 if no such slice
     * @param bay
     */
    BaySlice* baySliceContaining(int bay);

    /**
     * @return the compartment containing the position in brt
     * Returns 0 if the BayRowTier is not in any compartment. That should be impossible for any valid brt.
     */
    const Compartment* compartmentContaining(const BayRowTier& brt) const;

    /**
     * @return all lids
     */
    QList<HatchCover*> lids() const;

    /**
     * @return all tanks
     */
    QList<VesselTank*> tanks() const;

    /**
     * @return name of vessel
     */
    QString name() const;

    /**
    * @return IMO number of vessel
    */
    QString imoNumber() const;

    /**
    * @return call sign of vessel
    */
    QString callSign() const;

   /**
    * @return vessel_code of vessel
    */
    QString vesselCode() const;

    /**
     * @return a list of tank groups used in this vessel
     */
    QSet<QString> tankGroups() const;

    /**
     * @param group  -  if group not in tank_groups, behaviour is undefined.
     * @return tanks with given tank group
     */
    QList<VesselTank*> tankByTankGroup(const QString& tankgroup) const;

    /**
     * @return the tanks that are ballast tanks, e.g. in view of auto-ballasting
     */
    QList<VesselTank*> ballastTanks() const;

    /**
     * @return the name of the tank group containing ballast tanks. In case there are multiple
     * the tank group with the ballast tank appearing last in the list of tanks will be returned.
     */
    QString ballastTankGroup() const;

    /**
     * Return the tank with the given id (as defined in identifiable. Not stable across save/load or different vessels)
     * Returns 0 if not found
     */
    const VesselTank* tankById(int id) const;

    /**
     * Looks up the tank based on its description and returns a tank with matching descriptions
     * Returns 0 if not found
     */
    const VesselTank* tankByDescription(const QString& description) const;

    /**
     * Sets trim data in form of (lcg,weight) => trim.
     */
    void setTrimData(const ange::vessel::BilinearInterpolator& trimTable);

    /**
     * Sets draft data in form of (lcg,weight) => draft.
     */
    void setDraftData(const ange::vessel::BilinearInterpolator& trimTable);

    /**
     * Sets the bonjean curves in form of (draft,lcg) => area
     */
    void setBonjeanData(const ange::vessel::BilinearInterpolator& bonjeanData);

    /**
     * Sets the metacentre data in form of (draft,trim) => height
     */
    void setMetacenterData(const ange::vessel::BilinearInterpolator& metacentreData);

    /**
     * Sets the stress limits
     */
    void setStressLimits(const ange::vessel::BilinearInterpolator& maxBendingLimits,
                         const ange::vessel::BilinearInterpolator& minBendingLimits,
                         const ange::vessel::BilinearInterpolator& maxShearForceLimits,
                         const ange::vessel::BilinearInterpolator& minShearForceLimits,
                         const ange::vessel::BilinearInterpolator& maxTorsionLimits,
                         const ange::vessel::BilinearInterpolator& minTorsionLimits);

    /**
     * Sets the hull weight distribution data in form of a list of blocks
     */
    void setLightshipWeights(const QList<ange::vessel::BlockWeight>& blocks);

    /**
     * Sets the constant weights as a list of blocks
     */
    void setConstantWeights(const QList<ange::vessel::BlockWeight>& blocks);

    /**
     * @return the data used for calculating draft
     */
    const BilinearInterpolator& draftData() const;

    /**
     * @return the data used for calculating trim
     */
    const BilinearInterpolator& trimData() const;

    /**
     * @return the bonjean curves
     */
    const BilinearInterpolator& bonjeanCurves() const;

    /**
     * @return the metacentre data
     */
    const BilinearInterpolator& metacenterFunction() const;

    /**
     * @return the hull weight blocks
     */
    const QList<BlockWeight>& lightshipWeights() const;

    /**
     * @return the constant weights
     */
    const QList<BlockWeight>& constantWeights() const;

    /**
     * @return the maximum bending torque limits data
     */
    const BilinearInterpolator& maxBendingLimitsData() const;

    /**
     * @return the minimum bending torque limits data
     */
    const BilinearInterpolator& minBendingLimitsData() const;

    /**
     * @return the maximum shear force limits data
     */
    const BilinearInterpolator& maxShearForceLimitsData() const;

    /**
     * @return the minimum shear force limits data
     */
    const BilinearInterpolator& minShearForceLimitsData() const;

    /**
     * @return the the maximum torsional torque limits data
     */
    const BilinearInterpolator& maxTorsionLimitsData() const;

    /**
     * @return the minimum torsional torque limits data
     */
    const BilinearInterpolator& minTorsionLimitsData() const;

    /**
     * @return the draft for a given lcg and weight at waterDensity
     */
    ange::units::Length draft(ange::units::Mass weight, ange::units::Length lcg, ange::units::Density waterDensity) const;

    /**
     * @return the trim for a given lcg and weight at waterDensity
     */
    ange::units::Length trim(ange::units::Mass weight, ange::units::Length lcg, ange::units::Density waterDensity) const;

    /**
     * @return the submerged area for a given draft at a given lcg
     */
    ange::units::Unit<0, 2, 0> bonjean(ange::units::Length lcg, ange::units::Length draft) const;

    /**
     * @return the metacentre height in meter above keel for a given draft and trim
     */
    ange::units::Length metacenter(ange::units::Length trim, ange::units::Length draft) const;

    /**
     * @return the maximal bending torque limit for a given lcg in SI units (NewtonMeter)
     * BIC: use units
     */
    qreal maxBendingLimit(ange::units::Length lcg) const;

    /**
     * @return the minimal bending torque limit for a given lcg in SI units (NewtonMeter)
     * BIC: use units
     */
    qreal minBendingLimit(ange::units::Length lcg) const;

    /**
     * @return the maximal shear force limit for a given lcg in SI units (Newton)
     * BIC: use units
     */
    qreal maxShearForceLimit(ange::units::Length lcg) const;

    /**
     * @return the minimal shear force limit for a given lcg in SI units (Newton)
     * BIC: use units
     */
    qreal minShearForceLimit(ange::units::Length lcg) const;

    /**
     * @return the maximal torsional torque limit for a given lcg in SI units (NewtonMeter)
     * BIC: use units
     */
    qreal maxTorsionLimit(ange::units::Length lcg) const;

    /**
     * @return the minimal torsional torque limit for a given lcg in SI units (NewtonMeter)
     * BIC: use units
     */
    qreal minTorsionLimit(ange::units::Length lcg) const;

    /**
     * Sets the weight of the hull
     */
    void setLightshipWeight(ange::units::Mass weight);

    /**
     * Sets the lcg of the hull
     */
    void setHullLcg(ange::units::Length lcg);

    /**
     * Sets the length between perpendiculars (LPP) of the vessel
     */
    void setLpp(ange::units::Length lpp);

    /**
     * Sets the observation point of the vessel
     */
    void setObserver(ange::units::Length lcg, ange::units::Length vcg);

    /**
     * Sets the Vcg of the hull
     */
    void setHullVcg(const ange::units::Length vcg);

    /**
     * @returns the lcg for the empty hull
     */
    ange::units::Length hullLcg() const;

    /**
     * @returns the vcg for the empty hull
     */
    ange::units::Length hullVcg() const;

    /**
     * @returns the weight of the empty ship
     */
    ange::units::Mass lightshipWeight() const;

    /**
     * @returns the vessel length between perpendiculars
     */
    ange::units::Length lpp() const;

    /**
     * Note: This function currently guesses from the stack positions.
     * You will need to supply the data explicitly if this is to be used for anything but
     * drawings and such.
     * @return breadth of vessel
     */
    ange::units::Length breadth() const;

    /**
     * @return the bulkheads of the vessel, in order from the front of the vessel to the aft vessel
     */
    QList<CargoHold*> cargoHolds() const;

    /**
     * Add a hold to vessel. Parent will be set to vessel
     * @param cargoHold hold to add
     */
    void addCargoHold(CargoHold* cargoHold);

    /**
     * @returns the lcg of the observation point on command bridge
     */
    ange::units::Length observerLcg() const;

    /**
     * @returns the vcg of the observation point on command bridge
     */
    ange::units::Length observerVcg() const;

    /**
     * an enum used to describe if a vessel has the requested data
     * available
     * The idea is to keep the list as short and simple as possible,
     * perhaps at the expense of being overly restrictive.
     * For instance if a vessel has HYDROSTATICS data, we also assume
     * (implicitly for now) that it has e.g. an LPP and a breadth
     */
    enum VesselFeature {
        Hydrostatics =       0x01,
        HullShape =          0x02,
        VisibilityLineData = 0x04,
    };

    /**
     * @returns whether the requested vessel data is available or not
     */
    bool hasInfo(VesselFeature requested_data) const;

    /**
     * @returns true if the vessel is longitudinally oriented from aft to fwd
     * and false if, as it is rarely the case, it is oriented the opposite way.
     * Note: Bay numbering is another issue. It is assumed always to be increasing
     * from fwd to aft.
     */
    bool isOrientedAftFwd() const;
    /**
     * reorients (mirrors) the vessel fwd-aft wise. Needed to handle the cases when the
     * vessel profile is oriented with increasing longitude fwd -> aft, as our code assumes
     * the and aft -> fwd orientation (at least the block weights and stress/stability graphs)
     * Note: this will modify the vessel, i.e. the read vessel might be different from the saved
     * vessel. One workaround would be to re-mirror it before saving, but a better design would
     * probably be to simply keep the original vessel as a blob.
     */
    void mirrorLongitudinally();

    /**
     * Defines the longitudinal position of the accomodation, i.e. clears the clear_of_living_quarters
     * property of the slots in above deck stacks in the bay_slices next to the given position, all other
     * slots get the property set. This is used when placing dangerous goods.
     */
    void setAccomodation(ange::units::Length longitudinalPosition);

    /**
     * @return a summary of the slot table (number of slots, reefer plugs etc.)
     */
    const SlotTableSummary& slotTableSummary() const;

    /**
     * @return minimum known value in the fore/aft direction
     */
    ange::units::Length aftOverAll() const;

    /**
     * @return maximum known value in the fore/aft direction
     */
    ange::units::Length foreOverAll() const;

    /**
     * Sets the classification society
     */
    void setClassificationSociety(const QString& society);

    /**
     * @return the classification society of the vessel
     */
    QString classificationSociety() const;

    /**
     * Set the lashing pattern data for this vessel. This class takes ownership of the given object
     */
    void setLashingPatternData(const LashingPatternData* lashingPatternData);

    /**
     * Get the lashing pattern data for this vessel, will be nullptr if not defined in profile
     */
    const LashingPatternData* lashingPatternData() const;

private:
    QScopedPointer<VesselPrivate> d;
    void setup();

};

}
}

#endif
