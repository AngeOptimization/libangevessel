#ifndef ANGEVESSEL_HATCHCOVER_H
#define ANGEVESSEL_HATCHCOVER_H

#include "angevessel_export.h"

#include <QList>
#include <QScopedPointer>

namespace ange {
namespace vessel {
class Vessel;
class Stack;
}
}

namespace ange {
namespace vessel {

/**
 * Hatch cover, a lid over a compartment.
 */
class ANGEVESSEL_EXPORT HatchCover {

public:

    /**
     * Construct lid
     * @param above stacks under lid
     * @param below stacks on top off lid.
     */
    HatchCover(QList<Stack*> above, QList<Stack*> below);

    /**
     * Destructor
     */
    ~HatchCover();

    /**
     * @return stacks covering the lid, that is, on top of the lid.
     */
    QList<const Stack*> stacksAbove() const;

    /**
     * @return stacks covered by lid, that is, beneath the lid
     */
    QList<const Stack*> stacksBelow() const;

private:
    class HatchCoverPrivate;
    QScopedPointer<HatchCoverPrivate> d;

};

ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const HatchCover* lid);
ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const HatchCover& lid);

}
}

#endif // ANGEVESSEL_HATCHCOVER_H
