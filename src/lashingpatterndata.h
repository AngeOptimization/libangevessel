#ifndef ANGE_VESSEL_LASHINGPATTERNDATA_H
#define ANGE_VESSEL_LASHINGPATTERNDATA_H

#include "angevessel_export.h"
#include "bayrowlevel.h"
#include "direction.h"
#include <ange/units/units.h>

#include <QList>

namespace ange {
namespace vessel {
class LashingStackData;
class LashingRod;
class Point3D;
class StackLashingPattern;
class StackSupport;
}
}

namespace ange {
namespace vessel {


/**
 * Data about a lashing pattern for the vessel
 */
class ANGEVESSEL_EXPORT LashingPatternData {

public:
    ~LashingPatternData();

    /**
     * Register a named StackLashingPattern, this class takes ownership of the object
     */
    void addStackPattern(StackLashingPattern* stackLashingPattern);

    /**
     * Add data for a stack by refering to a pattern that was registered by addStackPattern()
     */
    void addStackData(BayRowLevel bayRowLevel, QString aftPatternName, QString forePatternName, int cellGuidesToTier);

    /**
     * Raw access to the data in the lashing pattern
     */
    QMultiHash<QString, const StackLashingPattern*> stackLashingPatterns() const;

    /**
     * Raw access to the data in the lashing pattern
     */
    QHash<BayRowLevel, const LashingStackData*> lashingStackDatas() const;

    /**
     * Get lashing data for a stack
     */
    const LashingStackData* get(BayRowLevel bayRowLevel) const;

private:
    QMultiHash<QString, const StackLashingPattern*> m_stackLashingPatterns;
    QHash<BayRowLevel, const LashingStackData*> m_lashingStackDatas;
};


/**
 * Data for a stack
 */
class ANGEVESSEL_EXPORT LashingStackData {

public: // Methods

    /**
     * Simple constructor
     */
    LashingStackData();

    /**
     * Create a rod for the given container corner, will be nullptr when the lashing pattern does not dictate any
     * lashing rod.
     */
    const LashingRod* generateRod(Direction::AftFore end, Direction::StarboardPort side, Direction::TopBottom topBottom,
                                  int containerNumber, int numberOfContainersInStack,
                                  units::Length heightUnderLashing, const Point3D& containerCorner) const;

public: // Fields

    /**
     * StackLashingPattern used in this stacks. This class has no ownership of these objects.
     */
    QHash<Direction::AftFore, QList<const StackLashingPattern*> > stackPatterns;

    /**
     * This stack has ceel guides up to this tier, if there are no cell guides this will be zero
     */
    int cellGuidesToTier;

};


/**
 * Description of a single lashing (one or two rods) in a lashing pattern for a stack
 */
class ANGEVESSEL_EXPORT StackLashingPattern {

public: // Methods

    /**
     * Simple constructor
     */
    StackLashingPattern();

    /**
     * Height derived from minimumHcCountUnderLashing
     */
    units::Length minimumHeightUnderLashing() const;

public: // Fields

    /**
     * Name of stack pattern
     */
    QString patternName;

    /**
     * Minimum number of containers that must be in the stack before this rod is attached
     */
    int minimumNumberOfContainersInStack;

    /**
     * Minimum number of HC containers that must be below the lashing before this pattern is used, if there is an
     * other StackLashingPattern with the same name that is attached to the same corner then only the one with the
     * highest minimumHcCountUnderLashing that is above the number of HC containers below the lashing is used.
     */
    int minimumHcCountUnderLashing;

    /**
     * The container this lashing is attached to counted up from the deck
     */
    int attachedToContainerNumber;

    /**
     * The top/bottom corner position this lashing is attached to
     */
    Direction::TopBottom position;

    /**
     * The side or sides this lashing is attached to
     */
    QList<Direction::StarboardPort> side;

    /**
     * The E-Module of the lashing bar
     */
    units::Unit<1, -1, -2> eModule; // Unit is Force/Length^2

    /**
     * The diameter of the lashing bar
     */
    units::Length diameter;

    /**
     * The maximum lashing force allowed in the lashing bar
     */
    units::Force maxLashingForce;

    /**
     * How much the lashing bridge this lashing bar is attached to will move, this is used in the GL rules
     */
    units::Length lashingBridgeDeformation;

    /**
     * The lashing angle, this is independent of the height of the container this lashing is attached to.
     *
     * Either angle and rodLength is defined or lashingPlatePostionVertical and lashingDistanceTransverse, the
     * undefined values should be NaN.
     */
    double angle; // In radians

    /**
     * The length of the lashing rod, this is independent of the height of the container this lashing is attached to.
     */
    units::Length rodLength;

    /**
     * Vertical coordinate of the position on the vessel where the lashing rod is attached, this can be used to
     * calculate angel and rodLength that is depended of the height of the container this lashing is attached to.
     */
    units::Length lashingPlatePostionVertical;

    /**
     * Vertical distance from container corner to the position on the vessel where the lashing rod is attached,
     * this can be used to calculate angel and rodLength that is depended of the height of the container this lashing
     * is attached to.
     */
    units::Length lashingDistanceTransverse;

};


}} // namespace ange::vessel

#endif // ANGE_VESSEL_LASHINGPATTERNDATA_H
