#ifndef ANGE_VESSEL_DIRECTION_H
#define ANGE_VESSEL_DIRECTION_H

#include "angevessel_export.h"

#include <QString>
#include <QList>

namespace ange {
namespace vessel {

namespace Direction {


/**
 * Enum representing the ends in longitudinal direction (aft/fore).
 */
enum AftFore {
    Aft,
    Fore,
};

/**
 * All the values in the enum, [Aft, Fore]
 */
ANGEVESSEL_EXPORT QList<AftFore> aftForeValues();

/**
 * Convert enum to string
 */
ANGEVESSEL_EXPORT QString toString(AftFore end);

/**
 * Convert string to enum, if the string as bad an invalid value will be returned and ok will be set to false
 */
ANGEVESSEL_EXPORT AftFore toAftFore(const QString& string, bool* ok);


/**
 * Enum representing the sides in transverse direction (starboard/port).
 */
enum StarboardPort {
    Starboard,
    Port,
};

/**
 * All the values in the enum, [Starboard, Port]
 */
ANGEVESSEL_EXPORT QList<StarboardPort> starboardPortValues();

/**
 * Convert enum to string
 */
ANGEVESSEL_EXPORT QString toString(StarboardPort side);

/**
 * Convert string to enum, if the string as bad an invalid value will be returned and ok will be set to false
 */
ANGEVESSEL_EXPORT StarboardPort toStarboardPort(const QString& string, bool* ok);


/**
 * Enum representing the sides in vertical direction (top/bottom).
 */
enum TopBottom {
    Top,
    Bottom,
};

/**
 * All the values in the enum, [Top, Bottom]
 */
ANGEVESSEL_EXPORT QList<TopBottom> topBottomValues();

/**
 * Convert enum to string
 */
ANGEVESSEL_EXPORT QString toString(TopBottom topBottom);

/**
 * Convert string to enum, if the string as bad an invalid value will be returned and ok will be set to false
 */
ANGEVESSEL_EXPORT TopBottom toTopBottom(const QString& string, bool* ok);


} // namespace Direction

}} // namespace ange::vessel

#endif // ANGE_VESSEL_DIRECTION_H
