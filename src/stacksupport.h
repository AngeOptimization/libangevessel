#ifndef STACKSUPPORT_H
#define STACKSUPPORT_H

#include "angevessel_export.h"

#include "point3d.h"
#include "decklevel.h"
#include <ange/units/units.h>

#include <QObject>

namespace ange {
namespace vessel {
class Stack;
class Slot;
}
}

namespace ange {
namespace vessel {

class ANGEVESSEL_EXPORT StackSupport : public QObject {

    Q_OBJECT

public:

    enum StackSupportType {
        Twenty,
        Forty,
    };

    /**
     * Construct StackSupport
     * @param stack_slots
     *   Slots in this StackSupport.
     */
    StackSupport(ange::units::Length max_height,
                    ange::units::Mass max_weight,
                    QString bayname,
                    QString rowname,
                    DeckLevel support_level,
                    ange::vessel::StackSupport::StackSupportType support_type,
                    const QList< ange::vessel::Slot* >& stack_slots,
                    const Point3D& bottom_pos);

    virtual ~StackSupport();

    /**
     * @return maximal stack weight, in kilograms
     */
    ange::units::Mass maxWeight() const;

    /**
     * @return maximal stack height, in meters
     */
    ange::units::Length maxHeight() const;

    /**
     * @return Bayname;
     */
    QString bayName() const;

    /**
     * @return rowname;
     */
    QString rowName() const;

    /**
     * @return a list from bottom to top, of stack support describing the slots
     */
    QList<Slot*> stackSlots() const;

    /**
      * @return the stack support type
      */
    StackSupportType stackSupportType() const;

    /**
      * @return the stack support level
      */
    DeckLevel stackSupportLevel() const;

    /**
     * @return the bay number
     */
    int bay() const;

    /**
     * @return the row number
     */
    int row() const;

    /**
     * @return bottom position
     */
    Point3D bottomPos() const;

    /**
     * Forbid imos  (dangerous goods) on this support
     **/
    void forbidImo(bool is_forbidden=true);

    /**
     * IMOs forbidden here
     */
    bool imoForbidden() const;

    /**
     * @return stack that contains this stack support
     */
    const Stack* stack() const;

    /**
     * negates the stack's longitudinal position
     * used when inverting the vessel's longitudinal axis
     */
    void mirrorLongitudinally();

private:
    class StackSupportPrivate;
    QScopedPointer<StackSupportPrivate> d;

    friend class Stack;
    void setStack(Stack* stack);
    void resetSlots(QList<Slot*> stack_slots);
    /* this one needs to be private, and not on the private class
     * since we are relying on a friend declaration over in Slot
     */
    void setSupports(QList< ange::vessel::Slot* > stack_slots);

};

ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const StackSupport& support);

}
}

#endif // STACKSUPPORT_H
