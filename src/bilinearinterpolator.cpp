/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010
 Copyright: See COPYING file that comes with this distribution
*/

#include "bilinearinterpolator.h"
#include <QMap>
namespace ange {
namespace vessel {

class BilinearInterpolator::Private : public QSharedData {
public:
    /**
     * The sample data structured such that data(x_j, y_i) = data.value(x_j).value(y_i)
     * Using a QMap ensures that the x- and y coordinates are sorted
     */
    QMap<qreal, QMap<qreal, qreal> > m_data;
    Private(const QList <qreal>& xCoordinates, const QList <qreal>& yCoordinates,
                         const QList< qreal>& sampleData) {
        for(int j = 0; j < xCoordinates.size(); ++j) {
            for(int i = 0; i < yCoordinates.size(); ++i) {
                m_data[xCoordinates.at(j)][yCoordinates.at(i)] = sampleData.at(i*xCoordinates.size() + j);
            }
        }
    }
};

BilinearInterpolator::BilinearInterpolator() : d(new BilinearInterpolator::Private(QList<qreal>(), QList<qreal>(), QList<qreal>())) {}

ange::vessel::BilinearInterpolator::BilinearInterpolator(const QList< qreal >& xCoordinates,
                                                         const QList< qreal >& yCoordinates,
                                                         const QList< qreal >& sampleData) :
                          d(new BilinearInterpolator::Private(xCoordinates, yCoordinates, sampleData)){}

BilinearInterpolator::BilinearInterpolator(const BilinearInterpolator& other) : d(other.d){}

const BilinearInterpolator& BilinearInterpolator::operator=(const BilinearInterpolator& rhs) {
  if (this != &rhs) {
    d = rhs.d;
  }
  return *this;
}

const QList< qreal > BilinearInterpolator::xCoordinates() const {
    if(!d->m_data.isEmpty()) {
        return d->m_data.keys();
    }
    return QList<qreal>();
}

const QList< qreal > BilinearInterpolator::yCoordinates() const {
    if(!d->m_data.isEmpty()) {
        return d->m_data.values().at(0).keys();
    }
    return QList<qreal>();
}

qreal ange::vessel::BilinearInterpolator::valueAt(qreal x, qreal y) const {
    // Dealing with a data point, return the original data sample as this is needed for reconstructing the data on save
    if (d->m_data.contains(x) && d->m_data.value(x).contains(y)) {
        return d->m_data.value(x).value(y);
    }

    // Find the x samples that frame the input x, if input is outside of the sample data make x1 and x2 the same value
    QMap<qreal, QMap<qreal, qreal> >::const_iterator xUp = d->m_data.upperBound(x);
    QMap<qreal, QMap<qreal, qreal> >::const_iterator xLow = xUp;
    if (xLow != d->m_data.begin()){
        --xLow;
    }
    if (xUp == d->m_data.end()) {
        --xUp;
    }
    qreal x1 = xLow.key();
    qreal x2 = xUp.key();

    // Find y samples and the corresponding sample data for the lower x sample
    QMap<qreal, qreal> xLow_y = xLow.value();
    QMap<qreal, qreal>::const_iterator xLow_yUp = xLow_y.upperBound(y);
    QMap<qreal, qreal>::const_iterator xLow_yLow = xLow_yUp;
    if (xLow_yLow != xLow_y.begin()) {
        --xLow_yLow;
    }
    if (xLow_yUp == xLow_y.end()) {
        --xLow_yUp;
    }
    qreal y1 = xLow_yLow.key();
    qreal y2 = xLow_yUp.key();
    qreal z11 = xLow_yLow.value();
    qreal z12 = xLow_yUp.value();

    // Reduce to 1d case when looking up data outside the x samples (interpolate in y-direction only)
    if (x1 == x2) {
        if (y1 == y2) {
            return z11;  // Lookup point is outside a corner, all 4 sample points are the same
        } else {
            return (z12*(y - y1) + z11*(y2 - y)) / (y2 - y1);
        }
    }

    // Lookup sample data for upper x samples using y samples found above
    QMap<qreal, qreal> xUp_y = xUp.value();
    qreal z21 = xUp_y.value(y1);
    qreal z22 = xUp_y.value(y2);  // This line could be moved after 1d case below for performance

    // Reduce to 1d case when looking up data outside the y samples (interpolate in x-direction only)
    if (y1 == y2) {
        return (z21*(x - x1) + z11*(x2 - x)) / (x2 -x1);
    }

    // Full 2d interpolation
    return (    z11 * (x2 - x) * (y2 - y) + z21 * (x - x1) * (y2 - y)
              + z12 * (x2 - x) * (y - y1) + z22 * (x - x1) * (y - y1) )
           / ((x2 - x1)*(y2 - y1));
}

void BilinearInterpolator::mirrorXaxis() {
    QMap<qreal, QMap<qreal, qreal> > newMap;
    for(QMap<qreal, QMap<qreal, qreal> >::iterator j = d->m_data.begin(); j != d->m_data.end(); ++j) {
        newMap[-j.key()] = *j;
    }
    d->m_data = newMap;
}

void BilinearInterpolator::mirrorYaxis() {
    QMap<qreal, QMap<qreal, qreal> > newMap;
    for(QMap<qreal, QMap<qreal, qreal> >::iterator j = d->m_data.begin(); j != d->m_data.end(); ++j) {
        for(QMap<qreal, qreal>::iterator i = (*j).begin(); i != (*j).end(); ++i) {
            newMap[j.key()][-i.key()] = d->m_data.value(j.key()).value(i.key());
        }
    }
    d->m_data = newMap;
}

void BilinearInterpolator::mirrorValues() {
    QMap<qreal, QMap<qreal, qreal> > newMap;
    for(QMap<qreal, QMap<qreal, qreal> >::iterator j = d->m_data.begin(); j != d->m_data.end(); ++j) {
        for(QMap<qreal, qreal>::iterator i = (*j).begin(); i != (*j).end(); ++i) {
            newMap[j.key()][i.key()] = -d->m_data.value(j.key()).value(i.key());
        }
    }
    d->m_data = newMap;
}

bool BilinearInterpolator::isValid() const {
    return xCoordinates().size() > 0;
}


BilinearInterpolator::~BilinearInterpolator() {}

}
}
