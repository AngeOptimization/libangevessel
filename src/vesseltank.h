#ifndef VESSELTANK_H
#define VESSELTANK_H

#include "angevessel_export.h"

#include <ange/units/units.h>
#include <ange/utils/identifiable.h>

#include <QSharedPointer>
#include <QString>

namespace ange {
namespace vessel {
class BilinearInterpolator;
class TankPrivate;
class Vessel;
}
}

namespace ange {
namespace vessel {

/**
 * Tank for containing some liquid, like bunker, ballast water, diesel for generators or whatever.
 */
class ANGEVESSEL_EXPORT VesselTank : public ange::utils::Identifiable<VesselTank> {
    // VesselTank is Identifiable because it is used in undo/redo in stow

public:

    /**
     * Construct tank.
     * @param id id of tank (-1 to generate a new id)
     */
    VesselTank(QString description, QString tank_group, qreal massCapacity, qreal volCapacity, qreal density,
           qreal foreEnd, qreal aftEnd, const BilinearInterpolator& lcg, const BilinearInterpolator& vcg,
           const BilinearInterpolator& tcg, const BilinearInterpolator& fsm,
           bool is_ballast);

    /**
     * Destroy tank
     */
    ~VesselTank();

    /**
     * Equality operator
     */
    bool operator==(const VesselTank& rhs) const;

    /**
     * @return vessel tank is placed in
     */
    Vessel* vessel() const;

    /**
     * @return the description of the tank, e.g. "No.3 side-tank P"
     */
    QString description() const;

    /**
     * @return the weight capacity of the tank in kg, this is the minimum of volume capasity of the tank times the density and the mass capacity.
     */
    qreal capacity() const;

    /**
     * @return the mass capacity of the tank in kg.
     */
    qreal massCapacity() const;

    /**
    * @return the volume capacity of the tank in m^3.
    */
    qreal volCapacity() const;

    /**
     * @return the density of the contents of the tank in SI units, kg/m^3.
     */
    qreal density() const;

    /**
     * @return the longitude (x-axis in the coordinate system we use) component of the center of mass (lcg) in meter
     * from the common reference point.
     */
    qreal lcg(ange::units::Mass weight) const;

    /**
     * @return the vertical (y-axis in the coordinate system we use) component of the center of mass (vcg) in meter
     * from the common reference point.
     */
    qreal vcg(ange::units::Mass weight) const;

    /**
     * @return the transversal (z-axis in the coordinate system we use) component of the center of mass (tcg) in meter
     * from the common reference point.
     */
    qreal tcg(ange::units::Mass weight) const;

    /**
     * @return the liquide surface moment for the given volume of the tank in question. This moment contribute to the GM of the vessel.
     */
    qreal fsm(ange::units::Mass weight) const;

    /**
     * @return the length coordinate of the bowmost end of the tank in meter
     * from the common reference point.
     */
    qreal foreEnd() const;

    /**
     * @return the length coordinate of the sternmost end of the tank in meter
     * from the common reference point.
     */
    qreal aftEnd() const;

    /**
     * @return true if the tank extent has been set in the input data.
     */
    bool extentSet();

    /**
     * @return whether the tank can be used for ballasting, i.e. whether its content can be used
     * to alter the vessel's stability condition
     */
    bool isBallast() const;

    /**
     * @return the data used for calculating the lcg
     */
    const BilinearInterpolator& lcgData() const;

    /**
     * @return the data used for calculating the vcg
     */
    const BilinearInterpolator& vcgData() const;

    /**
     * @return the data used for calculating the tcg
     */
    const BilinearInterpolator& tcgData() const;

    /**
     * @return the data used for calculating the free surface moment
     */
    const BilinearInterpolator& fsmData() const;

    /**
     * @return string representing the tank group of the tank
     */
    QString tankGroup() const;

    /**
     * mirrors the tank longitudinally. Used when mirroring the vessel
     */
    void mirrorLongitudinally();

private:
    void setVessel(Vessel* vessel);
    friend class Vessel;
    QSharedPointer<TankPrivate> d;
};

QDebug ANGEVESSEL_EXPORT operator<<(QDebug dbg, const VesselTank* tank);
QDebug ANGEVESSEL_EXPORT operator<<(QDebug dbg, const VesselTank& tank);

}} // namespace ange::vessel

Q_DECLARE_METATYPE(ange::vessel::VesselTank*);
Q_DECLARE_METATYPE(const ange::vessel::VesselTank*);

#endif // VESSELTANK_H
