#ifndef SLOTSUMMARY_H
#define SLOTSUMMARY_H

#include "angevessel_export.h"

#include <QList>
#include <QScopedPointer>

namespace ange {
namespace vessel {
class BaySlice;
}
}

namespace ange {
namespace vessel {

/**
 * A class summarizing the slot table of a vessel
 */
class ANGEVESSEL_EXPORT SlotTableSummary {

public:

    SlotTableSummary(const QList<BaySlice*>& baySlices);

    SlotTableSummary(const SlotTableSummary& other);

    ~SlotTableSummary();

    /**
     * @return total capacity of the vessel in TEU
     */
    int totalTEU() const;

    /**
     * @return total number of reefer plugs on the vessel
     */
    int reeferPlugs() const;

private:
    class SlotTableSummaryPrivate;
    QScopedPointer<SlotTableSummaryPrivate> d;
};

} //namespace vessel
} //namespace ange

#endif // SLOTSUMMARY_H
