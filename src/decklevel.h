#ifndef LIBANGEVESSEL_DECKLEVEL_H
#define LIBANGEVESSEL_DECKLEVEL_H

#include "angevessel_export.h"

#include <QDebug>

namespace ange {
namespace vessel {

enum DeckLevel {
    Above = 'A',
    Below = 'B',
};

inline bool operator<(DeckLevel lhs, DeckLevel rhs) {
    if (lhs != rhs) {
        return lhs == Below;
    }
    return false;
}

inline char toChar(DeckLevel level) {
    return level == Above ? 'A' : 'B';
}

inline QDebug operator<<(QDebug dbg, DeckLevel level) {
    return dbg << toChar(level);
}

}
}

#endif // LIBANGEVESSEL_DECKLEVEL_H
