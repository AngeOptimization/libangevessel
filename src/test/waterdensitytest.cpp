#include "../vessel.h"
#include "../bayslice.h"
#include "../bilinearinterpolator.h"

#include <QtTest/QtTest>

using ange::units::Density;
using ange::units::kilogram_per_meter3;
using ange::units::Length;
using ange::units::Mass;
using ange::units::meter;

using ange::vessel::BaySlice;
using ange::vessel::BilinearInterpolator;
using ange::vessel::HatchCover;
using ange::vessel::Vessel;
using ange::vessel::VesselTank;


namespace QTest {
    inline char* toString(Length length) {
        return QTest::toString<QString>(QString::number(length/meter));
    }
};


class WaterDensityTest : public QObject {
    Q_OBJECT
    public:
        WaterDensityTest()
            : vessel(QList<BaySlice*>(), QList<HatchCover*>(), QList<VesselTank*>(), QString("name"), QString("imo_number"), QString("callsign"), QString("vessel_code")) {
                QList<qreal> xWeight;
                QList<qreal> yLCG;
                QList<qreal> zDraft;
                QList<qreal> zTrim;
                xWeight << 15940 << 26574 << 38127 << 50744 << 64930 << 72442;
                yLCG << 4.0;  // do not have 2-D grid of values, therefore using 1-D data only
                zDraft << 4.0 << 6.0 << 8.0 << 10.0 << 12.0 << 13.0; // from simplified UASC A4 hydrostatics table
                zTrim << 1.0 << 2.0 << 2.8 << 3.2 << 2.6 << 2.2;  // freely invented

                BilinearInterpolator draftTable(xWeight, yLCG, zDraft);
                vessel.setDraftData(draftTable);

                BilinearInterpolator trimTable(xWeight, yLCG, zTrim);
                vessel.setTrimData(trimTable);

            }
    private Q_SLOTS:
        void initTestCase();
        void testVesselDraftTrim();
        void testVesselDraftTrim_data();
    private:
        Vessel vessel;
};

void WaterDensityTest::initTestCase(){
}

void WaterDensityTest::testVesselDraftTrim_data() {
    QTest::addColumn<Mass>("mass");
    QTest::addColumn<Length>("lcg");
    QTest::addColumn<Density>("density");
    QTest::addColumn<Length>("draft");
    QTest::addColumn<Length>("trim");

    // testing default ocean water density
    QTest::newRow("01") << Mass(15940) << Length(4.85) << Vessel::oceanWaterDensity << Length(4.0) << Length(1.0);
    QTest::newRow("02") << Mass(20000) << Length(4.0) << Vessel::oceanWaterDensity << Length(4.76359) << Length(1.38179);
    QTest::newRow("03") << Mass(30000) << Length(4.0) << Vessel::oceanWaterDensity << Length(6.59309) << Length(2.23724);
    QTest::newRow("04") << Mass(40000) << Length(4.0) << Vessel::oceanWaterDensity << Length(8.2969) << Length(2.85938);
    QTest::newRow("05") << Mass(50000) << Length(4.0) << Vessel::oceanWaterDensity << Length(9.88206) << Length(3.17641);
    QTest::newRow("06") << Mass(60000) << Length(4.0) << Vessel::oceanWaterDensity << Length(11.3049) << Length(2.80852);
    QTest::newRow("07") << Mass(70000) << Length(4.0) << Vessel::oceanWaterDensity << Length(12.6749) << Length(2.33003);

    // testing fresh water desity
    Density density = 995 * kilogram_per_meter3;
    QTest::newRow("11") << Mass(15940) << Length(4.85) << density << Length(4.09039) << Length(1.04519);
    QTest::newRow("12") << Mass(20000) << Length(4.0) << density << Length(4.877) << Length(1.4385);
    QTest::newRow("13") << Mass(30000) << Length(4.0) << density << Length(6.74968) << Length(2.29987);
    QTest::newRow("14") << Mass(40000) << Length(4.0) << density << Length(8.48808) << Length(2.89762);
    QTest::newRow("15") << Mass(50000) << Length(4.0) << density << Length(10.1076) << Length(3.16771);
    QTest::newRow("16") << Mass(60000) << Length(4.0) << density << Length(11.56) << Length(2.732 );
    QTest::newRow("17") << Mass(70000) << Length(4.0) << density << Length(12.9559) << Length(2.21765);
}

void WaterDensityTest::testVesselDraftTrim() {

    const Length delta(0.01); // 1 cm

    QFETCH(Mass, mass);
    QFETCH(Length, lcg);
    QFETCH(Density, density);
    QFETCH(Length, draft);
    QFETCH(Length, trim);

    // qDebug() << vessel.draft(mass, lcg, density)/meter;
    QVERIFY(vessel.draft(mass, lcg, density) >= draft - delta);
    QVERIFY(vessel.draft(mass, lcg, density) <= draft + delta);

    // qDebug() << vessel.trim(mass, lcg, density)/meter;
    QVERIFY(vessel.trim(mass, lcg, density) >= trim - delta);
    QVERIFY(vessel.trim(mass, lcg, density) <= trim + delta);
}

#include "waterdensitytest.moc"
QTEST_MAIN(WaterDensityTest);
