#include <QtTest>
#include "../blockweight.h"
#include <cmath>

using namespace ange::units;
using namespace ange::vessel;

static const Acceleration gravity_constant = 9.81 * meter / (second * second);

class BlockWeightTest : public QObject {
    Q_OBJECT
public:
    private Q_SLOTS:
        void testDefaultConstructor();
        void testExtendingAndReducing();
        void testAssignmentOperator();
        void testTorqueCalculations();
        void testFwdAftLimitLogic();
        void testNegativeAnd_0_WeightBlocks();
        void test_0_Density();
        void test_0_LcgAndTorque();
        void testTorqueLeftAndRightOf();
        void testWeightAftOf();
        void testReversedAxis();
        void testLcgSensitivityNearZero();
        void testMoveCoordinatesNearZeroLcg();
};

void BlockWeightTest::testDefaultConstructor() {
    BlockWeight blockWeight;
    QCOMPARE(blockWeight.weight()/kilogram, 0.0);
}

void BlockWeightTest::testExtendingAndReducing() {
    BlockWeight blockWeight1;
    BlockWeight blockWeight2 = BlockWeight(10.0*meter, 9.0*meter, 11.0*meter,10.0*meter, 10.0*meter, 10*ton);
    blockWeight1.extendWith(blockWeight2);
    BlockWeight blockWeight3 = BlockWeight(20.0*meter, 19.0*meter, 21.0*meter, 30.0*meter, 10.0*meter, 100*ton);
    blockWeight1.extendWith(blockWeight3);
    QCOMPARE(blockWeight1.weight()/kilogram, blockWeight2.weight()/kilogram + blockWeight3.weight()/kilogram);
    blockWeight1.extendWith(blockWeight2);
    blockWeight1.extendWith(blockWeight3.oppositeWeight());
    blockWeight1.extendWith(blockWeight2.oppositeWeight());
    QCOMPARE(blockWeight1.weight()/kilogram, blockWeight2.weight()/kilogram);
    QCOMPARE(blockWeight1.lcg()/meter, blockWeight2.lcg()/meter);
    QCOMPARE(blockWeight1.vcg()/meter, blockWeight2.vcg()/meter);
    QCOMPARE(blockWeight1.tcg()/meter, blockWeight2.tcg()/meter);
}

void BlockWeightTest::testAssignmentOperator() {
    BlockWeight blockWeight4 = BlockWeight();
    QCOMPARE(blockWeight4.weight()/kilogram, 0.0);
}

void BlockWeightTest::testTorqueCalculations() {
    BlockWeight blockWeight5(10*meter, 9*meter, 11*meter, 5*meter, 3*meter, 10*ton);
    MassMoment longitudinalTorque1 = blockWeight5.weight() * blockWeight5.lcg();
    MassMoment longitudinal_torque2 = blockWeight5.longitudinalMoment();
    QCOMPARE(longitudinalTorque1 / tonmeter, longitudinal_torque2 / tonmeter);
    MassMoment verticalTorque1 = blockWeight5.weight() * blockWeight5.vcg();
    MassMoment verticalTorque2 = blockWeight5.verticalMoment();
    QCOMPARE(verticalTorque1 / tonmeter , verticalTorque2 / tonmeter);
    MassMoment transverseTorque1 = blockWeight5.weight() * blockWeight5.tcg();
    MassMoment transverseTorque2 = blockWeight5.transverseMoment();
    QCOMPARE(transverseTorque1 / tonmeter, transverseTorque2 / tonmeter);
}

void BlockWeightTest::testFwdAftLimitLogic() {
    BlockWeight blockWeight6(10*meter, 7*meter, 12*meter, 5*meter, 3*meter, 10*ton);
    BlockWeight blockWeight7 = blockWeight6;
    blockWeight7.setAftLimit(5*meter);
    blockWeight7.setAftLimit(13*meter);
    QCOMPARE(blockWeight6.weight()/ton, blockWeight7.weight()/ton);
    QCOMPARE(blockWeight6.lcg()/meter, blockWeight7.lcg()/meter);
}

void BlockWeightTest::testNegativeAnd_0_WeightBlocks() {
    BlockWeight blockWeight_0_Weight(10*meter, -5000*kilogram_per_meter, 20*meter,
                                         5000*kilogram_per_meter, 0*meter, 0*meter);
    QCOMPARE(blockWeight_0_Weight.aftDensity()/kilogram_per_meter, -5000.0);
    BlockWeight blockWeight7Positive(10*meter, 5000*kilogram_per_meter, 20*meter,
                                          5000*kilogram_per_meter, 0*meter,0*meter);
    BlockWeight blockWeight7Negative(20*meter, -5000*kilogram_per_meter, 30*meter,
                                          -5000*kilogram_per_meter, 0*meter,0*meter);
    BlockWeight blockWeight7Total;
    blockWeight7Total.extendWith(blockWeight7Positive);
    blockWeight7Total.extendWith(blockWeight7Negative);
    QCOMPARE(blockWeight7Total.weight()/kilogram, 0.0);
    BlockWeight blockWeightToBeMoved = blockWeight7Negative;
    blockWeightToBeMoved.setWeight( -1 * blockWeightToBeMoved.weight() );
    blockWeightToBeMoved.extendWith(blockWeight7Total);
    QCOMPARE(blockWeightToBeMoved.lcg()/meter, blockWeight7Positive.lcg()/meter);
    BlockWeight blockWeightOriginal(15*meter, 13*meter, 18*meter, 12*meter, 0.5*meter, 10*ton);
    BlockWeight blockWeightTarget(16*meter, 13.5*meter, 18*meter, 14*meter, 0.5*meter, 10*ton);
    BlockWeight correction = blockWeightTarget;
    correction.extendWith(blockWeightOriginal.oppositeWeight());
    BlockWeight adjusted = blockWeightOriginal;
    adjusted.extendWith(correction);
    QCOMPARE(blockWeightTarget.lcg()/meter, adjusted.lcg()/meter);
    QCOMPARE(blockWeightToBeMoved.weight()/kilogram, 50000.0);
}

void BlockWeightTest::test_0_Density() {
    BlockWeight blockWeight8(10*meter, 0*kilogram_per_meter, 20*meter,
                                 3000*kilogram_per_meter, 0*meter,0*meter);
    QVERIFY(qAbs(blockWeight8.aftDensity()/kilogram_per_meter) < 2e-12); // is 1.16e-12 on WinXP/MSYS2
    QCOMPARE(blockWeight8.foreDensity()/kilogram_per_meter, 3000.0);
}

void BlockWeightTest::test_0_LcgAndTorque() {
    BlockWeight blockWeight9(10*meter, 10000*kilogram_per_meter, 20*meter,
                                 0*kilogram_per_meter, 0*meter,0*meter);
    QCOMPARE(blockWeight9.foreDensity()/kilogram_per_meter, 0.0);
    QCOMPARE(blockWeight9.aftDensity()/kilogram_per_meter, 10000.0);
    BlockWeight blockWeight10(0.0*meter, -7.0*meter, 7.0*meter, 0.0*meter, 0.0*meter, 50000*kilogram);
    QCOMPARE(blockWeight10.lcg()/meter, 0.0);
    BlockWeight blockWeight11(0.0*meter, -4.0*meter, 7.0*meter,
                                  0.0*meter, 0.0*meter, 1000*kilogram);
    QVERIFY(qFuzzyIsNull(blockWeight11.lcg()/meter));
}

void BlockWeightTest::testTorqueLeftAndRightOf() {
    BlockWeight blockWeight12(0.0*meter, -10.0*meter, 10.0*meter, 0.0*meter, 0.0*meter, 1000*kilogram);
    QCOMPARE(blockWeight12.longitudinalMomentAftOf(0.0*meter)/(kilogram*meter), 500.0*5.0);
}

void BlockWeightTest::testWeightAftOf() {
    BlockWeight blockWeight(67.7*meter, 10*kilogram/meter, 79.3*meter, 10*kilogram/meter);
    QCOMPARE(blockWeight.weight()/kilogram, 116.0);
    QCOMPARE(blockWeight.weightAftOf(72.1*meter)/kilogram, 44.0);
    QCOMPARE(blockWeight.weightForeOf(72.1*meter)/kilogram, 116.0 - 44.0);
}

void BlockWeightTest::testReversedAxis() {
    BlockWeight block_weight(20*meter, 5000*kilogram_per_meter, 10*meter,
                                         5000*kilogram_per_meter, 0*meter, 0*meter);
    QCOMPARE(block_weight.lcg()/meter, 15.0);
    QCOMPARE(block_weight.weight()/ton, 50.0);
}

void BlockWeightTest::testLcgSensitivityNearZero() {

    const Length lcgZero = 0.0*meter;
    const Length lcgEpsilon = 2.58266e-15*meter;
    const Length lcgMinusFive = -5.0*meter;
    const Length tcg = 0.0*meter;
    const Length vcg = 10.0*meter;
    const Length aftLimit = -105.0*meter;
    const Length foreLimit = 95.0*meter;
    const Mass weight = 2000.0*ton;
    const LinearDensity expectedMiddleDensisty = 10*ton_per_meter;

    const BlockWeight blockMinusFiveLcg(lcgMinusFive, aftLimit, foreLimit, vcg, tcg, weight);

    {
        BlockWeight blockZeroLcg(lcgZero, aftLimit, foreLimit, vcg, tcg, weight);
        QVERIFY(qAbs((blockZeroLcg.weight() - weight)/kilogram) < 1.0);
        QVERIFY(qAbs((blockZeroLcg.lcg() - lcgZero)/millimeter) < 1.0);
        QVERIFY(qAbs((blockZeroLcg.tcg() - tcg)/millimeter) < 1.0);
        QVERIFY(qAbs((blockZeroLcg.vcg() - vcg)/millimeter) < 1.0);
        QVERIFY(qAbs((blockZeroLcg.aftLimit() - aftLimit)/millimeter) < 1.0);
        QVERIFY(qAbs((blockZeroLcg.foreLimit() - foreLimit)/millimeter) < 1.0);
        QVERIFY(qAbs(blockZeroLcg.aftDensity()/kilogram_per_meter - 0.85*expectedMiddleDensisty/kilogram_per_meter) < 1.0);
        QVERIFY(qAbs(blockZeroLcg.foreDensity()/kilogram_per_meter - 1.15*expectedMiddleDensisty/kilogram_per_meter) < 1.0);

        BlockWeight blockTotal = blockMinusFiveLcg;
        blockTotal.extendWith(blockZeroLcg);
        QVERIFY(qAbs((blockTotal.weight() - 2.0*weight)/kilogram) < 1.0);
        QVERIFY(blockTotal.lcg() > lcgMinusFive);
        QVERIFY(blockTotal.lcg() < lcgZero);
        QVERIFY(qAbs((blockTotal.tcg() - tcg)/millimeter) < 1.0);
        QVERIFY(qAbs((blockTotal.vcg() - vcg)/millimeter) < 1.0);
        QVERIFY(qAbs((blockTotal.aftLimit() - aftLimit)/millimeter) < 1.0);
        QVERIFY(qAbs((blockTotal.foreLimit() - foreLimit)/millimeter) < 1.0);
        QVERIFY(qAbs(blockTotal.aftDensity()/kilogram_per_meter - 1.85*expectedMiddleDensisty/kilogram_per_meter) < 1.0);
        QVERIFY(qAbs(blockTotal.foreDensity()/kilogram_per_meter - 2.15*expectedMiddleDensisty/kilogram_per_meter) < 1.0);
    }

    {
        BlockWeight blockEpsilonLcg(lcgEpsilon, aftLimit, foreLimit, vcg, tcg, weight);
        QVERIFY(qAbs((blockEpsilonLcg.weight() - weight)/kilogram) < 1.0);
        QVERIFY(qAbs((blockEpsilonLcg.lcg() - lcgEpsilon)/millimeter) < 1.0);
        QVERIFY(qAbs((blockEpsilonLcg.tcg() - tcg)/millimeter) < 1.0);
        QVERIFY(qAbs((blockEpsilonLcg.vcg() - vcg)/millimeter) < 1.0);
        QVERIFY(qAbs((blockEpsilonLcg.aftLimit() - aftLimit)/millimeter) < 1.0);
        QVERIFY(qAbs((blockEpsilonLcg.foreLimit() - foreLimit)/millimeter) < 1.0);
        const QString fixmeAft = QString("blockEpsilonLcg has wrong aft density %1 t/m - should be close to 8.5 t/m, ticket #1609").arg(blockEpsilonLcg.aftDensity()/ton_per_meter);
        QEXPECT_FAIL("", fixmeAft.toUtf8().constData(), Continue);
        QVERIFY(qAbs(blockEpsilonLcg.aftDensity()/kilogram_per_meter - 0.85*expectedMiddleDensisty/kilogram_per_meter) < 1.0);
        const QString fixmeFore = QString("blockEpsilonLcg has wrong fore density %1 t/m - should be close to 11.5 t/m, ticket #1609").arg(blockEpsilonLcg.foreDensity()/ton_per_meter);
        QEXPECT_FAIL("", fixmeFore.toUtf8().constData(), Continue);
        QVERIFY(qAbs(blockEpsilonLcg.foreDensity()/kilogram_per_meter - 1.15*expectedMiddleDensisty/kilogram_per_meter) < 1.0);

        BlockWeight blockTotal = blockMinusFiveLcg;
        blockTotal.extendWith(blockEpsilonLcg);
        QVERIFY(qAbs((blockTotal.weight() - 2.0*weight)/kilogram) < 1.0);
        QVERIFY(blockTotal.lcg() > lcgMinusFive);
        QVERIFY(blockTotal.lcg() < lcgEpsilon);
        QVERIFY(qAbs((blockTotal.tcg() - tcg)/millimeter) < 1.0);
        QVERIFY(qAbs((blockTotal.vcg() - vcg)/millimeter) < 1.0);
        QVERIFY(qAbs((blockTotal.aftLimit() - aftLimit)/millimeter) < 1.0);
        QVERIFY(qAbs((blockTotal.foreLimit() - foreLimit)/millimeter) < 1.0);
        QVERIFY(qAbs(blockTotal.aftDensity()/kilogram_per_meter - 1.85*expectedMiddleDensisty/kilogram_per_meter) < 1.0);
        QVERIFY(qAbs(blockTotal.foreDensity()/kilogram_per_meter - 2.15*expectedMiddleDensisty/kilogram_per_meter) < 1.0);
    }
}

void BlockWeightTest::testMoveCoordinatesNearZeroLcg() {
    const Length lcgZero = 0.0*meter;
    const Length epsilon = 1.0e-15*meter;
    const Length one = 1.0*meter;
    const Length tcg = 0.0*meter;
    const Length vcg = 10.0*meter;
    const Length aftLimit = -5.0*meter;
    const Length foreLimit = 15.0*meter;
    const Length aft = (3*aftLimit + foreLimit)/4.0;
    const Length middle = (aftLimit + foreLimit)/2.0;
    const Length fore = (aftLimit + 3*foreLimit)/4.0;
    const Mass weight = 20.0*ton;

    // construct three identical blockweights, which are translated 0, epsilon and 1 meter, but should otherwise behave the same
    BlockWeight blockZero(lcgZero, aftLimit, foreLimit, vcg, tcg, weight);
    BlockWeight blockEpsilon(lcgZero + epsilon, aftLimit + epsilon, foreLimit + epsilon, vcg, tcg, weight);
    BlockWeight blockOne(lcgZero + one, aftLimit + one, foreLimit + one, vcg, tcg, weight);

    QVERIFY(qAbs((blockOne.aftDensity()-blockZero.aftDensity())/(kilogram/meter)) < 0.001);
    const QString fixmeAft = QString("blockEpsilon has wrong aft density %1 t/m - should be %2 t/m, ticket #1609")
    .arg(blockEpsilon.aftDensity()/ton_per_meter).arg(blockZero.aftDensity()/ton_per_meter);
    QEXPECT_FAIL("", fixmeAft.toUtf8().constData(), Continue);
    QVERIFY(qAbs((blockOne.aftDensity()-blockEpsilon.aftDensity())/(kilogram/meter)) < 0.001);

    QVERIFY(qAbs((blockOne.foreDensity()-blockZero.foreDensity())/(kilogram/meter)) < 0.001);
    const QString fixmeFore = QString("blockEpsilon has wrong aft density %1 t/m - should be %2 t/m, ticket #1609")
    .arg(blockEpsilon.foreDensity()/ton_per_meter).arg(blockZero.foreDensity()/ton_per_meter);
    QEXPECT_FAIL("", fixmeFore.toUtf8().constData(), Continue);
    QVERIFY(qAbs((blockOne.foreDensity()-blockEpsilon.foreDensity())/(kilogram/meter)) < 0.001);

    QVERIFY(qAbs((blockOne.weightAftOf(aftLimit+one)-blockZero.weightAftOf(aftLimit))/kilogram) < 0.001);
    QVERIFY(qAbs((blockOne.weightAftOf(aftLimit+one)-blockEpsilon.weightAftOf(aftLimit+epsilon))/kilogram) < 0.001);

    QVERIFY(qAbs((blockOne.weightAftOf(aft+one)-blockZero.weightAftOf(aft))/kilogram) < 0.001);
    const QString fixmeWeightAft = QString("blockEpsilon has wrong weight aft %1 t - should be %2 t, ticket #1609")
    .arg(blockEpsilon.weightAftOf(aft+epsilon)/ton).arg(blockZero.weightAftOf(aft+one)/ton);
    QEXPECT_FAIL("", fixmeWeightAft.toUtf8().constData(), Continue);
    QVERIFY(qAbs((blockOne.weightAftOf(aft+one)-blockEpsilon.weightAftOf(aft+epsilon))/kilogram) < 0.001);

    QVERIFY(qAbs((blockOne.weightAftOf(middle+one)-blockZero.weightAftOf(middle))/kilogram) < 0.001);
    QEXPECT_FAIL("", "block with LCG near zero has wrong weightAftOf value, ticket #1609", Continue);
    QVERIFY(qAbs((blockOne.weightAftOf(middle+one)-blockEpsilon.weightAftOf(middle+epsilon))/kilogram) < 0.001);

    QVERIFY(qAbs((blockOne.weightAftOf(fore+one)-blockZero.weightAftOf(fore))/kilogram) < 0.001);
    QEXPECT_FAIL("", "block with LCG near zero has wrong weightAftOf value, ticket #1609", Continue);
    QVERIFY(qAbs((blockOne.weightAftOf(fore+one)-blockEpsilon.weightAftOf(fore+epsilon))/kilogram) < 0.001);

    QVERIFY(qAbs((blockOne.weightAftOf(foreLimit+one)-blockZero.weightAftOf(foreLimit))/kilogram) < 0.001);
    QVERIFY(qAbs((blockOne.weightAftOf(foreLimit+one)-blockEpsilon.weightAftOf(foreLimit+epsilon))/kilogram) < 0.001);
}

#include "blockweighttest.moc"
QTEST_MAIN(BlockWeightTest);
