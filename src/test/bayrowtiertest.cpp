#include <QtTest>

#include "bayrowtier.h"

#include <stdexcept>

using namespace ange::vessel;

class BayRowTierTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testToString();
    void benchmark1();
    void benchmark2();
    void benchmark3();
    void benchmark4();
};
QTEST_MAIN(BayRowTierTest)

void BayRowTierTest::testToString() {
    QCOMPARE(BayRowTier(10, 0, 82).toString(), QStringLiteral("10,0,82"));
}

class BenchmarkBayRowTier {
public:
    BenchmarkBayRowTier() : m_bay(10), m_row(0), m_tier(82) {}
    QString toString1() const {
        return QString::fromLocal8Bit("%1,%2,%3").arg(m_bay).arg(m_row).arg(m_tier);
    }
    QString toString2() const {
        return QString::number(m_bay) + "," + QString::number(m_row) + "," + QString::number(m_tier);
    }
    QString toString3() const {
        return QString().sprintf("%d,%d,%d", m_bay, m_row, m_tier);
    }
    QString toString4() const {
        const int size = 12;
        char buffer[size];
        int rv = snprintf(buffer, size, "%d,%d,%d", m_bay, m_row, m_tier);
        if (rv >= size) {
            throw std::runtime_error("truncate in toString()");
        }
        return QString::fromLatin1(buffer);
    }
private:
    int m_bay;
    int m_row;
    int m_tier;
};

void BayRowTierTest::benchmark1() {
    BenchmarkBayRowTier brt;
    QBENCHMARK {
        brt.toString1();
    }
}

void BayRowTierTest::benchmark2() {
    BenchmarkBayRowTier brt;
    QBENCHMARK {
        brt.toString2();
    }
}

void BayRowTierTest::benchmark3() {
    BenchmarkBayRowTier brt;
    QBENCHMARK {
        brt.toString3();
    }
}

void BayRowTierTest::benchmark4() {
    BenchmarkBayRowTier brt;
    QBENCHMARK {
        brt.toString4();
    }
}

#include "bayrowtiertest.moc"
