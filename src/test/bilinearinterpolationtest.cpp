#include "../bilinearinterpolator.h"
#include <QtTest/QtTest>

class BilinearInterpolationTest : public QObject {
    Q_OBJECT
public:
private Q_SLOTS:
    void initTestCase();
    void test1dXonlyInterpolation();
    void test1dYonlyInterpolation();
    void testSingularityInterpolation();
    void test2dInterpolation();
    void test8extrapolations();
    void testMirrorXAxis();
    void testMirrorYAxis();
    void testMirrorValues();
    void testDataExtraction();
};

void BilinearInterpolationTest::initTestCase(){
}

void BilinearInterpolationTest::test1dXonlyInterpolation() {
    QList<qreal> xCoordinates;
    xCoordinates << 0.0 << 0.2 << 1.0;
    QList<qreal> yCoordinates;
    yCoordinates << 0.0;
    QList <qreal> samples;
    samples << 0.0 << 1.0 << 0.0;
    ange::vessel::BilinearInterpolator B(xCoordinates, yCoordinates, samples);
    QCOMPARE(B.valueAt(0.0, 0.0), 0.0);
    QCOMPARE(B.valueAt(0.0, 10.0), 0.0);
    QCOMPARE(B.valueAt(0.1, 0.0), 0.5);
    QCOMPARE(B.valueAt(0.1, -10.0), 0.5);
    QCOMPARE(B.valueAt(0.2, 0.0), 1.0);
    QCOMPARE(B.valueAt(0.2, 10.0), 1.0);
    QCOMPARE(B.valueAt(0.6, 0.0), 0.5);
    QCOMPARE(B.valueAt(0.6, 10.0), 0.5);
    QCOMPARE(B.valueAt(1.0, 0.0), 0.0);
    QCOMPARE(B.valueAt(1.0, -10.0), 0.0);
}

void BilinearInterpolationTest::test1dYonlyInterpolation() {
    QList<qreal> xCoordinates;
    xCoordinates << 0.0;
    QList<qreal> yCoordinates;
    yCoordinates << 0.0 << 0.2 << 1.0;
    QList <qreal> samples;
    samples << 0.0 << 1.0 << 0.0;
    ange::vessel::BilinearInterpolator B(xCoordinates, yCoordinates, samples);
    QCOMPARE(B.valueAt(0.0, 0.0), 0.0);
    QCOMPARE(B.valueAt(10.0, 0.0), 0.0);
    QCOMPARE(B.valueAt(0.0, 0.1), 0.5);
    QCOMPARE(B.valueAt(-10.0, 0.1), 0.5);
    QCOMPARE(B.valueAt(0.0, 0.2), 1.0);
    QCOMPARE(B.valueAt(10.0, 0.2), 1.0);
    QCOMPARE(B.valueAt(0.0, 0.6), 0.5);
    QCOMPARE(B.valueAt(10.0, 0.6), 0.5);
    QCOMPARE(B.valueAt(0.0, 1.0), 0.0);
    QCOMPARE(B.valueAt(-10.0, 1.0), 0.0);
}

void BilinearInterpolationTest::testSingularityInterpolation() {
    QList<qreal> xCoordinates;
    xCoordinates << 10.0;
    QList<qreal> yCoordinates;
    yCoordinates << 10.0;
    QList <qreal> samples;
    samples << 100.0;
    ange::vessel::BilinearInterpolator B(xCoordinates, yCoordinates, samples);
    QCOMPARE(B.valueAt(0.1, 0.0), 100.0);
    QCOMPARE(B.valueAt(0.1, 10.0), 100.0);
}

void BilinearInterpolationTest::test2dInterpolation() {
    QList<qreal> xCoordinates;
    xCoordinates << 0.0 << 1.0;
    QList<qreal> yCoordinates;
    yCoordinates << 0.0 << 1.0;
    QList <qreal> samples;
    samples << 0.0 << 0.0 << 2.0 << 2.0;
    ange::vessel::BilinearInterpolator B(xCoordinates, yCoordinates, samples);
    QCOMPARE(B.valueAt(0.0, 0.5), 1.0);
    QCOMPARE(B.valueAt(0.5, 0.5), 1.0);
    QCOMPARE(B.valueAt(1.0, 0.5), 1.0);

    QCOMPARE(B.valueAt(0.5, 0.0), 0.0);
    QCOMPARE(B.valueAt(0.5, 0.5), 1.0);
    QCOMPARE(B.valueAt(0.5, 1.0), 2.0);
}

void BilinearInterpolationTest::test8extrapolations() {
    QList<qreal> xCoordinates;
    xCoordinates << 0.0 << 1.0;
    QList<qreal> yCoordinates;
    yCoordinates << 0.0 << 1.0;
    QList <qreal> samples;
    samples << 0.0 << 0.0 << 2.0 << 2.0;
    ange::vessel::BilinearInterpolator B(xCoordinates, yCoordinates, samples);
    QCOMPARE(B.valueAt(-0.5, -0.5), 0.0);
    QCOMPARE(B.valueAt(-0.5, 0.5), 1.0);
    QCOMPARE(B.valueAt(-0.5, 1.5), 2.0);
    QCOMPARE(B.valueAt(0.5, 1.5), 2.0);
    QCOMPARE(B.valueAt(1.5, 1.5), 2.0);
    QCOMPARE(B.valueAt(1.5, 0.5), 1.0);
    QCOMPARE(B.valueAt(1.5, -0.5), 0.0);
    QCOMPARE(B.valueAt(0.5, -0.5), 0.0);
}

void BilinearInterpolationTest::testMirrorXAxis() {
    QList<qreal> xCoordinates;
    xCoordinates << 0.0 << 1.0;
    QList<qreal> yCoordinates;
    yCoordinates << 0.0 << 1.0;
    QList <qreal> samples;
    samples << 0.0 << 0.0 << 2.0 << 2.0;
    ange::vessel::BilinearInterpolator B(xCoordinates, yCoordinates, samples);
    B.mirrorXaxis();
    QCOMPARE(B.valueAt(-0.5, 0.5), 1.0);
}

void BilinearInterpolationTest::testMirrorYAxis() {
    QList<qreal> xCoordinates;
    xCoordinates << 0.0 << 1.0;
    QList<qreal> yCoordinates;
    yCoordinates << 0.0 << 1.0;
    QList <qreal> samples;
    samples << 0.0 << 0.0 << 2.0 << 2.0;
    ange::vessel::BilinearInterpolator B(xCoordinates, yCoordinates, samples);
    B.mirrorYaxis();
    QCOMPARE(B.valueAt(0.5, -0.5), 1.0);
}

void BilinearInterpolationTest::testMirrorValues() {
    QList<qreal> xCoordinates;
    xCoordinates << 0.0 << 1.0;
    QList<qreal> yCoordinates;
    yCoordinates << 0.0 << 1.0;
    QList <qreal> samples;
    samples << 0.0 << 0.0 << 2.0 << 2.0;
    ange::vessel::BilinearInterpolator B(xCoordinates, yCoordinates, samples);
    B.mirrorValues();
    QCOMPARE(B.valueAt(0.5, 0.5), -1.0);
}

void BilinearInterpolationTest::testDataExtraction() {
    QList<qreal> xCoordinates;
    xCoordinates << 1.0/7.0 << 1.0/3.0;
    QList<qreal> yCoordinates;
    yCoordinates << 1.0/7.0 << 1.0/3.0;
    QList <qreal> samples;
    samples << 1.0/7.0 << 1.0/7.0 << 1.0/11.0 << 1.0/11.0;
    ange::vessel::BilinearInterpolator B(xCoordinates, yCoordinates, samples);
    QCOMPARE(B.valueAt(1.0/7.0, 1.0/7.0), 1.0/7.0);
    QCOMPARE(B.valueAt(1.0/7.0, 1.0/3.0), 1.0/11.0);
    QCOMPARE(B.valueAt(1.0/3.0, 1.0/7.0), 1.0/7.0);
    QCOMPARE(B.valueAt(1.0/3.0, 1.0/3.0), 1.0/11.0);
}


#include "bilinearinterpolationtest.moc"
QTEST_MAIN(BilinearInterpolationTest);
