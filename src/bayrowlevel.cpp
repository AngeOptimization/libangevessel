#include "bayrowlevel.h"

#include <QString>

namespace ange {
namespace vessel {

BayRowLevel::BayRowLevel()
  : m_bay(0), m_row(0), m_level(Above)
{
    // Empty
}

BayRowLevel::BayRowLevel(int bay, int row, DeckLevel level)
  : m_bay(bay), m_row(row), m_level(level)
{
    Q_ASSERT(!isUnknown());
}

BayRowLevel BayRowLevel::fromUrn(QString urn) {
    // Convert from URN in this format: "urn:stowbase.org:vessel:bay=2,row=16,level=ABOVE".
    QRegExp regexp("urn:stowbase\\.org:vessel:bay=(\\d+),row=(\\d+),level=([A-Z]+)");
    if (!regexp.exactMatch(urn)) {
        return BayRowLevel();
    }
    bool ok;
    int bay = regexp.cap(1).toInt(&ok);
    if (!ok) {
        return BayRowLevel();
    }
    int row = regexp.cap(2).toInt(&ok);
    if (!ok) {
        return BayRowLevel();
    }
    QString levelString = regexp.cap(3);
    DeckLevel level;
    if (levelString == "ABOVE") {
        level = Above;
    } else if (levelString == "BELOW") {
        level = Below;
    } else {
        return BayRowLevel();
    }
    return BayRowLevel(bay, row, level);
}

QString BayRowLevel::toString() const {
    if (m_bay == 0) {
        return QStringLiteral("Unknown");
    } else {
        return QStringLiteral("%1,%2,%3").arg(m_bay).arg(m_row).arg(toChar(m_level));
    }
}

QDebug operator<<(QDebug dbg, const BayRowLevel& brl) {
    return dbg << brl.toString();
}

}} // namespace ange::vessel
