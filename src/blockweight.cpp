#include "blockweight.h"

#include "vessel.h"

#include <qnumeric.h> // workaround for a Qt 5.2 bug

#include <limits>
#include <cmath>

using namespace ange::units;

namespace ange {
namespace vessel {

/**
 * A class containing the data of the block_weight
 * The data is basically represented in the form of lcgs and torques (and a weight).
 * This was chosen over a basis representation based on fore and aft densities
 * as a matter of taste. The class provides both representations in its interface.
 * Using torque (normalized, i.e. without G) allows to represent a block weight's
 * negative and positive weight contributions.
 * In this way we can have a "0-weight" weight block, potentially with
 * large but exactly opposite positive and negative parts.
 * Such a block would in effect displace the LCG without modifying the weight.
 * That is for instance practical when adjusting weight blocks.
 *
 * Special care needs to be taken in the conversions to/from densities in the cases
 * when the weight, lcg, densities and/or torque are 0.
 */
class BlockWeight::BlockWeightPrivate : public QSharedData {

    /**
     * the shape of the block weight is density = a*x + b
     * with a = (fore_density - aft_density)/(fore_limit - aft_limit)
     * and b = aft_density() - aft_limit() * a;
     * the shape of the torque is density * x = a*x*x + b*x
     * the total torque of the blockweight is thus
     * torque = [1/3 * a * x^3 + 1/2 * b * x^2]{aft_limit, fore_limit}
     *
     * A couple of intermediate steps:
     * Unit<0,2,0> rv = ((r - 1)/(x2 - x1) / 3.0 * (x2*x2*x2 - x1*x1*x1)
     *                +  (1 - x1*(r - 1)/(x2 - x1)) / 2.0 * (x2*x2 - x1*x1));
     * Unit<0,2,0> rv = r/(x2 - x1)* (1 / 3.0 * (x2*x2*x2 - x1*x1*x1) - x1 / 2.0 * (x2*x2 - x1*x1))
     *                - 1/(x2 - x1)* (1 / 3.0 * (x2*x2*x2 - x1*x1*x1) - x1 / 2.0 * (x2*x2 - x1*x1))
     *                +  1 / 2.0 * (x2*x2 - x1*x1);
     * in the torque_density_ratio_components_t, coefficient_1 is the coefficient of r (1st line), coefficient_0
     * is the constant (2 last lines)
     */
    typedef struct {
        Unit<0, 2, 0> coefficient_1;
        Unit<0, 2, 0> coefficient_0;
    } TorqueDensityRatioComponents;

public:

    void init(Length lcg, Length aft_limit, Length fore_limit, Length vcg, Length tcg, Mass weight, MassMoment torque);
    /**
     * Initializes the object given densities (based on the previously initialized aft and fore limits)
     */
    void init_from_densities(const LinearDensity aft_density, const LinearDensity fore_density);
    /**
     * @returns the torque of the block_weight
     */
    MassMoment torque_wrt_0(const LinearDensity aft_density, const LinearDensity fore_density) const;
    /**
     * @returns the ratio between the torque and the aft density
     */
    TorqueDensityRatioComponents torque_density_ratio() const;
    /**
     *  @returns the ratio of the densities (fore_density/aft_density)
     */
    double density_ratio() const;
    /**
     * @returns the aft density
     */
    LinearDensity aft_density() const;
    /**
     * @returns the fore density
     */
    LinearDensity fore_density() const;

public:

    Length m_lcg;
    Length m_aft_limit;
    Length m_fore_limit;
    Length m_vcg;
    Length m_tcg;
    Mass m_weight;
    MassMoment m_torque;
    QString m_description;
};

// FIXME this init function could be replaced by a similar constructor on BlockWeightPrivate
void BlockWeight::BlockWeightPrivate::init(Length lcg, Length aft_limit, Length fore_limit, Length vcg, Length tcg,
                                           Mass weight, MassMoment torque) {
    m_lcg = lcg;
    m_aft_limit = aft_limit;
    m_fore_limit = fore_limit;
    m_vcg = vcg;
    m_tcg = tcg;
    m_weight = weight;
    m_torque = torque;
}

void BlockWeight::BlockWeightPrivate::init_from_densities(LinearDensity aft_density, LinearDensity fore_density) {
    if (m_aft_limit > m_fore_limit) {
        qWarning() << "Warning: m_aft_limit > m_fore_limit:" <<  m_aft_limit / meter << ">" << m_fore_limit / meter
                   << "weight:" << m_weight / kilogram;
    }
    m_weight = 0.0 * kilogram;
    m_torque = 0.0 * kilogrammeter;
    Length block_length = abs(m_fore_limit - m_aft_limit);
    if (qFuzzyIsNull(aft_density / kilogram_per_meter) && qFuzzyIsNull(fore_density / kilogram_per_meter)) {
        m_lcg = 0.5 * (m_aft_limit + m_fore_limit);
        return;
    }
    //this will send the LCG to infinity if the weight is 0. That's ok.
    m_torque = torque_wrt_0(aft_density, fore_density);
    m_weight = block_length * (aft_density + fore_density) / 2;
    m_lcg = m_torque / m_weight;
    return;
}

MassMoment BlockWeight::BlockWeightPrivate::torque_wrt_0(LinearDensity aft_density, LinearDensity fore_density) const {
    MassMoment torque;
    TorqueDensityRatioComponents R = torque_density_ratio();
    if (qFuzzyIsNull(aft_density / kilogram_per_meter)) {
        /**
         * developing the integral [1/3*ax^3 + 1/2*bx^2]{aft_limit, fore_limit}
         * reduces to:
         */
        torque = fore_density * R.coefficient_1;
        return torque;
    }
    //Note: the case fore_density == 0 is gracefully handled below
    torque = aft_density * (fore_density / aft_density * R.coefficient_1 + R.coefficient_0);
    //make sure the integration is eventually done from left to right (i.e. aft to fore) even if the longitudinal axis is reversed
    if (m_aft_limit > m_fore_limit) {
        torque = -torque;
    }
    return torque;
}

LinearDensity BlockWeight::BlockWeightPrivate::aft_density() const {
    TorqueDensityRatioComponents R = torque_density_ratio();
    if (qFuzzyIsNull(m_torque / kilogrammeter)) {
        /**
         * torque = d2 * C1 + d2 * C0 = 0
         * weight = (d1+d2)/2*(x2-x1)
         * combine to
         */
        return 2 * m_weight / ((m_fore_limit - m_aft_limit) * (1 - R.coefficient_0 / R.coefficient_1));
    }
    const double r = density_ratio();
    if (r > 1e100) { //workaround windows's lack of isinf
        return 0.0 * kilogram_per_meter;
    }
    //numerically unstable for very small m_torque and (r * R.coefficient_1 + R.coefficient_0)
    //i.e. the case of lcg ~= 0
    return m_torque / (r * R.coefficient_1 + R.coefficient_0);
}

LinearDensity BlockWeight::BlockWeightPrivate::fore_density() const {
    TorqueDensityRatioComponents R = torque_density_ratio();
    if (qFuzzyIsNull(m_torque / kilogrammeter)) {
        /**
         * torque = d2 * C1 + d2 * C0 = 0
         * weight = (d1+d2)/2*(x2-x1)
         * combine to
         */
        return 2 * m_weight / ((m_fore_limit - m_aft_limit) * (1 - R.coefficient_1 / R.coefficient_0));
    }
    const double r = density_ratio();
    if (qFuzzyIsNull(r)) {
        return 0.0 * kilogram_per_meter;
    }
    //numerically unstable for very small m_torque and (r * R.coefficient_1 + R.coefficient_0)
    //i.e. the case of lcg ~= 0
    return m_torque / (R.coefficient_1 + 1 / r * R.coefficient_0);
}

BlockWeight::BlockWeightPrivate::TorqueDensityRatioComponents BlockWeight::BlockWeightPrivate::torque_density_ratio() const {
    const Length& x1 = m_aft_limit;
    const Length& x2 = m_fore_limit;
    TorqueDensityRatioComponents rv;
    rv.coefficient_1 =   1 / (x2 - x1) * (1 / 3.0 * (x2 * x2 * x2 - x1 * x1 * x1) - x1 / 2.0 * (x2 * x2 - x1 * x1));
    rv.coefficient_0 = - 1 / (x2 - x1) * (1 / 3.0 * (x2 * x2 * x2 - x1 * x1 * x1) - x1 / 2.0 * (x2 * x2 - x1 * x1))
                       +  1 / 2.0 * (x2 * x2 - x1 * x1);
    return rv;
}

double BlockWeight::BlockWeightPrivate::density_ratio() const {
    // special casing: no torque - both densities are 0
    if (qFuzzyIsNull(m_torque / kilogrammeter)) {
        return 1.0;
    }
    /**
     * weight = aft_density (1 + r) * length / 2
     * torque = aft_density * (r * C1 + C2)
     * isolating r gives:
     */
    const Length length = m_fore_limit - m_aft_limit;
    TorqueDensityRatioComponents C = torque_density_ratio();
    double r = (m_torque * length - 2 * m_weight * C.coefficient_0)
               / (2 * m_weight * C.coefficient_1 - m_torque * length);
    return r;
}

BlockWeight::BlockWeight(Length lcg, Length aft_limit, Length fore_limit, Length vcg, Length tcg, Mass weight)
  : d(new BlockWeightPrivate())
{
    d->init(lcg, aft_limit, fore_limit, vcg, tcg, weight, weight * lcg);
    if (d->m_aft_limit == d->m_fore_limit) {
        d->m_aft_limit = d->m_lcg - 0.01 / 2 * meter;
        d->m_fore_limit = d->m_lcg + 0.01 / 2 * meter;
        qDebug() << "Longitudinal extent >= 1cm required - set to 1 cm";
    }
}

BlockWeight::BlockWeight(Length aft_limit, LinearDensity aft_density, Length fore_limit, LinearDensity fore_density,
                         Length vcg, Length tcg)
  : d(new BlockWeightPrivate())
{
    d->init(qQNaN() * meter, aft_limit, fore_limit, vcg, tcg, qQNaN() * kilogram, qQNaN() * kilogrammeter);
    d->init_from_densities(aft_density, fore_density);
}

BlockWeight::BlockWeight() : d(new BlockWeightPrivate())
{
    d->init(qQNaN() * meter, qQNaN() * meter, qQNaN() * meter, 0.0 * meter, 0.0 * meter, 0.0 * kilogram, 0.0 * kilogrammeter);
}

BlockWeight::BlockWeight(const BlockWeight& other) : d(other.d)
{
    // Empty
}

BlockWeight::~BlockWeight() {
    // Just to ensure that QScopedPointer knows about BlockWeightPrivate's destructor
}

BlockWeight& BlockWeight::operator=(const BlockWeight& other) {
    d = other.d;
    return *this;
}

Length BlockWeight::lcg() const {
    if (qFuzzyIsNull(d->m_torque / kilogrammeter)) {
        return d->m_lcg;
    }
    return d->m_torque / d->m_weight;
}

Length BlockWeight::aftLimit() const {
    return d->m_aft_limit;
}

Length BlockWeight::foreLimit() const {
    return d->m_fore_limit;
}

Mass BlockWeight::weight() const {
    return d->m_weight;
}

LinearDensity BlockWeight::aftDensity() const {
    return d->aft_density();
}

LinearDensity BlockWeight::foreDensity() const {
    return d->fore_density();
}

Length BlockWeight::tcg() const {
    return d->m_tcg;
}

Length BlockWeight::vcg() const {
    return d->m_vcg;
}

void BlockWeight::extendWith(const BlockWeight& other) {
    if (qIsNaN(other.d->m_aft_limit / meter)) {
        return;
    }
    if (qIsNaN(d->m_aft_limit / meter)) {
        *this = other;
        return;
    }
    const Mass total_weight = d->m_weight + other.weight();
    d->m_torque += other.d->m_torque;
    if (!qFuzzyIsNull(total_weight / kilogram)) {
        d->m_lcg = d->m_torque / total_weight;
        d->m_vcg = (d->m_vcg * weight() + other.vcg() * other.weight()) / total_weight;
        d->m_tcg = (d->m_tcg * weight() + other.tcg() * other.weight()) / total_weight;
    }
    d->m_weight = total_weight;
    d->m_aft_limit = qMin(d->m_aft_limit, other.d->m_aft_limit);
    d->m_fore_limit = qMax(d->m_fore_limit, other.d->m_fore_limit);
}

BlockWeight BlockWeight::oppositeWeight() const {
    BlockWeight opposite(*this);
    opposite.d->m_weight = - opposite.d->m_weight;
    opposite.d->m_torque = - opposite.d->m_torque;
    return opposite;
}

BlockWeight BlockWeight::totalBlockWeight(const QList<BlockWeight>& blockWeights) {
    BlockWeight rv;
    Q_FOREACH (const BlockWeight& blockWeight, blockWeights) {
        rv.extendWith(blockWeight);
    }
    return rv;
}

MassMoment BlockWeight::longitudinalMomentForeOf(Length lcg) const {
    double rv = 0.0;
    if (aftLimit() != foreLimit()) {
        if (lcg <= aftLimit()) {
            rv = torqueIntegral(aftLimit(), foreLimit(), lcg);
            //include the aft limit, or the calculation will fail miserably at the limit
            //exclude the fore limit, or we will be calculating the contribution of a 0 width block weight
        } else if (lcg >= aftLimit() && lcg < foreLimit()) {
            rv = torqueIntegral(lcg, foreLimit(), lcg);
        }
    }
    return rv * kilogram*meter; // FIXME use units during calculation
}

MassMoment BlockWeight::longitudinalMomentAftOf(Length lcg) const {
    double rv = 0.0;
    if (aftLimit() != foreLimit()) {
        if (lcg > foreLimit()) {
            rv = - torqueIntegral(aftLimit(), foreLimit(), lcg);
            //include the fore limit, or the calculation will fail miserably at the limit
            //exclude the aft limit, or we will be calculating the contribution of a 0 width block weight
        } else if (lcg > aftLimit() && lcg <= foreLimit()) {
            rv = -torqueIntegral(aftLimit(), lcg, lcg);
        }
    }
    return rv * kilogram*meter; // FIXME use units during calculation
}

double BlockWeight::torqueIntegral(Length start, Length end, Length torque_reference) const {
    if (aftDensity() / kilogram_per_meter == 0.0 && foreDensity() / kilogram_per_meter == 0.0) {
        return 0.0;
    }
    Unit<1, -2, 0> a = (foreDensity() - aftDensity()) / (foreLimit() - aftLimit());
    LinearDensity b = aftDensity() - aftLimit() * a;
    BlockWeight partial_block_weight(start, a * start + b, end, a * end + b);
    return partial_block_weight.longitudinalMoment(torque_reference) / kilogrammeter;
}

MassMoment BlockWeight::longitudinalMoment(Length longitudinalPosition) const {
    if (!qFuzzyIsNull(weight() / kilogram)) {
        return weight() * (lcg() - longitudinalPosition);
    } else {
        // a 0-weight block_weight's torque is independent of the reference
        // KIM why?
        return d->m_torque;
    }
}

MassMoment BlockWeight::verticalMoment(Length verticalPosition) const {
    return weight() * (vcg() - verticalPosition);
}

MassMoment BlockWeight::transverseMoment(Length transversePosition) const {
    return weight() * (tcg() - transversePosition);
}

Mass BlockWeight::weightAftOf(Length lcg) const {
    if (qIsNaN(d->m_aft_limit / meter)) {
        return 0 * kilogram;
    }
    if (foreLimit() <= lcg) {
        return weight();
    }
    if (lcg <= aftLimit()) {
        return 0 * kilogram;
    }
    Q_ASSERT(aftLimit() != foreLimit()); // if the limits were the same one of the two if statements above are true
    Unit<1, -2, 0> a = (foreDensity() - aftDensity()) / (foreLimit() - aftLimit());
    LinearDensity b = aftDensity() - aftLimit() * a;
    return 0.5 * (lcg - aftLimit()) * (aftDensity() + a * lcg + b);
}

Mass BlockWeight::weightForeOf(Length lcg) const {
    if (qIsNaN(d->m_aft_limit / meter)) {
        return 0 * kilogram;
    }
    if (foreLimit() <= lcg) {
        return 0 * kilogram;
    }
    if (lcg <= aftLimit()) {
        return weight();
    }
    Q_ASSERT(aftLimit() != foreLimit()); // if the limits were the same one of the two if statements above are true
    Unit<1, -2, 0> a = (foreDensity() - aftDensity()) / (foreLimit() - aftLimit());
    LinearDensity b = aftDensity() - aftLimit() * a;
    return 0.5 * (foreLimit() - lcg) * (foreDensity() + a * lcg + b);
}

void BlockWeight::setAftLimit(Length longitudinalPosition) {
    d->m_aft_limit = longitudinalPosition;
}

void BlockWeight::setAftDensity(LinearDensity density) {
    d->init_from_densities(density, foreDensity());
}

void BlockWeight::setForeLimit(Length longitudinalPosition) {
    d->m_fore_limit = longitudinalPosition;
}

void BlockWeight::setForeDensity(LinearDensity density) {
    d->init_from_densities(aftDensity(), density);
}

void BlockWeight::setTcg(Length tcg) {
    d->m_tcg = tcg;
}

void BlockWeight::setVcg(Length vcg) {
    d->m_vcg = vcg;
}

void BlockWeight::setWeight(Mass weight) {
    if (qFuzzyIsNull(weight / kilogram)) {
        d->m_weight = 0.0 * kilogram;
        d->m_torque = 0.0 * kilogrammeter;
        return;
    }
    const double changeRatio = weight / d->m_weight;
    d->m_weight = d->m_weight * changeRatio;
    d->m_torque = d->m_torque * changeRatio;
}

void BlockWeight::setDescription(const QString& description) {
    d->m_description = description;
}

QString BlockWeight::description() const {
    return d->m_description;
}

void BlockWeight::mirrorLongitudinally() {
    setAftLimit(-aftLimit());
    setForeLimit(-foreLimit());
    d->m_lcg = -d->m_lcg;
    d->m_torque = -d->m_torque;
}

QString BlockWeight::toString() const {
    return QString("BlockWeight['%7' Mass:%1t fore:%2 LCG:%3m aft:%4m VCG:%5m TCG:%6m]")
           .arg(weight() / ton, 0, 'g', 3)
           .arg(foreLimit() / meter, 0, 'g', 3)
           .arg(lcg() / meter, 0, 'g', 3)
           .arg(aftLimit() / meter, 0, 'g', 3)
           .arg(vcg() / meter, 0, 'g', 3)
           .arg(tcg() / meter, 0, 'g', 3)
           .arg(description())
           ;
}

QDebug operator<<(QDebug stream, const BlockWeight& block) {
    return stream << block.toString();
}

QDebug operator<<(QDebug stream, const BlockWeight* block) {
    return block == 0 ? stream << "BlockWeight[null]" : stream << *block;
}

} // namespace vessel
} // namespace ange
