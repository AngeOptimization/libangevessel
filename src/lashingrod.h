#ifndef LIBANGEVESSEL_LASHINGROD_H
#define LIBANGEVESSEL_LASHINGROD_H

#include "angevessel_export.h"
#include "direction.h"
#include "point3d.h"
#include <ange/units/units.h>

namespace ange {
namespace vessel {

/**
 * A single lashing rod, will usually use 4 to 8 for a container
 */
class ANGEVESSEL_EXPORT LashingRod {

public:
    /**
     * Create lashing rod with data that needs to be overwritten (lots of NaN)
     */
    LashingRod();

    /**
     * E-module, usually 1.4 to 1.9 kN/cm^2
     */
    units::Unit<1, -1, -2> eModule() const;

    void setEModule(units::Unit<1, -1, -2> eModule);

    /**
     * The diameter of the lashing rod, usually about 2.5 cm
     */
    units::Length diameter() const;

    void setDiameter(units::Length diameter);

    /**
     * Length of rod
     */
    units::Length length() const;

    void setLength(units::Length length);

    /**
     * The transverse angle from vertical with positive direction towards the center of the container.
     * In radians.
     */
    double transverseAngle() const;

    void setTransverseAngle(double transverseAngle);

    /**
     * Maximum safe work load (max. SWL). Usually between 230 and 300 kN.
     */
    units::Force maxLoad() const;

    void setMaxLoad(units::Force maxLoad);

    /**
     * How much the lashing bridge this rod is attached to will move (fetch) in the direction of the force. It is 0 mm
     * when the rod is attached directly to the deck or in the fore end. If an aft rod is attached to a one tier high
     * bridge the length is usually 10 mm, if it is attached to a two tier high bridge it is 25 mm.
     */
    units::Length lashingBridgeDeformation() const;

    void setLashingBridgeDeformation(units::Length lashingBridgeDeformation);

    /**
     * End lashing rod is attached to
     */
    Direction::AftFore end() const;

    void setEnd(Direction::AftFore end);

    /**
     * Side lashing rod is attached to
     */
    Direction::StarboardPort side() const;

    void setSide(Direction::StarboardPort side);

    /**
     * Top/bottom lashing rod is attached to
     */
    Direction::TopBottom topBottom() const;

    void setTopBottom(Direction::TopBottom topBottom);

    /**
     * Sets the length and transverseAngle based on the given coordinates for the lashing points (ends of lashing rods).
     * Uses the current value for side to adjust the sign of the angle.
     */
    void setLashingPoints(const Point3D& vessel, const Point3D& container);

    /**
     * Spring constant in the horizontal transverse direction, has been projected twice using the transverse angle
     */
    units::Unit<1, 0, -2> transverseSpringConstant() const;

private:
    units::Unit<1, -1, -2> m_eModule;
    units::Length m_diameter;
    units::Length m_length;
    double m_transverseAngle;
    units::Force m_maxLoad;
    units::Length m_lashingBridgeDeformation;
    Direction::AftFore m_end;
    Direction::StarboardPort m_side;
    Direction::TopBottom m_topBottom;

};

}} // namespace ange::vessel

#endif // LIBANGEVESSEL_LASHINGROD_H
