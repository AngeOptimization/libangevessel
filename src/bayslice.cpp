#include "bayslice.h"

#include "cargohold.h"
#include "compartment.h"
#include "slot.h"
#include "stack.h"
#include "vessel.h"

#include <stdexcept>

namespace ange {
namespace vessel {

struct sort_by_z_position {
  bool operator()(const Stack* lhs, const Stack* rhs) const {
    if (lhs==rhs) {
      return false;
    }
    if (lhs->level() != rhs->level()) {
      return rhs->level() == Below;
    }
    if (lhs->bottom().t() == rhs->bottom().t()) {
      // Sometimes, these numbers are missing or invalid. So fall back on row numbers
      const int rh_row = rhs->row();
      const int lh_row = lhs->row();
      const bool rh_odd = rh_row % 2;
      const bool lh_odd = lh_row % 2;
      if (rh_odd != lh_odd) {
        return rh_odd;
      }
      else return lh_odd ? lh_row < rh_row : lh_row > rh_row;
    }
    return lhs->bottom().t() < rhs->bottom().t();
  }
};

class BaySlicePrivate {
  public:
    BaySlicePrivate(const QList< Stack* >& stacks) ;
    void setCargoHold(CargoHold* hold, BaySlice* bay_slice) {
      if (hold == m_cargoHold) {
        return;
      }
      if (m_cargoHold) {
        m_cargoHold->removeBayslice(bay_slice);
        Q_FOREACH(Stack* stack, m_stacksAbove + m_stacksBelow) {
          stack->setAllowedImoClasses(QSet<QString>());
        }
      }
      m_cargoHold = hold;
      if (m_cargoHold) {
        m_cargoHold->addBayslice(bay_slice);
        Q_FOREACH(Stack* stack, m_stacksAbove) {
            stack->setForbiddenImoClasses(m_cargoHold->forbiddenImoClasses(Above));
            stack->setAllowedImoClasses(m_cargoHold->acceptedImoClasses(Above));
        }
        Q_FOREACH(Stack* stack, m_stacksBelow) {
            stack->setForbiddenImoClasses(m_cargoHold->forbiddenImoClasses(Below));
            stack->setAllowedImoClasses(m_cargoHold->acceptedImoClasses(Below));
        }
      }
    }

    QList<Stack*> m_stacksAbove;
    QList<Stack*> m_stacksBelow;
    QList<Compartment*> m_compartments;
    QList<int> m_bays;
    int m_joined_bay;
    QList<int> m_allBays;
    CargoHold* m_cargoHold;
};

BaySlice::BaySlice(const QList< Stack* >& stacks)
  : QObject(0), d(new BaySlicePrivate(stacks))
{
    Q_FOREACH (Stack* stack, stacks) {
        stack->setParent(this);
    }
}

Stack* BaySlice::stackContaining(BayRowTier brt) const {
  Q_FOREACH (Stack* stack, stacks()) {
    if (stack->row() == brt.row() and stack->tiers().contains(brt.tier())) {
      return stack;
    }
  }
  return 0;
}

Stack* BaySlice::stackContaining(int row, DeckLevel level) const {
  Q_FOREACH (Stack* stack, stacks()) {
    if (stack->row() == row && stack->level() == level) {
      return stack;
    }
  }
  return 0;
}

const QList< Stack* > BaySlice::stacks() const {
    return d->m_stacksAbove + d->m_stacksBelow;
}

const QList< Stack* > BaySlice::stacksAbove() const {
    return d->m_stacksAbove;
}

const QList< Stack* > BaySlice::stacksBelow() const {
    return d->m_stacksBelow;
}

QList< int > BaySlice::bays() const {
    return d->m_bays;
}

int BaySlice::joinedBay() const {
    return d->m_joined_bay;
}

QList<int> BaySlice::allBays() const {
    return d->m_allBays;
}

void BaySlice::setCompartments(QList< Compartment* > compartments) {
    d->m_compartments = compartments;
}

QList< Compartment* > BaySlice::compartments() const {
    return d->m_compartments;
}

BaySlicePrivate::BaySlicePrivate(const QList< Stack* >& stacks)
  : m_joined_bay(-1),
    m_cargoHold(0L)
{
    Q_FOREACH(Stack* stack, stacks) {
        if (stack->level() == Above) {
            m_stacksAbove.append(stack);
        } else {
            m_stacksBelow.append(stack);
        }
    }
  qSort(m_stacksAbove.begin(), m_stacksAbove.end(), sort_by_z_position());
  qSort(m_stacksBelow.begin(), m_stacksBelow.end(), sort_by_z_position());
  Q_FOREACH(Stack* stack, stacks) {
    Q_FOREACH(int bay, stack->bays()) {
      if (!m_bays.contains(bay)) {
        m_bays << bay;
      }
    }
  }
  qSort(m_bays);
  if (m_bays.size() == 1) {
    m_joined_bay = m_bays.value(0);
  } else if (m_bays.size() == 2) {
    int fore = m_bays[0];
    int aft = m_bays[1];
    if (fore % 2 == 0 || aft % 2 == 0 || aft - fore != 2) {
      throw std::runtime_error(QString("Bay slice containing exactly %1 and %2").arg(fore).arg(aft).toStdString());
    }
    m_joined_bay = (aft + fore) / 2;
  } else if (m_bays.size() == 3) {
    if (m_bays[1] - m_bays[0] != 1 || m_bays[2] - m_bays[1] != 1 || m_bays[1] % 2 != 0) {
      throw std::runtime_error(QString("Bay slice containing exactly %1, %2 and %3").arg(m_bays[0]).arg(m_bays[1]).arg(m_bays[2]).toStdString());
    }
    m_joined_bay = m_bays[1];
  } else if (m_bays.size() == 0) {
    throw std::runtime_error("Empty bay slice");
  } else {
    QString pretty("[");
    pretty += QString::number(m_bays[0]);
    for (int i = 1; i < m_bays.size(); ++i) {
      pretty += ", " + QString::number(m_bays[i]);
    }
    pretty += "]";
    throw std::runtime_error(QString("Bay slice with bays %1 invalid").arg(pretty).toStdString());
  }
  Q_ASSERT(m_joined_bay != -1);
  QSet<int> allBays;
  Q_FOREACH (int bay, m_bays) {
    allBays << bay;
  }
  allBays << m_joined_bay;
  Q_FOREACH (Stack* stack, m_stacksAbove + m_stacksBelow) {
      Q_FOREACH (Slot* slot, stack->stackSlots()) {
          allBays << slot->brt().bay();
      }
  }
  m_allBays = allBays.toList();
  qSort(m_allBays);
}

bool BaySlice::isAftward(BayRowTier brt) const {
  return isAftward(brt.bay());
}

bool BaySlice::isForward(BayRowTier brt) const {
  return isForward(brt.bay());
}

bool BaySlice::isAftward(int bay) const {
  return bays().back() == bay;
}

bool BaySlice::isForward(int bay) const {
  return bays().front() == bay;
}

Compartment* BaySlice::compartmentContainingRow
(int row, DeckLevel level) const {
  Q_FOREACH (Compartment* compartment, d->m_compartments) {
    Q_FOREACH (const Stack* stack, compartment->stacks()) {
      if (stack->row() == row && stack->level() == level) {
        return compartment;
      }
    }
  }
  return 0;
}

QDebug operator<<( QDebug dbg, const ange::vessel::BaySlice* bay_slice ) {
  if (bay_slice) {
    return dbg << *bay_slice;
  } else {
    return dbg << "(bayslice: <null>)";
  }
}

QDebug operator<<( QDebug dbg, const ange::vessel::BaySlice& bay_slice ) {
  return dbg << "(bayslice: " << bay_slice.joinedBay() << ")";

}

CargoHold* BaySlice::cargoHold() const {
  return d->m_cargoHold;
}

void BaySlice::setCargoHold(CargoHold* cargoHold) {
  d->setCargoHold(cargoHold, this);
}

void BaySlice::setClearOfLivingQuarters(bool isClearAbove, bool isClearBelow) {
    Q_FOREACH (Stack* stack, stacks()) {
        if (stack->level() == Above) {
            stack->setClearOfLivingQuarters(isClearAbove);
        } else {
            stack->setClearOfLivingQuarters(isClearBelow);
        }
    }
}

units::Length BaySlice::lcg() const {
    QList<ange::units::Length> stackLongitudinalPositions;
    Q_FOREACH(const Stack* stack, stacks()) {
        stackLongitudinalPositions << stack->bottom().l();
    }
    qSort(stackLongitudinalPositions);
    const ange::units::Length medianStackLcg = stackLongitudinalPositions.at(stackLongitudinalPositions.size()/2);
    return medianStackLcg;
}

BaySlice::~BaySlice() {
  if (d->m_cargoHold) {
    d->m_cargoHold->removeBayslice(this);
  }
}

}} // namespace ange::vessel
