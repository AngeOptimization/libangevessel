#ifndef ANGE_VESSEL_BAYROWLEVEL_H
#define ANGE_VESSEL_BAYROWLEVEL_H

#include "angevessel_export.h"
#include "decklevel.h"

#include <QMetaType>
#include <QHash>

class QString;

namespace ange {
namespace vessel {

/**
 * Bay/Row/Level position of a stack
 */
class ANGEVESSEL_EXPORT BayRowLevel {

public:

    /**
     * The unknown/unplaced stack (0, 0, ABOVE)
     */
    BayRowLevel();

    /**
     * Constructor
     */
    BayRowLevel(int bay, int row, DeckLevel level);

    /**
     * Convert from URN in this format: "urn:stowbase.org:vessel:bay=2,row=16,level=ABOVE".
     * Will return the unknown stack if conversion fails.
     */
    static BayRowLevel fromUrn(QString urn);

    /**
     * Get bay
     */
    int bay() const;

    /**
     * Get row
     */
    int row() const;

    /**
     * Get level
     */
    DeckLevel level() const;

    /**
     * This is the unknown BayRowLevel
     */
    bool isUnknown() const;

    /**
     * @return string representation
     */
    QString toString() const;

private:
    int m_bay;
    int m_row;
    DeckLevel m_level;

private:
    friend uint qHash(const BayRowLevel& brl);

};

ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const BayRowLevel& brl);

// Implementations:

inline int BayRowLevel::bay() const {
    Q_ASSERT(!isUnknown());
    return m_bay;
}

inline int BayRowLevel::row() const {
    Q_ASSERT(!isUnknown());
    return m_row;
}

inline DeckLevel BayRowLevel::level() const {
    Q_ASSERT(!isUnknown());
    return m_level;
}

inline bool BayRowLevel::isUnknown() const {
    return m_bay == 0;
}

inline bool operator==(const BayRowLevel& lhs, const BayRowLevel& rhs) {
    return lhs.bay() == rhs.bay() && lhs.row() == rhs.row() && lhs.level() == rhs.level();
}

inline bool operator!=(const BayRowLevel& lhs, const BayRowLevel& rhs) {
    return !(lhs == rhs);
}

inline uint qHash(const BayRowLevel& brl) {
    return 10000 * brl.m_bay + 100 * brl.m_row + brl.m_level;
}

}} // namespace ange::vessel

Q_DECLARE_METATYPE(ange::vessel::BayRowLevel);

#endif // ANGE_VESSEL_BAYROWLEVEL_H
