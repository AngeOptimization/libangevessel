#ifndef ANGEVESSEL_COMPARTMENT_H
#define ANGEVESSEL_COMPARTMENT_H

#include <QObject>
#include "angevessel_export.h"

namespace ange {
namespace vessel {

class Stack;
class HatchCover;
class BaySlice;

class ANGEVESSEL_EXPORT Compartment : public QObject {

public:

    /**
     * Construct compartment
     * @param stacks stacks for compartment
     * @param lidsAbove list of lids on top of this compartment
     * @param lidsBelow list of lids beneath stacks in this compartment
     */
    Compartment(BaySlice* parent, QList<Stack*> stacks, QList<HatchCover*> lidsAbove, QList<HatchCover*> lidsBelow);

    virtual ~Compartment();

    /**
     * @return bay_slice that containers this compartment
     */
    BaySlice* baySlice() const;

    /**
     * @return stacks that make up this compartment
     */
    QList<const Stack*> stacks() const;

    /**
     * @return lids on top of compartment
     */
    QList<const HatchCover*> lidsAbove() const;

    /**
     * @return lids beneath this compartment
     */
    QList<const HatchCover*> lidsBelow() const;

private:
    class CompartmentPrivate;
    CompartmentPrivate* d;

private:
    using QObject::setParent; // Make changes to parent less likely

};

ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const Compartment& compartment);
ANGEVESSEL_EXPORT QDebug operator<<(QDebug dbg, const Compartment* compartment);

}
}

#endif // COMPARTMENT_H
