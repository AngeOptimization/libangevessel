#include "compartment.h"

#include "bayslice.h"
#include "stack.h"
#include <stdexcept>

namespace ange {
namespace vessel {

template<typename T>
QList<const T*> constify(QList<T*> list) {
    QList<const T*> constList;
    Q_FOREACH (const T* t, list) {
        constList << t;
    }
    return constList;
}

class Compartment::CompartmentPrivate {
public:
    QList<const Stack*> m_stacks;
    QList<const HatchCover*> m_above;
    QList<const HatchCover*> m_below;
    CompartmentPrivate(const QList<Stack*>& stacks, const QList<HatchCover*>& above, const QList<HatchCover*>& below)
        : m_stacks(constify(stacks)), m_above(constify(above)), m_below(constify(below)) {}
};

Compartment::Compartment(BaySlice* parent, QList<Stack*> stacks, QList<HatchCover*> lidsAbove, QList<HatchCover*> lidsBelow)
  : QObject(parent), d(new Compartment::CompartmentPrivate(stacks, lidsAbove, lidsBelow))
{
    if (stacks.isEmpty()) {
        throw std::invalid_argument("Compartments must have at least one stack");
    }
    const DeckLevel level = stacks.front()->level();
    Q_FOREACH (const Stack* stack, stacks) {
        if (stack->level() != level) {
            throw std::invalid_argument("Compartments cannot span multiple levels");
        }
    }
    QString objectName = QString("%1%2%3-%4").arg(parent->joinedBay()).arg(toChar(level)).arg(stacks.front()->row()).arg(stacks.back()->row());
    setObjectName(objectName);
    Q_FOREACH (Stack* stack, stacks) {
        stack->setCompartment(this);
    }
}

Compartment::~Compartment() {
    delete d;
}

BaySlice* Compartment::baySlice() const {
    return qobject_cast<BaySlice*>(parent());
}

QList<const Stack*> Compartment::stacks() const {
    return d->m_stacks;
}

QList<const HatchCover*> Compartment::lidsAbove() const {
    return d->m_above;
}

QList<const HatchCover*> Compartment::lidsBelow() const {
    return d->m_below;
}

QDebug operator<<(QDebug dbg, const ange::vessel::Compartment* compartment) {
    if (compartment) {
        return dbg << *compartment;
    } else {
        return dbg << "<null compartment>";
    }
}

QDebug operator<<(QDebug dbg, const ange::vessel::Compartment& compartment) {
    return dbg << compartment.objectName();
}

}} // end namespaces
